'use strict';

var env = process.env.NODE_ENV;
var config = require('../config/environment/' + env);

var LocalStrategy = require('passport-local').Strategy;
var BasicStrategy = require('passport-http').BasicStrategy;

var bcrypt = require('bcrypt-nodejs');
var _ = require('lodash-node');
var async = require('async');
var Cryptr = require('cryptr');

var mysql = require('mysql');
var Database = require('../app/utils/database').Database;
var db = new Database();

module.exports = function(passport, jwt) {

    passport.use('user', new BasicStrategy(
        function(username, password, done) {
            var str = mysql.format('SELECT U.UserID,UG.GroupID,U.UserName,U.Password,U.FullName,U.Inactive,U.EmployeeID,U.IsEmployee,U.Email,U.MobileNo,U.AccessLimit,MD5(E.e_id) as E_UUID, MD5(E.e_idno) as Emp_UUID,MD5(U.UserID) AS UUID FROM users U INNER JOIN employees E ON E.e_idno = U.EmployeeID LEFT JOIN users_group UG ON U.UserGroupID = UG.GroupID WHERE U.UserName=? AND U.IsEmployee=1;', [username]);
            console.log('user str: ', str);
            db.query(str, verifyAuth(password, done));
        }
    ));

    passport.use('hris', new BasicStrategy(
        function(username, password, done) {
            var str = mysql.format('SELECT U.UserID,UG.GroupID,U.UserName,U.Password,U.FullName,U.Inactive,U.EmployeeID,U.IsEmployee,U.Email,U.MobileNo,U.AccessLimit,MD5(E.e_id) as E_UUID, MD5(E.e_idno) as Emp_UUID,MD5(U.UserID) AS UUID FROM users U LEFT JOIN employees E ON E.e_idno = U.EmployeeID LEFT JOIN users_group UG ON U.UserGroupID = UG.GroupID WHERE U.UserName=? AND U.IsEmployee=0;', [username]);
            console.log('hris str: ', str);
            db.query(str, verifyAuth(password, done));
        }
    ));

    passport.serializeUser(function(user, done) {
        done(null, user);
    });

    passport.deserializeUser(function(user, done) {
        done(null, user);
    });

    function verifyAuth(password, done) {
        var cryptr = new Cryptr(config.app_secretkey);
        return function(err, user) {
            if (err) {
                console.log('error:', err);
                return done(err);
            }

            if (_.isEmpty(user)) {
                return done(null, {
                    msg: 'User does not exist with this user account.',
                    success: false,
                    result: ''
                });
            }

            user = user[0];
            console.log('user: ', user);
            async.waterfall([
                (callback) => {
                    console.log('user.GroupID: ', user.GroupID);
                    if (!_.isNull(user.GroupID)) {
                        console.log('group.Prev: ');
                        var strSQL = mysql.format('SELECT GP.GroupID,MO.ModuleID,MO.ModuleName,ME.Name,ME.Caption,ME.Description,GP.Read AS DefaultRead,GP.Write AS DefaultWrite, GP.Delete AS DefaultDelete, GP.Export AS DefaultExport,GP.Print AS DefaultPrint,ME.Inactive,ME.ID ' +
                            'FROM menus ME LEFT JOIN modules MO ON ME.Module = MO.ModuleID LEFT JOIN group_privileges GP ON GP.ModuleID = ME.ID ' +
                            'WHERE GP.GroupID = ? ORDER BY MO.ModuleID;', [user.GroupID]);
                        console.log('Group strSQL: ',strSQL);
                        db.query(strSQL, function(e, r) {
                            if (e) {
                                return done(null, {
                                    msg: '',
                                    success: false,
                                    result: e
                                });
                            }
                            callback(null,r);
                        });
                    } else {
                        console.log('user.Prev: ');
                        var strSQL = mysql.format('SELECT UP.UserID,MO.ModuleID,MO.ModuleName,ME.Name,ME.Caption,ME.Description,UP.Read AS DefaultRead,UP.Write AS DefaultWrite ,UP.Delete AS DefaultDelete,UP.Print AS DefaultPrint,UP.Export AS DefaultExport, ME.Inactive,ME.ID FROM menus ME ' +
                            'LEFT JOIN modules MO ON ME.Module = MO.ModuleID LEFT JOIN user_privileges UP ON UP.ModuleID = ME.ID ' +
                            'WHERE UP.UserID=? ORDER BY MO.ModuleID;', [user.UserID]);
                        console.log('strSQL: ',strSQL);
                        db.query(strSQL, function(e, r) {
                            if (e) {
                                return done(null, {
                                    msg: '',
                                    success: false,
                                    result: e
                                });
                            }
                            callback(null,r);
                        });
                    }
                },
                (privileges, callback) => {
                    if (user.Inactive === 1) {
                        return done(null, {
                            msg: 'Your account has been INACTIVE.',
                            success: false,
                            result: ''
                        });
                    } else {
                        var encryptedString = cryptr.encrypt(password);
                        if (encryptedString !== user.Password) {
                            return done(null, {
                                msg: 'Invalid Username or Password',
                                success: false,
                                result: ''
                            });
                        }
                        delete user.Password;
                        var token = jwt.sign(user, config.token_secret, {
                            expiresInMinutes: 60 * 5
                        });

                        return done(null, {
                            msg: 'Login successfully',
                            success: true,
                            result: {
                                user: user,
                                privilege: privileges,
                                token: token
                            }
                        });
                    }
                }
            ]);
        };
    };
};