(function() {
    'use strict';

    angular.module('hris')
        .factory('civilstatus', ['Restangular', function(Restangular) {
            return {

                saveEntry: function(data) {
                    return Restangular.all('civilstatus').customPOST(data).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                deleteCivilStatus: function(id) {
                    return Restangular.all('civilstatus/' + id).customDELETE().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                updateCivilStatus: function(id, data) {
                    return Restangular.all('civilstatus/' + id).customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },
                getCivilStatusById: function(id) {
                    return Restangular.all('civilstatus').customGET(id).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },
                getAllCivilStatus: function() {
                    return Restangular.all('civilstatus').customGET().then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                }
            };
        }]);

})();