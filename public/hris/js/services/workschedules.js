(function () {
    'use strict';

    angular.module('hris')
        .factory('workschedules', ['Restangular', function (Restangular) {
            return {
                getAllDays: function () {
                    return Restangular.all('days').customGET().then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                },
                getAllWorkSchedules: function () {
                    return Restangular.all('workschedules').customGET().then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                },
                getWorkSchedulesTemplates: function (sched_id) {
                    return Restangular.all('workschedules').customGET(sched_id).then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                },
                getWorkScheduleDetails: function (sched_id) {
                    return Restangular.all('workschedules/' + sched_id + '/details').customGET().then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                },
                Add_Work_Schedule: function (data) {
                    return Restangular.all('workschedules').customPOST(data).then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                },
                Delete_Work_Schedule: function (sched_id) {
                    return Restangular.all('workschedules/' + sched_id).customDELETE().then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                },
                Update_Work_Schedule: function (sched_id, data) {
                    return Restangular.all('workschedules/' + sched_id).customPUT(data).then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                },
                SetWorkScheduleTemplates: function (sched_id,data) {
                    return Restangular.all('workschedules/' + sched_id + '/templates').customPOST(data).then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                },
            };
        }]);

})();