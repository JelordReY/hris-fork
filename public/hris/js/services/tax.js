(function() {
    'use strict';

    angular.module('hris')
        .factory('wtax', ['Restangular', function(Restangular) {
            return {


                saveEntry: function(data) {
                    return Restangular.all('wtax').customPOST(data).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                saveHoldingTax: function(tax_id, data) {
                    return Restangular.all('wtax/' + tax_id + '/bracket').customPOST(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },

                saveTaxCeiling: function(tax_id, data) {
                    return Restangular.all('wtax/' + tax_id + '/ceiling').customPOST(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },

                saveTaxPercentage: function(tax_id, data) {
                    return Restangular.all('wtax/' + tax_id + '/percent').customPOST(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },

                saveTaxDue: function(tax_id, data) {
                    return Restangular.all('wtax/' + tax_id + '/due').customPOST(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },

                updateWithholdingTax: function(id, data) {
                    return Restangular.all('wtax/' + id).customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },

                updateTaxDetails: function(id ,data) {
                    return Restangular.all('wtax/' + id + '/bracket/' + id).customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },  

                updateTaxPercent: function(id, data) {
                    return Restangular.all('wtax/' + id + '/percent/' + id).customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },

                updateTaxCeil: function(id, data) {
                    return Restangular.all('wtax/' + id + '/ceiling/' + id).customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },
                
                updateTaxDue: function(id, data) {
                    return Restangular.all('wtax/' + id + '/due/' + id).customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },

                getTaxTableByID: function(id) {
                    return Restangular.all('wtax').customGET(id).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                getTaxDetails: function(id) {
                    return Restangular.all('wtax/' + id + '/bracket').customGET().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                getTaxesByID: function(id) {
                    return Restangular.all('wtax').customGET(id).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                removeTaxDetailsByTAXID: function(id) {
                    return Restangular.all('wtax/' + id + '/bracket').customDELETE().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                getTaxDueByID: function(id) {
                    return Restangular.all('wtax/' + id + '/due').customGET().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                getTaxCeilingByID: function(id) {
                    return Restangular.all('wtax/' + id + '/ceiling').customGET().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                 getTaxPercentByID: function(id) {
                    return Restangular.all('wtax/' + id + '/percent').customGET().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                removeTaxCeilByTAXID: function(id) {
                    return Restangular.all('wtax/' + id + '/ceiling').customDELETE().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                 removeTaxPercentByTAXID: function(id) {
                    return Restangular.all('wtax/' + id + '/percent').customDELETE().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                removeTaxdueByTAXID: function(id) {
                    return Restangular.all('wtax/' + id + '/due').customDELETE().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },


                removeWithholdingTax: function(id) {
                    return Restangular.all('wtax/' + id).customDELETE().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                getCeilingDetails: function(tax_id,obj) {
                    console.log('obj ',obj);
                    return Restangular.all('wtax/' + tax_id +  '/ceiling').customGET("",obj).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                getAllTaxSummary: function(id,obj) {
                    console.log('obj ',obj);
                    return Restangular.all('wtax/'+id).customGET("",obj).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },  
                getAllWithholdingTax: function(params) {
                    return Restangular.all('wtax').customGET("", params).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;


                        });
                }
            };
        }]);

})();