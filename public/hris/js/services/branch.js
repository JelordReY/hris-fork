(function() {
    'use strict';

    angular.module('hris')
        .factory('branch', ['Restangular', function(Restangular) {
            return {
                deleteBranch: function(id) {
                    return Restangular.all('branch/' + id).customDELETE().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                updateBranch: function(id, data) {
                    return Restangular.all('branch/' + id).customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },
                getBranchId: function(id) {
                    return Restangular.all('branch').customGET(id).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },
                saveEntry: function(data) {
                    return Restangular.all('branch').customPOST(data).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },
                getAllBranch: function(params) {
                    return Restangular.all('branch').customGET("", params).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;


                        });
                }
            };
        }]);

})();