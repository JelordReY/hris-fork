(function() {
    /*jslint camelcase:false*/
    'use strict';

    angular.module('hris')
        .factory('authInterceptor', function($q, localStorageService, $location) {
            return {
                // Add authorization token to headers
                request: function(config) {
                    config.headers = config.headers || {};
                    if (localStorageService.get('user')) {
                        var token = localStorageService.get('user').token;
                        config.headers.Authorization = 'Bearer ' + token;
                    }
                    return config || $q.when(config);
                },
                // Intercept 401s and redirect you to login
                responseError: function(response) {
                    if (localStorageService.get('user')) {
                        var token = localStorageService.get('user').token;

                        if (response != null && response.status === 401 && token) {
                            localStorageService.remove('user');
                            $location.path('/login');
                            return $q.reject(response);
                        } else if (response.status === 500) {
                            $location.path('/login');
                            return response;
                        } else if (response.status === 0) {
                            $location.path('/login');
                            return response;
                        } else {
                            return $q.reject(response);
                        }
                    } else if (response.status === 401) {
                        localStorageService.remove('user');
                        $location.path('/login');
                        return $q.reject(response);
                    } else if (response.status === 500) {
                        // $location.path('/login');
                        return $q.reject(response);
                    } else if (response.status === 0) {
                        // $location.path('/login');
                        return $q.reject(response);
                    } else {
                        return $q.reject(response);
                    }
                }
            };
        });

})();