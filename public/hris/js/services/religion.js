(function() {
    'use strict';

    angular.module('hris')
        .factory('religion', ['Restangular', function(Restangular) {
            return {

                deleteReligion: function(id) {
                    return Restangular.all('religions/' + id).customDELETE().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                updateReligion: function(id, data) {
                    return Restangular.all('religions/' + id).customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },

                getReligionById: function(id) {
                    return Restangular.all('religions').customGET(id).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                saveEntry: function(data) {
                    return Restangular.all('religions').customPOST(data).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },


                getAllReligions: function() {
                    return Restangular.all('religions').customGET().then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;


                        });
                }
            };
        }]);

})();