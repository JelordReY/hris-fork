(function() {
    'use strict';

    angular.module('hris')
        .factory('jobposition', ['Restangular', function(Restangular) {
            return {

                deleteGroup: function(id) {
                    return Restangular.all('jobpositiongroup/' + id).customDELETE().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                deleteClass: function(id) {
                    return Restangular.all('jobpositionclass/' + id).customDELETE().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                updateJobPosition: function(id, data) {
                    return Restangular.all('jobposition/' + id).customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },

                updateGroup: function(id, data) {
                    return Restangular.all('jobpositiongroup/' + id).customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },

                updateClass: function(id, data) {
                    return Restangular.all('jobpositionclass/' + id).customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },

                getClassById: function(id) {
                    return Restangular.all('jobpositionclass').customGET(id).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                getGroupById: function(id) {
                    return Restangular.all('jobpositiongroup').customGET(id).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                getJobPositionById: function(id) {
                    return Restangular.all('jobposition').customGET(id).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                deleteJobPosition: function(id) {
                    return Restangular.all('jobposition/' + id).customDELETE().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                saveEntry: function(data) {
                    return Restangular.all('jobposition').customPOST(data).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                saveEntry2: function(data) {
                    console.log('data:', data)
                    return Restangular.all('jobpositiongroup').customPOST(data).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                saveEntry3: function(data) {
                    console.log('data:', data)
                    return Restangular.all('jobpositionclass').customPOST(data).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },


                getAllGroup: function() {
                    return Restangular.all('jobpositiongroup').customGET().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                getAllClass: function() {
                    return Restangular.all('jobpositionclass/').customGET().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },


                getAllJobPosition: function() {
                    return Restangular.all('jobposition').customGET().then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                }
            };
        }]);


})();