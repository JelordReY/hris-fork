(function () {
    'use strict';

    angular.module('hris')
        .factory('authInterceptor', authInterceptor);

    authInterceptor.$inject = ['$rootScope', '$q', '$location', '$injector', 'localStorageService','$activityIndicator'];

    function authInterceptor($rootScope, $q, $location, $injector, localStorageService,$activityIndicator) {
        return {
            request: function(config) {
                config.headers = config.headers || {};

                if (localStorageService.get('user')) {
                    var token = localStorageService.get('user').token;
                    var authdata = localStorageService.get('authdata');

                    config.headers.auth_token = token;               
                    if (authdata) {
                        config.headers.Authorization = 'Basic ' + authdata;
                    }
                }
                $activityIndicator.startAnimating();
                return config || $q.when(config);
            },
            response: function(response) {
                $activityIndicator.stopAnimating();
                return response || $q.when(response);
            },
            responseError: function(response) {
                if (response.status === 401) {
                    $rootScope.$broadcast('unauthorized');
                    $location.path('/login');
                }else if (response.status === 403) {
                    $rootScope.$broadcast('unauthorized');
                    $location.path('/login');
                }
                $activityIndicator.stopAnimating();
                return $q.reject(response);
            }
        };
    }

})();