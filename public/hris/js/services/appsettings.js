(function () {
    'use strict';

    angular.module('hris')
        .factory('appsettings', ['Restangular', function (Restangular) {
            return {
                Add_Application_Settings: function (data) {
                    return Restangular.all('appsettings').customPOST(data).then(function (res) {
                        return res;
                    }, function (err) {
                        return err.data;
                    });
                },
                GetAllApplicationSettings: function () {
                    return Restangular.all('appsettings').customGET().then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                },
                GetApplicationSettingsByID: function (SetupID) {
                    return Restangular.all('appsettings').customGET(SetupID).then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                },
                Delete_Application_Settings: function (SetupID) {
                    return Restangular.all('appsettings/' + SetupID).customDELETE().then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                },
                Update_Application_Settings: function (SetupID, data) {
                    return Restangular.all('appsettings/' + SetupID).customPUT(data).then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                },
                Activate_Settings: function (SetupID) {
                    return Restangular.all('appsettings/' + SetupID + '/active').customPUT().then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                }
            };
        }]);

})();