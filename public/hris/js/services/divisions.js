(function() {
    'use strict';

    angular.module('hris')
        .factory('division', ['Restangular', function(Restangular) {
            return {
                deleteDivision: function(id) {
                    return Restangular.all('divisions/' + id).customDELETE().then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },

                updateDivision: function(id, data) {
                    return Restangular.all('divisions/' + id).customPUT(data).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },
                getDivisionsById: function(id) {
                    return Restangular.all('divisions').customGET(id).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },

                saveEntry: function(data) {
                    return Restangular.all('divisions').customPOST(data).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;
                    });
                },
                getAllDivisions: function() {
                    return Restangular.all('divisions').customGET().then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                }
            };
        }]);

})();