(function () {
    'use strict';

    angular.module('hris')
        .factory('miscFctry', ['Restangular', function (Restangular) {
            return {
                getAllReligion: function () {
                    return Restangular.all('religions').customGET().then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                },
                getAllNationality: function () {
                    return Restangular.all('nationality').customGET().then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                }
            };
        }]);

})();