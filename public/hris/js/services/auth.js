(function() {
    'use strict';

    angular.module('hris')
        .factory('auth', auth);

    auth.$inject = ['Restangular', '$http', 'localStorageService', 'Base64'];

    function auth(Restangular, $http, localStorageService, Base64) {
        return {
            isAuthenticated: false,
            login: function(username, password) {
                var authdata = Base64.encode(username + ':' + password);
                localStorageService.set('authdata', authdata);
                $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;
                return Restangular.all('hris').customPOST().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            storeUser: function(user) {
                localStorageService.set('user', user);
                return true;
            },
            getUser: function(user) {
                return localStorageService.get('user');
            },
            logout: function() {
                return Restangular.all('logout').customGET().then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            forgotPassword: function(data) {
                return Restangular.all('forgotpassword').customPOST(data).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
        }
    }

})();