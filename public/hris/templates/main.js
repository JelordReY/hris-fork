(function() {
    'use strict';

    angular.module('hris')
        .controller('mainCtrl', mainCtrl);

    mainCtrl.$inject = ['$scope', '$state', 'auth', 'localStorageService', '$location', '$uibModal', 'users','toastr'];

    function mainCtrl($scope, $state, auth, localStorageService, $location, $uibModal, users,toastr) {
        $scope.UserPrivilege = [];

        $scope.UserRights = {
            AllowRead: 1,
            AllowWrite: 2,
            AllowDelete: 3,
            AllowPrint: 4
        }

        if (auth.getUser()) {
            $scope.currentUser = auth.getUser().user;
            $scope.UserPrivilege = auth.getUser().privilege;
        } else {
            $location.path('/login');
        }  

        $scope.denyWriteMsg = function(){
            toastr.warning('Sorry you have NO WRITE privilege in this module.<br>If this should not be case, please inform your Database Administrator.','Limited Privileges Only');
            return;
        };
<<<<<<< HEAD
        
=======

>>>>>>> adcc73cce1947a0128f68dd5435f944c7c06c82c
        $scope.denyReadMsg = function(){
            toastr.warning('Sorry you have NO READ privilege in this module.<br>If this should not be case, please inform your Database Administrator.','Limited Privileges Only');
            return;
        };

        $scope.denyPrintMsg = function(){
            toastr.warning('Sorry you have NO PRINT privilege in this module.<br>If this should not be case, please inform your Database Administrator.','Limited Privileges Only');
            return;
        };

        $scope.denyDeleteMsg = function(){
            toastr.warning('Sorry you have NO EXPORT privilege in this module.<br>If this should not be case, please inform your Database Administrator.','Limited Privileges Only');
<<<<<<< HEAD

=======
            return;
>>>>>>> adcc73cce1947a0128f68dd5435f944c7c06c82c
        };
        


        $scope.findModule = function(UserPrivilege, pRights) {
            var results = _.find($scope.UserPrivilege, function(row) {
                return row.Name == UserPrivilege
            });
            if (results) {
                switch (pRights) {
                    case $scope.UserRights.AllowRead:
                        if (results.DefaultRead == 1) {
                            return true;
                        }
                        break;
                    case $scope.UserRights.AllowWrite:
                        if (results.DefaultWrite == 1) {
                            return true;
                        }
                        break;
                    case $scope.UserRights.AllowDelete:
                        if (results.DefaultDelete == 1) {
                            return true;
                        }
                        break;
                    case $scope.UserRights.AllowPrint:
                        if (results.DefaultPrint == 1) {
                            return true;
                        }
                        break;
                    default:
                        return false;
                }
            } else {
                return false;
            }
        };

        $scope.logout = function() {
            auth.logout().then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    localStorageService.remove('user');
                    localStorageService.remove('authdata');

                    $state.go('auth.login');
                }
            });
        };

        $scope.addCivilStatus = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/setupmanager/civilstatus/civilstatus.modal.html',
                controller: 'civilStatusModal',
                backdrop : 'static',
                size: 'md'
            });

            modalInstance.result.then(function() {}, function() {});
        };

        $scope.addTaxExemption = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/accounting/wtax/taxexemption/taxexemption.html',
                controller: 'taxExemptionCtrl',
                backdrop: 'static' ,
                size: 'md'
            });

            modalInstance.result.then(function() {}, function() {});
        };

        $scope.showUserProfile = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/profile/profile.html',
                controller: 'profileCtrl',
                backdrop : 'static', 
                size: 'lg'
            });

            modalInstance.result.then(function() {}, function() {});
        };

        $scope.showAbout = function() {

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/about.modal.html',
                size: 'md',
                backdrop : 'static',
                scope: $scope
            });

            $scope.cancel = function() {
                modalInstance.dismiss('cancel');
            };

            modalInstance.result.then(function() {}, function() {});
        };
    }
})();