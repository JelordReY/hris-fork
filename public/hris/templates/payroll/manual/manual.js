(function () {
    'use strict';

    angular.module('hris')
        .controller('manualPayrollCtrl', manualPayrollCtrl);

    manualPayrollCtrl.$inject = ['$scope', '$state', 'toastr', 'localStorageService', '$location', '$uibModal', 'employee','$timeout','$stateParams'];

    function manualPayrollCtrl($scope, $state, toastr, localStorageService, $location, $uibModal, employee,$timeout,$stateParams) {
        $scope.currentPayroll = {};

        if (localStorageService.get('hris.payroll')) {
            $scope.currentPayroll = localStorageService.get('hris.payroll');
        }

        $scope.browseEmployee = function () {
            if (_.isEmpty($scope.currentPayroll)) {
                toastr.warning("WARNING!!! Unable to proceed.The system found that the you have not selected ACTIVE PAYROLL OPTION to get the employee ATTENDANCE." +
                    "Please contact the HUMARN RESOURCE OFFICER or SYSTEM ADMINISTRATOR for this matter", 'WARNING');
                return;
            }

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/humanresource/pickemployee.modal.html',
                controller: 'employeePickerCtrl',
                size: 'lg'
            });

            modalInstance.result.then(function (selectedItem) {
                if (selectedItem) {
                    $state.go('main.manualpayroll', {
                        emp_id: selectedItem.UUID
                    });
                }
            }, function () {});
        };

        $scope.clearEmployee = function () {
            $state.go('main.manualpayroll', {
                emp_id: null
            });
        };

        $scope.getEmployeeProfile = function (e_id) {
            employee.getEmployeeProfile(e_id, {}).then(function (data) {
                if (data.statusCode == 200 && data.response.success) {
                    var employeeD = data.response.result;
                    if (!_.isEmpty(employeeD)) {
                        $scope.employee = employeeD;
                        if ($scope.employee.Photo) {
                            $scope.profileImage = 'data:image/png;base64,' + $scope.employee.Photo;
                        }


                    }
                } else {
                    toastr.warning(data.response.msg +
                        "Please contact the HUMAN RESOURCE OFFICER or SYSTEM ADMINISTRATOR for this matter", 'WARNING');
                    $timeout(function () {
                        $state.go('main.manualpayroll', {
                            emp_id: null
                        });
                    }, 300);
                    return;
                }
            });
        };

        if ($stateParams.emp_id) {
            $scope.getEmployeeProfile($stateParams.emp_id);
        }
    }
})();