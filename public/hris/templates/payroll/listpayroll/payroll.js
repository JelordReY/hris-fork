(function () {
    'use strict';

    angular.module('hris')
        .controller('payrollListCtrl', payrollListCtrl);

    payrollListCtrl.$inject = ['$scope', 'attendance', 'localStorageService', '$uibModal', '$state','ngDialog','toastr'];

    function payrollListCtrl($scope, attendance, localStorageService, $uibModal, $state,ngDialog,toastr) {
        console.log('payrollListCtrl')
        $scope.overtimes = [];
        $scope.currentPayroll = {};

        if (localStorageService.get('hris.payroll')) {
            $scope.currentPayroll = localStorageService.get('hris.payroll');
        }

        attendance.getAllOvertime($scope.currentPayroll.UUID).then(function (data) {
            if (data.statusCode == 200 && data.response.success) {
                var overtimes = data.response.result;
                if (!_.isEmpty(overtimes)) {
                    $scope.overtimes = overtimes;
                }
            }
        });

        $scope.payrollOptions = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/attendance/DTR/payrolloption.modal.html',
                controller: 'dtrPayrollOptionCtrl',
                size: 'md'
            });

            modalInstance.result.then(function (selectedItem) {
                if (selectedItem) {
                    $scope.currentPayroll = selectedItem;
                    localStorageService.set('hris.payroll', selectedItem);

                    if ($stateParams.emp_id) {
                        $scope.getEmployeeProfile($stateParams.emp_id);
                    }
                }
            }, function () {});
        };

        $scope.viewEmployeeOvertime = function (EmployeeID,PayrollID) {
            var hashEmployeeID = CryptoJS.MD5(EmployeeID.toString());
            var hashPayrollID = CryptoJS.MD5(PayrollID.toString());
            $state.go('main.overtime_term', {
                emp_id: hashEmployeeID,
                payroll_id:hashPayrollID
            });
        };

        $scope.deleteOvertime = function (RefNo) {
            $scope.modal = {
                title: 'Employee Overtime',
                message: 'Are you sure to delete this overtime?'
            };

            ngDialog.openConfirm({
                templateUrl: './public/dialogs/custom.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function () {
                attendance.DeleteOvertime(RefNo).then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'SUCCESS');
                        $timeout(function () {
                            $state.reload();
                        }, 300);
                    } else {
                        toastr.error(data.response.msg, 'ERROR');
                        return;
                    }
                });
            });
        };
    }

})();