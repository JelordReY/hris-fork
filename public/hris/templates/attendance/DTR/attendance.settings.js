(function () {
    'use strict';

    angular.module('hris')
        .controller('dtrAttendanceSettingsCtrl', dtrAttendanceSettingsCtrl);

    dtrAttendanceSettingsCtrl.$inject = ['$scope', '$uibModalInstance', 'Payroll', '$timeout'];

    function dtrAttendanceSettingsCtrl($scope, $uibModalInstance, Payroll, $timeout) {
    
        $scope.selectEntry = function () {
            $uibModalInstance.close($scope.selected);
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

})();
