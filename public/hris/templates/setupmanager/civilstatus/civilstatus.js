(function() {
    'use strict';

    angular.module('hris')
        .controller('civilStatusModal', civilStatusModal)
        .controller('civilStatusModal2', civilStatusModal2);



    civilStatusModal.$inject = ['$scope', '$state', '$uibModal', 'civilstatus', 'ngTableParams', 'ngDialog', 'toastr', '$uibModalInstance'];

    function civilStatusModal($scope, $state, $uibModal, civilstatus, ngTableParams, ngDialog, toastr, $uibModalInstance) {
        $scope.IsDisable = true;
        console.log('civilStatusModal')
        $scope.civilstats = {};
        $scope.txtFind = '';
        $scope.selectedItem = {};

        $scope.tableParams = new ngTableParams({
            page: 1,
            count: 10,
            filter: $scope.txtFind,
            counts: []
        }, {
            total: 0,
            counts: [],
            getData: function($defer, params) {
                civilstatus.getAllCivilStatus({
                    action: 'picker',
                    limit: 1000
                }).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var civilstats = data.response.result;

                        if (params.filter().text) {
                            $scope.civilstats = $filter('filter')(civilstats, params.filter().text);
                        } else {
                            $scope.civilstats = civilstats;
                        }

                        _.each($scope.civilstats, function(row) {
                            row.selected = false;
                        });
                        params.total($scope.civilstats.length);
                        $defer.resolve($scope.civilstats.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    } else {
                        toastr.error(data.response.msg, 'Error');
                    }
                });
            }
        });

        $scope.addCivilStatus = function() {
            $scope.civilstatus = {};
            $scope.IsDisable = false;

        }

        $scope.updateCivilStatus = function(id) {
            $scope.IsDisable = false;
            civilstatus.getCivilStatusById(id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var civilstatus = data.response.result;
                    if (!_.isEmpty(civilstatus)) {
                        $scope.civilstatus = civilstatus;
                    }
                }
            });
        }

        $scope.Reload = function() {
            $scope.tblCivilStatus.reload();
        };

        $scope.deleteCivilStatus = function(id) {
            ngDialog.openConfirm({
                templateUrl: './public/dialogs/delete.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                civilstatus.deleteCivilStatus(id).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success')
                        $scope.tblCivilStatus.reload();
                    }
                });
            });
        }


        $scope.addEntry = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/setupmanager/civilstatus/civilstatus.modal2.html',
                controller: 'civilStatusModal2',
                resolve: {
                    civilstat_id: function() {
                        return null
                    }
                },
                size: 'lg'
            });

            modalInstance.result.then(function(selectedItem) {

            }, function() {});
        };

        $scope.cancel = function() {
            $uibModalInstance.close('close');
        };


        $scope.saveEntry = function() {
            if ($scope.civilstatus)
                if ($scope.civilstatus.cs_id) {
                    civilstatus.updateCivilStatus($scope.civilstatus.cs_id, $scope.civilstatus).then(function(data) {
                        if (data.statusCode == 200 && data.response.success) {
                            toastr.success(data.response.msg, 'Success');
                            $scope.tableParams.reload();
                        } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else {
                            toastr.error(data.msg, 'Error');
                        }
                    });
                } else {
                    civilstatus.saveEntry($scope.civilstatus).then(function(data) {
                        if (data.statusCode == 200 && data.response.success) {
                            toastr.success(data.response.msg, 'Success');
                            $scope.tableParams.reload();
                        } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else {
                            toastr.error(data.msg, 'Error');
                        }
                    });
                }
        };
    }




    civilStatusModal2.$inject = ['$scope', '$uibModalInstance', 'civilstatus', 'toastr', 'civilstat_id'];


    function civilStatusModal2($scope, $uibModalInstance, civilstatus, toastr, civilstat_id) {
        console.log('civilstat_id:', civilstat_id)
        $scope.civilstatus = {};
        if (civilstat_id) {
            civilstatus.getCivilStatusById(civilstat_id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    $scope.civilstatus = data.response.result;
                    console.log('$scope.civilstatus:', $scope.civilstatus);
                }
            });
        }

        /*$scope.saveEntry = function() {
            if (civilstat_id) {
                console.log('$scope.civilstatus:', $scope.civilstatus)
                civilstatus.updateCivilStatus(civilstat_id, $scope.civilstatus).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $uibModalInstance.close('save');
                    }

                });
            } else {
                console.log('civilstatus:', $scope.civilstatus)
                civilstatus.saveEntry($scope.civilstatus).then(function(data) {
                    console.log('data:', data)
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                    }
                });
            }
        };*/

        $scope.cancels = function() {
            $uibModalInstance.dismiss('cancel');
        };


    }

})();