(function() {
    'use strict';

    angular.module('hris')
        .controller('companyCtrl', companyCtrl)
        .controller('companyLogo', companyLogo);



    companyCtrl.$inject = ['$scope', '$state', 'company', 'toastr', 'localStorageService', '$uibModal', 'ngDialog', '$timeout'];

    function companyCtrl($scope, $state, company, toastr, localStorageService, $uibModal, ngDialog, $timeout) {
        $scope.company = {};

        company.getCompanyInfo().then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                var company = data.response.result;
                if (!_.isEmpty(company)) {
                    $scope.company = company;
                    if ($scope.company.ci_logo) {
                        $scope.profileImage = 'data:image/png;base64,' + $scope.company.ci_logo;

                    }
                    if ($scope.company.ci_mission) {
                        $scope.profileMission = 'data:image/png;base64,' + $scope.company.ci_mission;

                    }

                    if ($scope.company.ci_vision) {
                        $scope.profileVision = 'data:image/png;base64,' + $scope.company.ci_vision;

                    }

                     if ($scope.company.ci_history) {
                        $scope.profileHistory = 'data:image/png;base64,' + $scope.company.ci_history;

                    }
                     
                        
                    
                }
            }
        });



        $scope.uploadCompanyProfile = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/setupmanager/company/upload-companyprofile.html',
                controller: 'companyLogo',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    CompID: function() {
                        return $scope.company.ci_id
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $state.reload();
                }
            }, function() {});
        };

        $scope.uploadCompanyMission = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/setupmanager/company/upload-mission.html',
                controller: 'companyLogo',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    CompID: function() {
                        return $scope.company.ci_id
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $state.reload();
                }
            }, function() {});
        };

        $scope.uploadCompanyVision = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/setupmanager/company/upload-vision.html',
                controller: 'companyLogo',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    CompID: function() {
                        return $scope.company.ci_id
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $state.reload();
                }
            }, function() {});
        };

        $scope.uploadCompanyHistory = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/setupmanager/company/upload-history.html',
                controller: 'companyLogo',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    CompID: function() {
                        return $scope.company.ci_id
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $state.reload();
                }
            }, function() {});
        };

        $scope.DeleteCompanyProfile = function() {
            $scope.modal = {
                title: 'Company Logo',
                message: 'Remove this company logo ? '


            };
            ngDialog.openConfirm({
                templateUrl: './public/dialogs/custom.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'

            }).then(function() {
                company.deleteCompanyProfile($scope.company.ci_id).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $timeout(function() {
                            $state.reload();
                        }, 300);
                    } else {
                        toastr.error(data.response.msg, 'Error');
                        return;
                    }
                })
            });
        };



        var user = localStorageService.get('user');
        console.log('user:', user)
        $scope.saveEntry = function() {
            console.log('company:', $scope.company)
            company.updateCompanyInfo($scope.company).then(function(data) {
                console.log('data:', data)
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success');
                    $state.reload();


                }
            });
        };
        $scope.cancel = function() {    
            $uibModalInstance.dismiss('cancel');
        };
    }


})();

companyLogo.$inject = ['$scope', '$uibModalInstance', 'company', 'CompID', 'toastr', '$timeout', 'Upload'];

function companyLogo($scope, $uibModalInstance, company, CompID, toastr, $timeout, Upload) {
    $scope.newImage = false;
    $scope.picFile = null;
    $scope.croppedDataUrl = null;

    $scope.upload = function(file) {
        $scope.newImage = true;
    };

    $scope.saveEntry = function(arg) {
        var file = Upload.dataUrltoBlob($scope.croppedDataUrl, $scope.picFile.name);
        console.log('file:', file);
        Upload.upload({
                url: '/api/v1/company/' + CompID + '/image',
                data: {
                    comp_image: file,
                    arg: arg
                },
                method: 'PUT'
            }).then(function(resp) {
                if (resp.data.statusCode === 200 && resp.data.response.success) {
                    toastr.success('Image successfully uploaded', 'Success');
                    $timeout(function() {
                        $uibModalInstance.close('save');
                    }, 300);
                } else if (resp.data.statusCode === 400 && !resp.data.response.success && _.isArray(resp.data.response.result)) {
                    _.each(resp.data.response.result, function(msg) {
                        toastr.warning(msg.msg, 'Warning');
                    });
                } else {
                    toastr.error(resp.data.response.msg, 'Error');
                    return;
                }

            }, function(resp) {
                console.log('Error status: ' + resp.status);
            }, function(evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progressPercentage: ', progressPercentage);
            })
            .catch(function(err) {
                console.log('Error: ', err);
            });
    };

    $scope.DeleteCompanyProfile = function() {
        $scope.picFile = null;
        $scope.newImage = false;
    };


    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
}