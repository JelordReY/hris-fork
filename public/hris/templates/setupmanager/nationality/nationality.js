(function() {
    'use strict';

    angular.module('hris')
        .controller('nationalityCtrl', nationalityCtrl)
        .controller('nationalityModalCtrl', nationalityModalCtrl);

    nationalityCtrl.$inject = ['$scope', '$state', '$uibModal', 'nationality', 'ngTableParams', 'ngDialog', 'toastr' , '$filter'];

    function nationalityCtrl($scope, $state, $uibModal, nationality, ngTableParams, ngDialog, toastr , $filter) {
        console.log('nationalityCtrl')
        $scope.nationalies = {};
        $scope.dataSearch = {
            text: ''
        };
        $scope.selectedItem = {};

        $scope.tblNationality = new ngTableParams({
            page: 1, // show first page
            count: 10, // count per page
            filter: $scope.dataSearch,
            counts: []
        }, {
            total: 0,
            counts: [],
            getData: function($defer, params) {
                nationality.getAllNationality({
                    action: 'picker',
                    limit: 1000
                }).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var nationalies = data.response.result;

                        if (params.filter().text) {
                            $scope.nationalies = $filter('filter')(nationalies, params.filter().text);
                        } else {
                            $scope.nationalies = nationalies;
                        }

                        _.each($scope.nationalies, function(row) {
                            row.selected = false;
                        });
                        console.log('$scope.nationalies;', $scope.nationalies)
                        params.total($scope.nationalies.length);
                        $defer.resolve($scope.nationalies.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    } else {
                        toastr.error(data.response.msg, 'Error');
                    }
                });
            }
        });
        $scope.Reload = function() {
            $scope.tblNationality.reload();
        };
        $scope.searchNationality = function(){
           $scope.tblNationality.reload();
        }
        $scope.deleteNationality = function(id) {
            console.log('id:', id)
            ngDialog.openConfirm({
                templateUrl: './public/dialogs/delete.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                nationality.deleteNationality(id).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.Successccess(data.response.msg, 'Success')
                        $scope.tblNationality.reload();
                    }
                });
            });
        }
        $scope.updateNationality = function(id) {
            console.log('id:', id)
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/setupmanager/nationality/nationality.modal.html',
                controller: 'nationalityModalCtrl',
                resolve: {
                    nat_id: function() {
                        return id;
                    },

                },
                size: 'md'
            });
            $scope.cancel = function() {
                modalInstance.dismiss('cancel');
            };


        }
        $scope.addEntry = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/setupmanager/nationality/nationality.modal.html',
                controller: 'nationalityModalCtrl',
                resolve: {
                    nat_id: function() {
                        return null
                    }
                },
                size: 'md'
            });

            modalInstance.result.then(function(selectedItem) {

            }, function() {});
        };
    }
    nationalityModalCtrl.$inject = ['$scope', '$uibModalInstance', 'nationality', 'toastr', 'nat_id'];


    function nationalityModalCtrl($scope, $uibModalInstance, nationality, toastr, nat_id) {
        console.log('nationalityModalCtrl')
        console.log('nat_id:', nat_id)
        $scope.nationality = {};
        if (nat_id) {
            nationality.getNationalityById(nat_id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    $scope.nationality = data.response.result;
                    console.log('$scope.nationality:', $scope.nationality);

                }
            });
        }

        $scope.saveEntry = function() {
            if (nat_id) {
                console.log('$scope.nationality:', $scope.nationality)
                nationality.updateNationality(nat_id, $scope.nationality).then(function(data) {
                   if (data.statusCode == 200 && data.response.success) {
                            toastr.success(data.response.msg, 'Success');
                            $uibModalInstance.close('save');
                        } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else {
                            toastr.error(data.msg, 'Error');
                        }
                    });
                } else {

                console.log('nationality:', $scope.nationality)
                nationality.saveEntry($scope.nationality).then(function(data) {
                    console.log('data:', data)
                   if (data.statusCode == 200 && data.response.success) {
                            toastr.success(data.response.msg, 'Success');
                            $uibModalInstance.close('save');
                        } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else {
                            toastr.error(data.msg, 'Error');
                        }
                    });
                }
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };


    }





})();