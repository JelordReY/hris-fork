(function() {
    'use strict';

    angular.module('hris')
        .controller('branchesCtrl', branchesCtrl)
        .controller('branchModalCtrl', branchModalCtrl);

    branchesCtrl.$inject = ['$scope', '$state', '$uibModal', 'branch', 'ngTableParams', 'ngDialog', 'toastr', '$filter'];

    function branchesCtrl($scope, $state, $uibModal, branch, ngTableParams, ngDialog, toastr, $filter) {
        $scope.branches = [];
        $scope.dataSearch = {
            text: ''
        };
        $scope.selectedItem = {};
        $scope.tableParams = new ngTableParams({
            page: 1, // show first page
            count: 10, // count per page
            filter: $scope.dataSearch,
            counts: []
        }, {
            total: 0,
            counts: [],
            getData: function($defer, params) {
                branch.getAllBranch({
                    action: 'picker',
                    limit: 1000
                }).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var branches = data.response.result;

                        if (params.filter().text) {
                            $scope.branches = $filter('filter')(branches, params.filter().text);
                        } else {
                            $scope.branches = branches;
                        }

                        _.each($scope.branches, function(row) {
                            row.selected = false;
                        });
                        params.total($scope.branches.length);
                        $defer.resolve($scope.branches.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    } else {
                        toastr.error(data.response.msg, 'Error');
                    }
                });
            }
        });


        $scope.Reload = function() {
            $scope.tableParams.reload();
        };

        $scope.deleteBranch = function(id) {
            console.log('id:', id)
            ngDialog.openConfirm({
                templateUrl: './public/dialogs/delete.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                console.log('DELETE:')
                branch.deleteBranch(id).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success')
                        $scope.tableParams.reload();

                    }
                });
            });
        };

        $scope.updateBranch = function(id) {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/setupmanager/branch/branch.modal.html',
                controller: 'branchModalCtrl',
                resolve: {
                    branch_id: function() {
                        return id;
                    },
                },
                size: 'md'
            });

            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $scope.tableParams.reload();
                }
            }, function() {});
        }
        $scope.searchBranches = function() {
            $scope.tableParams.reload();
        }

        $scope.addEntry = function() {
            console.log('hello error')
            $scope.branch = {};
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/setupmanager/branch/branch.modal.html',
                controller: 'branchModalCtrl',
                resolve: {
                    branch_id: function() {
                        return null
                    }
                },
                size: 'md'
            });

            modalInstance.result.then(function(selectedItem) {}, function() {});
        };
    }

    branchModalCtrl.$inject = ['$scope', '$uibModalInstance', 'branch', 'toastr', 'branch_id', '$timeout'];

    function branchModalCtrl($scope, $uibModalInstance, branch, toastr, branch_id, $timeout) {
        $scope.branch = {};

        if (branch_id) {
            branch.getBranchId(branch_id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var branch = data.response.result;
                    if (!_.isEmpty(branch)) {
                        $scope.branch = branch;
                    }
                }
            });
        }

        $scope.saveEntry = function() {
            if (branch_id) {
                console.log('$scope.branch:', $scope.branch)
                branch.updateBranch(branch_id, $scope.branch).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $timeout(function() {
                            $uibModalInstance.close('save');
                        }, 300);
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            } else {
                branch.saveEntry($scope.branch).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $timeout(function() {
                            $uibModalInstance.close('save');
                        }, 300);
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            }
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };

    }
})();