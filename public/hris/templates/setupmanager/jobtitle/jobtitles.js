(function() {
    'use strict';

    angular.module('hris')
        .controller('jobpositionsCtrl', jobpositionsCtrl)
        .controller('jobpositionModalCtrl', jobpositionModalCtrl)
        .controller('jobGroupCtrl', jobGroupCtrl)
        .controller('jobClassCtrl', jobClassCtrl);

    jobpositionsCtrl.$inject = ['$scope', '$state', '$uibModal', 'jobposition', 'ngTableParams', 'ngDialog', 'toastr', '$filter'];

    function jobpositionsCtrl($scope, $state, $uibModal, jobposition, ngTableParams, ngDialog, toastr, $filter) {

        console.log('jobpositionsCtrl')
        $scope.group = {};
        $scope.dataSearch = {
            text: ''
        };
        $scope.selectedItem = {};

        $scope.tblJobPosClass = new ngTableParams({
            page: 1,
            count: 10,
            filter: $scope.dataSearch,
            counts: []
        }, {
            total: 0,
            counts: [],
            getData: function($defer, params) {
                jobposition.getAllJobPosition({
                    action: 'picker',
                    limit: 1000
                }).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var group = data.response.result;

                        if (params.filter().text) {
                            $scope.group = $filter('filter')(group, params.filter().text);
                        } else {
                            $scope.group = group;
                        }


                        _.each($scope.group, function(row) {
                            row.selected = false;
                        });
                        console.log('$scope.group;', $scope.group)
                        params.total($scope.group.length);
                        $defer.resolve($scope.group.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    } else {
                        toastr.error(data.response.msg, 'Error');
                    }
                });
            }
        });


        $scope.searchJobs = function() {
            $scope.tblJobPosClass.reload();
        }

        $scope.Reload = function() {
            $scope.tblJobPosClass.reload();
        };
        $scope.deleteJOP = function(id) {
            console.log('id:', id)
            ngDialog.openConfirm({
                templateUrl: './public/dialogs/delete.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                console.log('Nigana ba :')
                jobposition.deleteJobPosition(id).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success')
                        $scope.tblJobPosClass.reload();
                    }
                });
            });
        }


        $scope.updateJOBT = function(id) {
            console.log('id:', id)
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/setupmanager/jobtitle/jobtitle.modal.html',
                controller: 'jobpositionModalCtrl',
                resolve: {
                    jp_id: function() {
                        return id;
                    },

                },
                size: 'lg'
            });
            $scope.cancel = function() {
                modalInstance.dismiss('cancel');
            };


        }

        $scope.addGroupJob = function(id) {
            console.log('id:', id)
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/setupmanager/jobtitle/position.group.modal.html',
                controller: 'jobGroupCtrl',
                resolve: {
                    jp_id: function() {
                        return id;
                    },
                },
                size: 'md'
            });
            $scope.cancel = function() {
                modalInstance.dismiss('cancel');
            };


        }



        $scope.addClassJob = function(id) {
            console.log('id:', id)
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/setupmanager/jobtitle/position.class.modal.html',
                controller: 'jobClassCtrl',
                resolve: {
                    jp_id: function() {
                        return id;
                    },
                },
                size: 'md'
            });
            $scope.cancel = function() {
                modalInstance.dismiss('cancel');
            };


        }

        $scope.addEntry = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/setupmanager/jobtitle/jobtitle.modal.html',
                controller: 'jobpositionModalCtrl',
                resolve: {
                    jp_id: function() {
                        return null
                    }
                },
                size: 'lg'
            });

            modalInstance.result.then(function(selectedItem) {

            }, function() {});
        };
    }

    jobpositionModalCtrl.$inject = ['$scope', '$uibModalInstance', 'jobposition', 'jp_id', 'toastr', 'ngTableParams'];

    function jobpositionModalCtrl($scope, $uibModalInstance, jobposition, jp_id, toastr, ngTableParams) {
        console.log('jobpositionModalCtrl:')
        console.log('jp_id:', jp_id)
        $scope.jobpostgroup = [];
        $scope.jobpositions = [];
        $scope.jobposition = {};
        if (jp_id) {
            jobposition.getJobPositionById(jp_id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    $scope.jobposition = data.response.result;
                    console.log('$scope.jobposition:', $scope.jobposition);
                }
            });
        }
        jobposition.getAllGroup().then(function(data) {
            console.log('data:', data)
            if (data.statusCode == 200 && data.response.success) {
                $scope.jobpostgroup = data.response.result;
                console.log('data:', $scope.jobpostgroup);
            }

        })
        jobposition.getAllClass().then(function(data) {
            console.log('data:', data)
            if (data.statusCode == 200 && data.response.success) {
                $scope.jobpositions = data.response.result;
                console.log('data:', $scope.jobpositions);
            }

        })


        $scope.saveEntry = function() {
                if (jp_id) {
                    console.log('$scope.jobposition:', $scope.jobposition)
                    jobposition.updateJobPosition(jp_id, $scope.jobposition).then(function(data) {
                        if (data.statusCode == 200 && data.response.success) {
                            toastr.success(data.response.msg, 'Success');
                            $uibModalInstance.close('save');
                        } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else {
                            toastr.error(data.msg, 'Error');
                        }
                    });
                } else {

                    console.log('jobposition:', $scope.jobposition)
                    jobposition.saveEntry($scope.jobposition).then(function(data) {
                        console.log('data:', data)
                        if (data.statusCode == 200 && data.response.success) {
                            toastr.success(data.response.msg, 'Success');
                            $uibModalInstance.close('save');
                        } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else {
                            toastr.error(data.msg, 'Error');
                        }
                    });
                }
            },

            $scope.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            };

    }

    jobGroupCtrl.$inject = ['$scope', '$state', '$uibModalInstance', 'jobposition', 'jp_id', 'toastr', 'ngTableParams', 'ngDialog'];

    function jobGroupCtrl($scope, $state, $uibModalInstance, jobposition, jp_id, toastr, ngTableParams, ngDialog) {
        $scope.jgroupdata = [];
        $scope.IsDisable = true;
        $scope.txtFind = '';
        $scope.selectedItem = {};
        $scope.tableParams = new ngTableParams({
            page: 1, // show first page
            count: 5, // count per page
            counts: []
        }, {
            total: 0,
            counts: [],
            getData: function($defer, params) {
                jobposition.getAllGroup({
                    action: 'picker',
                    limit: 1000
                }).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var jgroupdata = data.response.result;

                        if ($scope.txtFind.length > 0) {
                            $scope.jgroupdata = $filter('filter')(jgroupdata, $scope.txtFind);
                        } else {
                            $scope.jgroupdata = jgroupdata;
                        }

                        _.each($scope.jgroupdata, function(row) {
                            row.selected = false;
                        });
                        console.log('$scope.jgroupdata;', $scope.jgroupdata)
                        params.total($scope.jgroupdata.length);
                        $defer.resolve($scope.jgroupdata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    } else {
                        toastr.error(data.response.msg, 'Error');
                    }
                });
            }
        });

        $scope.updateGroupData = function(id) {
            $scope.IsDisable = false;
            console.log('id:', id)
            if (id) {
                jobposition.getGroupById(id).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        $scope.groupdata = data.response.result;
                        if ($scope.groupdata.length == 0) {
                            $scope.groupdata = {};
                        }
                        console.log('$scope.groupdata:', $scope.groupdata);
                    }
                });
            }

        }

        $scope.deleteJobGroup = function(id) {
            console.log('id:', id)
            ngDialog.openConfirm({
                templateUrl: './public/dialogs/delete.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                jobposition.deleteGroup(id).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success')
                        $scope.tableParams.reload();
                    }
                });
            });
        }

        $scope.saveEntry2 = function() {
            if ($scope.groupdata)
                if ($scope.groupdata.GroupID) {
                    console.log('$scope.groupdata:', $scope.groupdata)
                    jobposition.updateGroup($scope.groupdata.GroupID, $scope.groupdata).then(function(data) {
                        if (data.statusCode == 200 && data.response.success) {
                            toastr.success(data.response.msg, 'Success');
                            $scope.tableParams.reload();
                        } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else {
                            toastr.error(data.msg, 'Error');
                        }
                    });
                } else {

                    console.log('groupdata:', $scope.groupdata)
                    jobposition.saveEntry2($scope.groupdata).then(function(data) {
                        console.log('data:', data)
                        if (data.statusCode == 200 && data.response.success) {
                            toastr.success(data.response.msg, 'Success');
                            $scope.tableParams.reload();
                        } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else {
                            toastr.error(data.msg, 'Error');
                        }
                    });
                }
        };
        $scope.addJGroup = function() {
            $scope.groupdata = {};
            $scope.IsDisable = false;

        }

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };




    }

    jobClassCtrl.$inject = ['$scope', '$uibModalInstance', 'jobposition', 'jp_id', 'toastr', 'ngTableParams', 'ngDialog'];


    function jobClassCtrl($scope, $uibModalInstance, jobposition, jp_id, toastr, ngTableParams, ngDialog) {
        $scope.IsDisable = true;
        $scope.selecteddata = '';
        $scope.txtFind = '';
        $scope.selectedItem = {};
        $scope.tableParams = new ngTableParams({
            page: 1, // show first page
            count: 5, // count per page
            counts: []
        }, {
            total: 0,
            counts: [],
            getData: function($defer, params) {
                jobposition.getAllClass({
                    action: 'picker',
                    limit: 1000
                }).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var classdata = data.response.result;

                        if ($scope.txtFind.length > 0) {
                            $scope.classdata = $filter('filter')(classdata, $scope.txtFind);
                        } else {
                            $scope.classdata = classdata;
                        }

                        _.each($scope.classdata, function(row) {
                            row.selected = false;
                        });
                        console.log('$scope.classdata;', $scope.classdata)
                        params.total($scope.classdata.length);
                        $defer.resolve($scope.classdata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    } else {
                        toastr.error(data.response.msg, 'Error');
                    }
                });
            }
        });

        $scope.updateClassData = function(id) {
            $scope.IsDisable = false;
            console.log('id:', id)
            if (id) {
                jobposition.getClassById(id).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        $scope.classdata = data.response.result;
                        if ($scope.classdata.length == 0) {
                            $scope.classdata = {};
                        }
                        console.log('$scope.classdata:', $scope.classdata);
                    }
                });
            }

        }

        $scope.deleteClassification = function(id) {
            console.log('id:', id)
            ngDialog.openConfirm({
                templateUrl: './public/dialogs/delete.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                jobposition.deleteClass(id).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success')
                        $scope.tableParams.reload();
                    }
                });
            });
        }

        $scope.saveEntry3 = function() {
            if ($scope.classdata)
                if ($scope.classdata.IndexID) {
                    console.log('$scope.classdata:', $scope.classdata)
                    jobposition.updateClass($scope.classdata.IndexID, $scope.classdata).then(function(data) {
                        if (data.statusCode == 200 && data.response.success) {
                            toastr.success(data.response.msg, 'Success');
                            $scope.tableParams.reload();
                        } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else {
                            toastr.error(data.msg, 'Error');
                        }
                    });
                } else {

                    console.log('classdata:', $scope.classdata)
                    jobposition.saveEntry3($scope.classdata).then(function(data) {
                        console.log('data:', data)
                        if (data.statusCode == 200 && data.response.success) {
                            toastr.success(data.response.msg, 'Success');
                            $scope.tableParams.reload();
                        } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else {
                            toastr.error(data.msg, 'Error');
                        }
                    });
                }
        };

        $scope.addClass = function() {
            $scope.classdata = {};
            $scope.IsDisable = false;

        }


        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };



    }




})();