(function() {
    'use strict';

    angular.module('hris')
        .controller('emptypesCtrl', emptypesCtrl)
        .controller('empTypesModalCtrl', empTypesModalCtrl);

    emptypesCtrl.$inject = ['$scope', '$state', '$uibModal', 'employmentstatus', 'ngTableParams', 'ngDialog', 'toastr', '$filter'];

    function emptypesCtrl($scope, $state, $uibModal, employmentstatus, ngTableParams, ngDialog, toastr, $filter) {

        console.log('emptypesCtrl')
        $scope.employmentstatuses = [];
        $scope.dataSearch = {
            text: ''
        };
        $scope.selectedItem = {};

        $scope.tblEmploymentStatus = new ngTableParams({
            page: 1, // show first page
            count: 10, // count per page
            filter: $scope.dataSearch,
            counts: []
        }, {
            total: 0,
            counts: [],
            getData: function($defer, params) {
                employmentstatus.getAllEmploymentStatus({
                    action: 'picker',
                    limit: 1000
                }).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var employmentstatuses = data.response.result;

                        if (params.filter().text) {
                            $scope.employmentstatuses = $filter('filter')(employmentstatuses, params.filter().text);
                        } else {
                            $scope.employmentstatuses = employmentstatuses;
                        }

                        _.each($scope.employmentstatuses, function(row) {
                            row.selected = false;
                        });
                        console.log('$scope.employmentstatuses;', $scope.employmentstatuses)
                        params.total($scope.employmentstatuses.length);
                        $defer.resolve($scope.employmentstatuses.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    } else {
                        toastr.error(data.response.msg, 'Error');
                    }
                });
            }
        });
        $scope.Reload = function() {
            $scope.tblEmploymentStatus.reload();
        };
        $scope.searchEmployment = function() {
            $scope.tblEmploymentStatus.reload();
        }
        $scope.deleteEmploymentStatus = function(id) {
            console.log('id:', id)
            ngDialog.openConfirm({
                templateUrl: './public/dialogs/delete.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                employmentstatus.deleteEmploymentStatus(id).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success')
                        $scope.tblEmploymentStatus.reload();
                    }
                });
            });
        }

        $scope.updateEmploymentStatus = function(id) {
            console.log('id:', id)
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/setupmanager/employmenttype/type.modal.html',
                controller: 'empTypesModalCtrl',
                resolve: {
                    empstat_id: function() {
                        return id;
                    },

                },
                size: 'md'
            });
            $scope.cancel = function() {
                modalInstance.dismiss('cancel');
            };


        }


        $scope.addEntry = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/setupmanager/employmenttype/type.modal.html',
                controller: 'empTypesModalCtrl',
                resolve: {
                    empstat_id: function() {
                        return null
                    }
                },
                size: 'md'
            });

            modalInstance.result.then(function(selectedItem) {

            }, function() {});
        };
    }

    empTypesModalCtrl.$inject = ['$scope', '$uibModalInstance', 'employmentstatus', 'toastr', 'empstat_id'];

    function empTypesModalCtrl($scope, $uibModalInstance, employmentstatus, toastr, empstat_id) {
        console.log('empTypesModalCtrl')
        console.log('empstat_id:', empstat_id)
        $scope.employmentstatus = {};
        if (empstat_id) {
            employmentstatus.getEmploymentStatusById(empstat_id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    $scope.employmentstatus = data.response.result;
                    console.log('$scope.employmentstatus:', $scope.employmentstatus);

                }
            });
        }

        $scope.saveEntry = function() {
            if (empstat_id) {
                console.log('$scope.employmentstatus:', $scope.employmentstatus)
                employmentstatus.updateEmploymentStatus(empstat_id, $scope.employmentstatus).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $uibModalInstance.close('save');
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            } else {


                console.log('employmentstatus:', $scope.employmentstatus)
                employmentstatus.saveEntry($scope.employmentstatus).then(function(data) {
                    console.log('data:', data)
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $uibModalInstance.close('save');
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            }
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };


    }

})();