(function () {
    'use strict';

    angular.module('hris')
        .controller('employeeModalCtrl', employeeModalCtrl);

    employeeModalCtrl.$inject = ['$scope', '$uibModalInstance', 'employee', 'civilstatus', 'employmentstatus', 'branch', 'division', 'taxexemption', 'department', 'jobposition', 'Payroll', '$state', 'toastr'];

    function employeeModalCtrl($scope, $uibModalInstance, employee, civilstatus, employmentstatus, branch, division, taxexemption, department, jobposition, Payroll, $state, toastr) {
        $scope.civilstatus = [];
        $scope.employmentstatus = [];
        $scope.branches = [];
        $scope.divisions = [];
        $scope.taxexemption = [];
        $scope.departments = [];
        $scope.jobpositions = [];
        $scope.paytypes = [];

        $scope.employee = {};
        $scope.employee.e_birthdate = new Date();
        $scope.employee.AppointedDate = new Date();
        $scope.employee.e_dateemployed = new Date();
        $scope.employee.e_gender = 'M';
        $scope.employee.IsTin = false;
        $scope.employee.TAX_FIXED = false;
        $scope.employee.IsSSS = false;
        $scope.employee.SSS_FIXED = false;
        $scope.employee.IsPh = false;
        $scope.employee.PH_FIXED = false;
        $scope.employee.IsPagIbig = false;
        $scope.employee.PAGIBIG_FIXED = false;
        $scope.employee.InActive = 0;

        $scope.popup1 = {
            opened: false
        };
        $scope.popup2 = {
            opened: false
        };
        $scope.popup3 = {
            opened: false
        };

        $scope.dateOptions = {
            formatYear: 'yyyy'
        };

        civilstatus.getAllCivilStatus().then(function (data) {
            if (data.statusCode == 200 && data.response.success) {
                var civilstatus = data.response.result;
                if (!_.isEmpty(civilstatus)) {
                    $scope.civilstatus = civilstatus;
                }
            }
        });

        employmentstatus.getAllEmploymentStatus().then(function (data) {
            if (data.statusCode == 200 && data.response.success) {
                var employmentstatus = data.response.result;
                if (!_.isEmpty(employmentstatus)) {
                    $scope.employmentstatus = employmentstatus;
                }
            }
        });

        branch.getAllBranch().then(function (data) {
            if (data.statusCode == 200 && data.response.success) {
                var branches = data.response.result;
                if (!_.isEmpty(branches)) {
                    $scope.branches = branches;
                }
            }
        });

        division.getAllDivisions().then(function (data) {
            if (data.statusCode == 200 && data.response.success) {
                var divisions = data.response.result;
                if (!_.isEmpty(divisions)) {
                    $scope.divisions = divisions;
                }
            }
        });

        taxexemption.getAllTaxExemptions().then(function (data) {
            if (data.statusCode == 200 && data.response.success) {
                var taxexemption = data.response.result;
                if (!_.isEmpty(taxexemption)) {
                    $scope.taxexemption = taxexemption;
                }
            }
        });

        department.getAllDepartments().then(function (data) {
            if (data.statusCode == 200 && data.response.success) {
                var departments = data.response.result;
                if (!_.isEmpty(departments)) {
                    $scope.departments = departments;
                }
            }
        });

        jobposition.getAllJobPosition().then(function (data) {
            if (data.statusCode == 200 && data.response.success) {
                var jobpositions = data.response.result;
                if (!_.isEmpty(jobpositions)) {
                    $scope.jobpositions = jobpositions;
                }
            }
        });

        Payroll.getAllPayTypes().then(function (data) {
            if (data.statusCode == 200 && data.response.success) {
                var paytypes = data.response.result;
                if (!_.isEmpty(paytypes)) {
                    $scope.paytypes = paytypes;
                }
            }
        });

        employee.getEmployeeProfile($scope.employee.Emp_UUID, {
            action: 'generate_id'
        }).then(function (data) {
            if (data.statusCode == 200 && data.response.success) {
                var EmployeeID = data.response.result;
                console.log('EmployeeID: ', EmployeeID);
                $scope.employee.e_idno = EmployeeID.EmployeeID;
            }
        });

        $scope.open1 = function() {
            $scope.popup1.opened = true;
        };
        $scope.open2 = function() {
            $scope.popup2.opened = true;
        };
        $scope.open3 = function() {
            $scope.popup3.opened = true;
        };

        $scope.genderChanged = function(){
            console.log('genderChanged: ',$scope.employee.e_gender);
        };

        $scope.chkSSS_CheckedChanged = function(){
            console.log('chkSSS_CheckedChanged: ',$scope.employee.IsSSS);
        };

        $scope.chkSSSAttrib_CheckedChanged = function(){
            console.log('chkSSSAttrib_CheckedChanged: ',$scope.employee.SSS_FIXED);
        };

        $scope.chkPhilHealth_CheckedChanged = function(){
            console.log('chkPhilHealth_CheckedChanged', $scope.employee.IsPh);
        };

        $scope.chkPHAttrib_CheckedChanged = function(){
            console.log('chkPHAttrib_CheckedChanged: ',$scope.employee.PH_FIXED);
        };

        $scope.chkPAGIBIG_CheckedChanged = function(){
            console.log('chkPAGIBIG_CheckedChanged: ',$scope.employee.IsPagIbig)
        };

        $scope.chkLoveAttrib_CheckedChanged = function(){
            console.log('chkLoveAttrib_CheckedChanged: ',$scope.employee.PAGIBIG_FIXED);
        };

        $scope.chkTIN_CheckedChanged = function(){
            console.log('chkTIN_CheckedChanged: ',$scope.employee.IsTin);
        };

        $scope.chkTINAttrib_CheckedChanged = function(){
            console.log('chkTINAttrib_CheckedChanged: ',$scope.employee.TAX_FIXED);
        };

        $scope.saveEntry = function () {
            console.log('employee: ', $scope.employee);
            
            if ($scope.employee.e_birthdate) {
                $scope.employee.e_birthdate = moment($scope.employee.e_birthdate).format('YYYY-MM-DD');
            }

            if($scope.employee.e_dateemployed){
                $scope.employee.e_dateemployed = moment($scope.employee.e_dateemployed).format('YYYY-MM-DD');
            }

            if($scope.employee.AppointedDate){
                $scope.employee.AppointedDate = moment($scope.employee.AppointedDate).format('YYYY-MM-DD');
            }

            
            employee.Add_Employee($scope.employee).then(function (data) {
                if (data.statusCode == 200 && data.response.success) {
                    var result = data.response.result;
                    $uibModalInstance.close(result);
                } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                    _.each(data.response.result, function (msg) {
                        toastr.warning(msg.msg, 'Warning');
                    });
                } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                    _.each(data.response.result, function (msg) {
                        toastr.warning(msg.msg, 'Warning');
                    });
                } else {
                    toastr.error(data.msg, 'Error');
                }
            });
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

})();