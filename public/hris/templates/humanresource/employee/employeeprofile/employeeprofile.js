(function(){
    'use strict';
    
    angular.module('hris')
        .controller('employeeProfileCtrl', employeeProfileCtrl)
        .controller('employeeChildrenCtrl', employeeChildrenCtrl);
    
    employeeChildrenCtrl.$inject = ['$scope', '$uibModalInstance', 'employee', '$timeout', 'cur_employee', 'toastr'];

    employeeProfileCtrl.$inject = ['$scope', '$uibModal', 'employee', 'civilstatus', 'employmentstatus', 'branch', 'division', 'taxexemption', 'department', 'jobposition', 'Payroll',
    '$stateParams', '$state', 'ngDialog', 'miscFctry', 'toastr', '$timeout', 'workschedules'
    ];
    
    function employeeChildrenCtrl($scope, $uibModalInstance, employee, $timeout, cur_employee, toastr) {
        $scope.dtpDOB = new Date();
        $scope.popup1 = {
            opened: false
        };
        $scope.fullname = '';
    
        $scope.dateOptions = {
            formatYear: 'yyyy'
        };
        console.log('cur_employee: ', cur_employee);
    
        $scope.open1 = function() {
            $scope.popup1.opened = true;
        };
    
        $scope.selectEntry = function() {
            var child = {
                ec_name: $scope.fullname,
                ec_dateofbirth: $scope.dtpDOB,
                e_id: cur_employee.e_idno
            };
            employee.addEmployeeChildren(cur_employee.Emp_UUID, child).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success');
                    $timeout(function() {
                        $uibModalInstance.close('save');
                    }, 300);
                }
            });
        };
    
        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
    
    
    function employeeProfileCtrl($scope, $uibModal, employee, civilstatus, employmentstatus, branch, division, taxexemption, department, jobposition, Payroll,
        $stateParams, $state, ngDialog, miscFctry, toastr, $timeout, workschedules) {
    
        $scope.civilstatus = [];
        $scope.employmentstatus = [];
        $scope.branches = [];
        $scope.divisions = [];
        $scope.taxexemption = [];
        $scope.departments = [];
        $scope.jobpositions = [];
        $scope.paytypes = [];
        $scope.religions = [];
        $scope.nationalities = [];
        $scope.workschedules = [];
        $scope.workDays = [];
    
        $scope.employee = {};
        $scope.employeeFamily = {};
        $scope.employeeChildren = [];
        $scope.education = {};
        $scope.employeeWorkScheds = [];
    
        $scope.IsDisabled = true;
    
        $scope.dt = new Date();
        $scope.popup1 = {
            opened: false
        };
    
        $scope.dateOptions = {
            formatYear: 'yyyy'
        };
    
    
        miscFctry.getAllReligion().then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                var religions = data.response.result;
                if (!_.isEmpty(religions)) {
                    $scope.religions = religions;
                }
            }
        });
    
        miscFctry.getAllNationality().then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                var nationalities = data.response.result;
                if (!_.isEmpty(nationalities)) {
                    $scope.nationalities = nationalities;
                }
            }
        });
    
        civilstatus.getAllCivilStatus().then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                var civilstatus = data.response.result;
                if (!_.isEmpty(civilstatus)) {
                    $scope.civilstatus = civilstatus;
                }
            }
        });
    
        employmentstatus.getAllEmploymentStatus().then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                var employmentstatus = data.response.result;
                if (!_.isEmpty(employmentstatus)) {
                    $scope.employmentstatus = employmentstatus;
                }
            }
        });
    
        branch.getAllBranch().then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                var branches = data.response.result;
                if (!_.isEmpty(branches)) {
                    $scope.branches = branches;
                }
            }
        });
    
        division.getAllDivisions().then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                var divisions = data.response.result;
                if (!_.isEmpty(divisions)) {
                    $scope.divisions = divisions;
                }
            }
        });
    
        taxexemption.getAllTaxExemptions().then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                var taxexemption = data.response.result;
                if (!_.isEmpty(taxexemption)) {
                    $scope.taxexemption = taxexemption;
                }
            }
        });
    
        department.getAllDepartments().then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                var departments = data.response.result;
                if (!_.isEmpty(departments)) {
                    $scope.departments = departments;
                }
            }
        });
    
        jobposition.getAllJobPosition().then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                var jobpositions = data.response.result;
                if (!_.isEmpty(jobpositions)) {
                    $scope.jobpositions = jobpositions;
                }
            }
        });
    
        Payroll.getAllPayTypes().then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                var paytypes = data.response.result;
                if (!_.isEmpty(paytypes)) {
                    $scope.paytypes = paytypes;
                }
            }
        });
    
        workschedules.getAllWorkSchedules().then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                var types = data.response.result;
                if (!_.isEmpty(types)) {
                    $scope.workschedules = types;
                }
            }
        });
    
        workschedules.getAllDays().then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                var workDays = data.response.result;
                if (!_.isEmpty(workDays)) {
                    $scope.workDays = workDays;
                }
            }
        });
    
        $scope.getEmployeeProfile = function(e_id) {
            employee.getEmployeeProfile(e_id, {}).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var employeeD = data.response.result;
                    if (!_.isEmpty(employeeD)) {
                        $scope.employee = employeeD;
                        
                        $scope.IsDisabled = false;
    
                        if (_.isEmpty($scope.employee.device_no)) {
                            toastr.warning("WARNING!!! Selected EMPLOYEE has no DEVICE NUMBER for Attendance purposes.", 'WARNING');
                        }
    
                        if ($scope.employee.Photo) {
                            $scope.profileImage = 'data:image/png;base64,' + $scope.employee.Photo;
                        }
                        $scope.dtpDOB = new Date($scope.employee.e_birthdate);
                        $scope.employee.work_schedule = _.find($scope.workschedules, {
                            'SchedID': $scope.employee.ScheduleID
                        });
                        if ($scope.employee.work_schedule) {
                            $scope.getEmployeeWorkSchedule($scope.employee.work_schedule.UUID);
                        }
    
                        employee.getEmployeeProfile($scope.employee.Emp_UUID, {
                            action: 'family'
                        }).then(function(data) {
                            if (data.statusCode == 200 && data.response.success) {
                                var employeeFamily = data.response.result;
                                if (!_.isEmpty(employeeFamily)) {
                                    $scope.employeeFamily = employeeFamily;
                                }
                            }
                        });
    
                        employee.getEmployeeProfile($scope.employee.Emp_UUID, {
                            action: 'children'
                        }).then(function(data) {
                            if (data.statusCode == 200 && data.response.success) {
                                var employeeChildren = data.response.result;
                                if (!_.isEmpty(employeeChildren)) {
                                    $scope.employeeChildren = employeeChildren;
                                }
                            }
                        });
    
                        employee.getEmployeeProfile($scope.employee.Emp_UUID, {
                            action: 'education'
                        }).then(function(data) {
                            if (data.statusCode == 200 && data.response.success) {
                                var education = data.response.result;
                                if (!_.isEmpty(education)) {
                                    $scope.education = education;
                                }
                            }
                        });
                    }
                }
            });
        };
    
        $scope.getEmployeeWorkSchedule = function(Sched_id) {
            $scope.employeeWorkScheds = [];
            workschedules.getWorkSchedulesTemplates(Sched_id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var workscheds = data.response.result;
                    if (!_.isEmpty(workscheds)) {
                        $scope.employeeWorkScheds.push({
                            day_id: 1,
                            morning_in: workscheds.IN01,
                            morning_out: workscheds.OUT01,
                            aft_in: workscheds.IN02,
                            aft_out: workscheds.OUT02,
                            break_in: workscheds.IN03,
                            break_out: workscheds.OUT03,
                            nonworking: false
                        }, {
                            day_id: 2,
                            morning_in: workscheds.IN11,
                            morning_out: workscheds.OUT11,
                            aft_in: workscheds.IN12,
                            aft_out: workscheds.OUT12,
                            break_in: workscheds.IN13,
                            break_out: workscheds.OUT13,
                            nonworking: false
                        }, {
                            day_id: 3,
                            morning_in: workscheds.IN21,
                            morning_out: workscheds.OUT21,
                            aft_in: workscheds.IN22,
                            aft_out: workscheds.OUT22,
                            break_in: workscheds.IN23,
                            break_out: workscheds.OUT23,
                            nonworking: false
                        }, {
                            day_id: 4,
                            morning_in: workscheds.IN31,
                            morning_out: workscheds.OUT31,
                            aft_in: workscheds.IN32,
                            aft_out: workscheds.OUT32,
                            break_in: workscheds.IN33,
                            break_out: workscheds.OUT33,
                            nonworking: false
                        }, {
                            day_id: 5,
                            morning_in: workscheds.IN41,
                            morning_out: workscheds.OUT41,
                            aft_in: workscheds.IN42,
                            aft_out: workscheds.OUT42,
                            break_in: workscheds.IN43,
                            break_out: workscheds.OUT43,
                            nonworking: false
                        }, {
                            day_id: 6,
                            morning_in: workscheds.IN51,
                            morning_out: workscheds.OUT51,
                            aft_in: workscheds.IN52,
                            aft_out: workscheds.OUT52,
                            break_in: workscheds.IN53,
                            break_out: workscheds.OUT53,
                            nonworking: false
                        }, {
                            day_id: 7,
                            morning_in: workscheds.IN61,
                            morning_out: workscheds.OUT61,
                            aft_in: workscheds.IN62,
                            aft_out: workscheds.OUT62,
                            break_in: workscheds.IN63,
                            break_out: workscheds.OUT63,
                            nonworking: false
                        });
                    }
                }
            });
        };
    
        if ($stateParams.e_id) {
            $scope.getEmployeeProfile($stateParams.e_id);
        }
    
        $scope.refreshData = function() {
            if ($stateParams.e_id) {
                $scope.employee = {};
                $scope.employeeFamily = {};
                $scope.employeeChildren = [];
    
                $scope.getEmployeeProfile($stateParams.e_id);
            }
        };
    
        $scope.open1 = function() {
            $scope.popup1.opened = true;
        };
    
        $scope.saveEntry = function() {
            $uibModalInstance.close('save');
        };
    
        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    
        $scope.browseEmployee = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/humanresource/pickemployee.modal.html',
                controller: 'employeePickerCtrl',
                size: 'lg'
            });
    
            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $state.go('main.employeeprofile', {
                        e_id: selectedItem.UUID
                    });
                }
            }, function() {});
        };
    
        $scope.addEntry = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/humanresource/employee/employee.entry.html',
                controller: 'employeeModalCtrl',
                size: 'lg'
            });
    
            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    var hashEmployeeID = CryptoJS.MD5(selectedItem.toString());
                    $state.go('main.employeeprofile', {
                        e_id: hashEmployeeID
                    });
                }
            }, function() {});
        };
    
        $scope.updateEmployeeID = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/humanresource/employeeID/employeeID.modal.html',
                controller: 'employeeIDModalCtrl',
                size: 'sm',
                resolve: {
                    EmpID: function() {
                        return $scope.employee.UUID
                    }
                }
            });
    
            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $state.reload();
                }
            }, function() {});
        }
    
        $scope.updateEmployeePicture = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/humanresource/employeepicture/upload.modal.html',
                controller: 'employeePictureModalCtrl',
                size: 'lg',
                resolve: {
                    EmpID: function() {
                        return $scope.employee.UUID;
                    }
                }
            });
    
            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $state.reload();
                }
            }, function() {});
        };
    
        $scope.clearEmployeePicture = function() {
            $scope.modal = {
                title: 'Employee Profile',
                message: 'Do you want to clear employee picture?'
            };
            ngDialog.openConfirm({
                templateUrl: './public/dialogs/custom.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                employee.Remove_Employee_Photo($scope.employee.UUID).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $timeout(function() {
                            $state.reload();
                        }, 300);
                    } else {
                        toastr.error(data.response.msg, 'Error');
                        return;
                    }
                })
            });
        };
    
        $scope.showEmployeeFingerprint = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/humanresource/employee/employeeprofile/fingerprint.modal.html',
                controller: 'employeeFingerPrintCtrl',
                size: 'lg',
                resolve: {
                    employeeD: function() {
                        return $scope.employee;
                    }
                }
            });
    
            modalInstance.result.then(function(selectedItem) {}, function() {});
        };
    
        $scope.updateEmployeeInfo = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/humanresource/employee/employee.entry2.html',
                controller: 'employeeUpdateModalCtrl',
                size: 'lg',
                resolve: {
                    employeeD: function() {
                        return $scope.employee;
                    }
                }
            });
    
            modalInstance.result.then(function(selectedItem) {
                if (selectedItem) {
                    $state.reload();
                }
            }, function() {});
        };
    
        $scope.copyAddress = function() {
            $scope.employee.Perhomeadd = $scope.employee.e_homeadd;
            $scope.employee.PerMunicipality = $scope.employee.e_cityadd;
            $scope.employee.PerProvince = $scope.employee.e_provincialadd;
            $scope.employee.PerZipcode = $scope.employee.e_zipcode;
        };
    
        $scope.insertEmployeeChildren = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/humanresource/employee/employeeprofile/children.modal.html',
                controller: 'employeeChildrenCtrl',
                size: 'md',
                resolve: {
                    cur_employee: function() {
                        return $scope.employee;
                    }
                }
            });
    
            modalInstance.result.then(function(selectedItem) {
                $scope.refreshData();
            }, function() {});
        };
    
        $scope.deleteEmployeeChildren = function(_id) {
            ngDialog.openConfirm({
                templateUrl: './public/dialogs/delete.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                employee.deleteEmployeeChildren($scope.employee.Emp_UUID, _id).then(function(data1) {
                    if (data1.statusCode == 200 && data1.response.success) {
                        toastr.success(data1.response.msg, 'Success');
    
                        $scope.refreshData();
                    } else {
                        toastr.error(data1.response.msg, 'Error');
                        return;
                    }
                });
            });
        };
    
        $scope.changeSchedule = function() {
            if ($scope.employee.work_schedule) {
                $scope.getEmployeeWorkSchedule($scope.employee.work_schedule.UUID);
            }
        };
    
        $scope.saveEmployeeWorkSchedule = function() {
            if ($stateParams.e_id) {
                $scope.modal = {
                    title: 'Work Schedule',
                    message: 'Do you want to apply this schedule?'
                };
                ngDialog.openConfirm({
                    templateUrl: './public/dialogs/custom.dialog.html',
                    scope: $scope,
                    className: 'ngdialog-theme-default'
                }).then(function() {
                    $scope.employee.ScheduleID = $scope.employee.work_schedule.SchedID;
                    employee.TagEmployeeWorkSchedule($scope.employee.Emp_UUID, $scope.employee.ScheduleID).then(function(data) {
                        if (data.statusCode == 200 && data.response.success) {
                            toastr.success(data.response.msg, 'Success');
                            $timeout(function() {
                                window.location.reload();
                            }, 300);
                        } else {
                            toastr.error(data.response.msg, 'Error');
                            return;
                        }
                    })
                });
            }
        };
    
        $scope.resetEmployeeWorkSchedule = function() {
            if ($stateParams.e_id) {
                $scope.modal = {
                    title: 'Work Schedule',
                    message: 'Do you want to reset this schedule?'
                };
                ngDialog.openConfirm({
                    templateUrl: './public/dialogs/custom.dialog.html',
                    scope: $scope,
                    className: 'ngdialog-theme-default'
                }).then(function() {
                    $scope.employee.ScheduleID = $scope.employee.work_schedule.SchedID;
                    employee.UnTagEmployeeWorkSchedule($scope.employee.Emp_UUID, $scope.employee.ScheduleID).then(function(data) {
                        if (data.statusCode == 200 && data.response.success) {
                            toastr.success(data.response.msg, 'Success');
                            $timeout(function() {
                                window.location.reload();
                            }, 300);
                        } else {
                            toastr.error(data.response.msg, 'Error');
                            return;
                        }
                    })
                });
            }
        };
    
        $scope.saveEmployee = function() {
            if ($stateParams.e_id) {
                $scope.modal = {
                    title: 'Update Employee',
                    message: 'Do you want to update the record on this employee?'
                };
                ngDialog.openConfirm({
                    templateUrl: './public/dialogs/custom.dialog.html',
                    scope: $scope,
                    className: 'ngdialog-theme-default'
                }).then(function() {
                    employee.updateEmployee($scope.employee.Emp_UUID, $scope.employee).then(function(data) {
                        if (data.statusCode == 200 && data.response.success) {
    
                            $scope.employeeFamily.EmployeeID = $scope.employee.e_idno;
                            employee.updateEmployeeFamily($scope.employee.Emp_UUID, $scope.employeeFamily).then(function(data1) {
                                if (data1.statusCode == 200 && data1.response.success) {
                                    toastr.success(data1.response.msg, 'Success');
    
                                    $timeout(function() {
                                        $scope.refreshData();
                                    }, 500);
                                } else {
                                    toastr.error(data1.response.msg, 'Error');
                                    return;
                                }
                            });
    
                            $scope.education.employee_id = $scope.employee.e_idno;
                            employee.updateEmployeeEducation($scope.employee.Emp_UUID, $scope.education).then(function(data1) {
                                if (data1.statusCode == 200 && data1.response.success) {
                                    toastr.success(data1.response.msg, 'Success');
    
                                    $timeout(function() {
                                        $scope.refreshData();
                                    }, 500);
                                } else {
                                    toastr.error(data1.response.msg, 'Error');
                                    return;
                                }
                            });
    
                        } else {
                            toastr.error(data.response.msg, 'Error');
                            return;
                        }
                    });
                });
            }
        };
    }
})();