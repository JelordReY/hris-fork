(function() {
    'use strict';

    angular.module('hris')
        .controller('employeePictureModalCtrl', employeePictureModalCtrl);

    employeePictureModalCtrl.$inject = ['$scope', '$uibModalInstance', 'employee', 'EmpID', 'toastr', '$timeout', 'Upload'];

    function employeePictureModalCtrl($scope, $uibModalInstance, employee, EmpID, toastr, $timeout, Upload) {
        $scope.newImage = false;
        $scope.picFile = null;
        $scope.croppedDataUrl = null;

        $scope.upload = function(file) {
            $scope.newImage = true;
        };

        $scope.saveEntry = function() {
            var file = Upload.dataUrltoBlob($scope.croppedDataUrl, $scope.picFile.name);
            console.log('file: ', file);
            Upload.upload({
                    url: '/api/v1/employees/' + EmpID + '/image',
                    data: {
                        emp_image:file
                    },
                    method: 'PUT'
                }).then(function(resp) {
                    if (resp.data.statusCode === 200 && resp.data.response.success) {
                        toastr.success('Image successfully uploaded', 'Success');
                        $timeout(function() {
                            $uibModalInstance.close('save');
                        }, 300);
                    } else if (resp.data.statusCode === 400 && !resp.data.response.success && _.isArray(resp.data.response.result)) {
                        _.each(resp.data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(resp.data.response.msg, 'Error');
                        return;
                    }

                }, function(resp) {
                    console.log('Error status: ' + resp.status);
                }, function(evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    console.log('progressPercentage: ', progressPercentage);
                })
                .catch(function(err) {
                    console.log('Error: ', err);
                });
        };

        $scope.removethumb = function() {
            $scope.picFile = null;
            $scope.newImage = false;
        };


        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})()