(function () {
    'use strict';

    angular.module('hris')
        .controller('employeeIDModalCtrl', employeeIDModalCtrl);

    employeeIDModalCtrl.$inject = ['$scope', '$uibModalInstance', 'employee', 'EmpID', 'toastr','$timeout'];

    function employeeIDModalCtrl($scope, $uibModalInstance, employee, EmpID, toastr,$timeout) {
        $scope.txtOldEmployee = '';
        $scope.txtNewEmployee = '';
        $scope.txtConfirm = '';
        $scope.employee = {};

        employee.getEmployeeProfile(EmpID, {}).then(function (data) {
            if (data.statusCode == 200 && data.response.success) {
                var employeeD = data.response.result;
                if (!_.isEmpty(employeeD)) {
                    $scope.employee = employeeD;
                    $scope.txtOldEmployee = employeeD.e_idno;
                }
            }
        });

        $scope.saveEntry = function () {
            console.log('$scope.txtOldEmployee.toLowerCase(): ',$scope.txtConfirm.toLowerCase());
            console.log('$scope.txtNewEmployee.toLowerCase(): ',$scope.txtNewEmployee.toLowerCase());
            if($scope.txtNewEmployee.toLowerCase() != $scope.txtConfirm.toLowerCase()){
                toastr.warning('New Employee ID does not match in confirmation.', 'WARNING');
                return;
            }

            employee.Update_Employee_ID(EmpID, {
                new_e_id: $scope.txtNewEmployee,
                old_e_id: EmpID
            }).then(function (data) {
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success');
                    $timeout(function(){
                        $uibModalInstance.close(EmpID);
                    },300);
                } else {
                    toastr.error(data.response.msg, 'Error');
                    return;
                }
            })
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
})()