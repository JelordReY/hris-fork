(function() {
    'use strict';

    angular.module('hris')
        .controller('employeePickerCtrl', employeePickerCtrl);

    employeePickerCtrl.$inject = ['$scope', 'employee', '$uibModalInstance', 'ngTableParams', '$timeout', '$filter'];

    function employeePickerCtrl($scope, employee, $uibModalInstance, ngTableParams, $timeout, $filter) {
        $scope.employees = [];
        $scope.txtSearch = {
            text: ''
        };
        $scope.selectedItem = {};

        $scope.tableParams = new ngTableParams({
            page: 1, // show first page
            count: 10, // count per page
            filter: $scope.txtSearch,
            counts: []
        }, {
            total: 0,
            counts: [],
            getData: function($defer, params) {
                $timeout(function() {
                    employee.getEmployeeMasterlist({
                        action: 'picker',
                        limit: 1000
                    }).then(function(data) {
                        if (data.statusCode == 200 && data.response.success) {
                            var employees = data.response.result;

                            if (params.filter().text) {
                                $scope.employees = $filter('filter')(employees, params.filter().text);
                            } else {
                                $scope.employees = employees;
                            }

                            _.each($scope.employees, function(row) {
                                row.selected = false;
                            });

                            params.total($scope.employees.length);
                            $defer.resolve($scope.employees.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                        } else {
                            toastr.error(data.response.msg, 'Error');
                        }
                    });
                }, 300);
            }
        });

        $scope.searchData = function() {
            console.log('searchData')
            $scope.tableParams.reload();
        };

        $scope.setSelectedItem = function(item) {
            if (item.selected) {
                $scope.selectedItem = {};
                item.selected = false;
            } else {
                _.each($scope.employees, function(row) {
                    row.selected = false;
                });
                $scope.selectedItem = item;
                item.selected = true;
            }
        };

        $scope.selectEntry = function() {
            $uibModalInstance.close($scope.selectedItem);
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();