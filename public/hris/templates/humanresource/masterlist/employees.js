(function() {
    'use strict';

    angular.module('hris')
        .controller('employeeCtrl', employeeCtrl);

    employeeCtrl.$inject = ['$scope', '$state', 'ngTableParams', 'employee', '$uibModal', 'branch', 'department', '$filter'];

    function employeeCtrl($scope, $state, ngTableParams, employee, $uibModal, branch, department, $filter) {
        $scope.employee = [];
        $scope.branches = [];
        $scope.action = {};
        $scope.selectedItem = {};
        $scope.txtSearch = {
            text: ''
        };

        async.waterfall([
            function(callback) {
                branch.getAllBranch().then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var branches = data.response.result;
                        if (!_.isEmpty(branches)) {
                            callback(null, branches);
                        }
                    }
                });
            },
            function(branches, callback) {
                department.getAllDepartments().then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var departments = data.response.result;
                        if (!_.isEmpty(departments)) {
                            callback(null, branches, departments);
                        }
                    }
                });
            },
            function(branches, departments, callback) {
                _.each(departments, function(row) {
                    row.tree_name = row.d_name;
                    row.tree_id = row.d_id;
                    row.tree_type = 'department';
                });

                _.each(branches, function(row) {
                    row.tree_name = row.branch_name;
                    row.tree_id = row.branch_id;
                    row.tree_type = 'branch';
                    row.departments = _.filter(departments, {
                        'branch_id': row.branch_id
                    }) || [];
                });
                $scope.branches = branches;
            }
        ]);

        $scope.tableParams = new ngTableParams({
            page: 1, // show first page
            count: 20, // count per page
            filter: $scope.txtSearch
        }, {
            getData: function($defer, params) {
                employee.getEmployeeMasterlist($scope.action).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var employees = data.response.result;
                        if (params.filter().text) {
                            $scope.employee = $filter('filter')(employees, params.filter().text);
                        } else {
                            $scope.employee = employees;
                        }
                        _.each($scope.employee, function(row) {
                            row.selected = false;
                            if (row.Photo) {
                                row.Photo = 'data:image/png;base64,' + row.Photo;
                            }
                        });
                        params.total($scope.employee.length);
                        $defer.resolve($scope.employee.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    }
                });
            }
        });

        $scope.$watch('mytree.currentNode', function(newObj, oldObj) {
            if ($scope.mytree && angular.isObject($scope.mytree.currentNode)) {
                $scope.action = {
                    action: $scope.mytree.currentNode.tree_type,
                    _id: $scope.mytree.currentNode.tree_id
                };
                $scope.tableParams.reload();
            }
        }, false);

        $scope.addEntry = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './public/hris/templates/humanresource/employee/employee.entry.html',
                controller: 'employeeModalCtrl',
                size: 'lg'
            });

            modalInstance.result.then(function(selectedItem) {}, function() {});
        };

        $scope.refreshData = function() {
            $scope.action = {};
            $scope.tableParams.reload();
        };

        $scope.searchData = function() {
            $scope.tableParams.reload();
        };

        $scope.setSelectedItem = function(item) {
            $state.go('main.employeeprofile', {
                e_id: item.UUID
            })
        };

        $scope.printData = function() {
            $state.go('main.masterlist_print', { action: $scope.action.action, _id: $scope.action._id });
        };
    }

})();