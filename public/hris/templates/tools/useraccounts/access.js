(function () {
    'use strict';

    angular.module('hris')
        .controller('userAccessCtrl', userAccessCtrl);

    userAccessCtrl.$inject = ['$scope', '$state', 'users', 'localStorageService', 'toastr', '$timeout', 'ngTableParams', '$stateParams', 'ngDialog'];

    function userAccessCtrl($scope, $state, users, localStorageService, toastr, $timeout, ngTableParams, $stateParams, ngDialog) {

        $scope.refreshData=function(){
            $scope.privileges = [];
            $scope.user = {};
            $scope.useraccess = [];

            async.waterfall([
                function(callback){
                    users.getAccessPrivileges().then(function (data) {
                        if (data.statusCode == 200 && data.response.success) {
                            var privileges = data.response.result;
                            if (!_.isEmpty(privileges)) {
                                $scope.privileges = privileges;
                            }
                            callback();
                        } else {
                            console.log('2')
                            toastr.error(data.response.msg, 'ERROR');
                            return;
                        }
                    });
                },
                function(callback){
                    if ($stateParams.user_id) {
                        users.GetUserByUserID($stateParams.user_id).then(function (data) {
                            if (data.statusCode == 200 && data.response.success) {
                                var user = data.response.result;
                                if (!_.isEmpty(user)) {
                                    $scope.user = user;
                                }
                                
                                users.getUserPrivilege($stateParams.user_id).then(function(data1){
                                    if (data1.statusCode == 200 && data1.response.success) {
                                        var access = data1.response.result;
                                        _.each($scope.privileges,function(row){
                                            var result = _.find(access,{'ID':row.ID});
                                            if(result){
                                                row.DefaultDelete = result.DefaultDelete;
                                                row.DefaultExport = result.DefaultExport;
                                                row.DefaultPrint = result.DefaultPrint;
                                                row.DefaultRead = result.DefaultRead;
                                                row.DefaultWrite = result.DefaultWrite;
                                                row.Inactive = result.Inactive;
                                            }
                                        });
                                    }
                                })
                            } else {
                                console.log('1')
                                toastr.error(data.response.msg, 'ERROR');
                                return;
                            }
                        })
                    }
                }    
            ]);
        };

        $scope.refreshData();

        $scope.saveAccess = function () {
            $scope.modal = {
                title: 'User Account Access',
                message: 'Are you sure to update this user account access?'
            };
            ngDialog.openConfirm({
                templateUrl: './public/dialogs/custom.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function () {
                async.eachSeries($scope.privileges, function (item, cb) {
                    users.setUserPrivilege($stateParams.user_id, item).then(function (data) {
                        if (data.statusCode == 200 && data.response.success) {
                            cb();
                        } else {
                            toastr.error(data.response.msg, 'ERROR');
                            return;
                        }
                    });
                }, function () {
                    toastr.success('Record Successfully saved', 'SUCCESS');
                    $state.reload();
                    return;
                });
            });
        };

        $scope.myFunction = function () {
            angular.element(document).ready(function () {
                $('#myTable').each(function () {
                    var Column_number_to_Merge = 1;
                    var Previous_TD = null;
                    var i = 1;
                    $("tbody", this).find('tr').each(function () {
                        var Current_td = $(this).find('td:nth-child(' + Column_number_to_Merge + ')');
                        if (Previous_TD == null) {
                            Previous_TD = Current_td;
                            i = 1;
                        } else if ($(Current_td).scope().row.ModuleName == $(Previous_TD).scope().row.ModuleName) {
                            Current_td.remove();
                            Previous_TD.attr('rowspan', i + 1);
                            i = i + 1;
                        } else {
                            Previous_TD = Current_td;
                            i = 1;
                        }
                    });
                });
            });
        };
    }
})();