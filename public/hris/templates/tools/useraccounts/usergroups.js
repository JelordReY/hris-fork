(function () {
    'use strict';

    angular.module('hris')
        .controller('userGroupsModalCtrl', userGroupsModalCtrl);

    userGroupsModalCtrl.$inject = ['$scope', '$state', 'users', 'localStorageService', 'toastr', '$timeout', '$uibModalInstance', 'ngTableParams', 'ngDialog'];

    function userGroupsModalCtrl($scope, $state, users, localStorageService, toastr, $timeout, $uibModalInstance, ngTableParams, ngDialog) {
        $scope.employee = {};
        $scope.group = {};
        $scope.IsDisable = true;

        $scope.tableParams = new ngTableParams({
            page: 1, // show first page
            count: 20, // count per page
            counts: []
        }, {
            total: 0,
            counts: [],
            getData: function ($defer, params) {
                users.GetAllUserGroups().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var usersD = data.response.result;
                        $scope.useraccounts = usersD;
                        params.total($scope.useraccounts.length);
                        $defer.resolve($scope.useraccounts.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    } else {
                        toastr.error(data.response.msg, 'ERROR');
                        returnl
                    }
                });
            }
        });

        $scope.refreshData = function(){
            $scope.tableParams.reload();
        };

        $scope.addUserGroup = function () {
            $scope.group = {};
            $scope.IsDisable = false;
        };

        $scope.updateUserGroup = function (UUID) {
            users.GetUserGroupByID(UUID).then(function (data) {
                if (data.statusCode == 200 && data.response.success) {
                    $scope.group = data.response.result;
                    $scope.IsDisable = false;
                } else {
                    toastr.error(data.response.msg, 'ERROR');
                    return;
                }
            });
        };

        $scope.deleteUserGroup = function (UUID) {
            $scope.modal = {
                title: 'User Groups',
                message: 'Are you sure to delete this user group?'
            };

            ngDialog.openConfirm({
                templateUrl: './public/dialogs/custom.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function () {
                users.Delete_User_Group(UUID).then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'SUCCESS');
                        $timeout(function () {
                            $scope.refreshData();
                        }, 300);
                    } else {
                        toastr.error(data.response.msg, 'ERROR');
                        return;
                    }
                });
            });
        };

        $scope.saveEntry = function () {
            if ($scope.group.GroupID) {
                users.Update_User_Group($scope.group.UUID, $scope.group).then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'SUCCESS');
                        $timeout(function () {
                            $scope.group = {};
                            $scope.IsDisable = true;
                            $scope.refreshData();
                        }, 300);
                    } else {
                        toastr.error(data.response.msg, 'ERROR');
                        return;
                    }
                });
            } else {
                users.Add_User_Group($scope.group).then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'SUCCESS');
                        $timeout(function () {
                            $scope.group = {};
                            $scope.IsDisable = true;
                            $scope.refreshData();
                        }, 300);
                    } else {
                        toastr.error(data.response.msg, 'ERROR');
                        return;
                    }
                });
            }
        };

        $scope.previewGroups = function(UUID){
            $uibModalInstance.close(UUID);
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();