'use strict';

angular.module('hris')
    .controller('sssCtrl', sssCtrl)
    .controller('sssDetailCtrl', sssDetailCtrl)
    .controller('sssModalCtrl', sssModalCtrl);

sssCtrl.$inject = ['$scope', '$state', 'toastr', 'auth', '$timeout', 'sss', 'ngTableParams', 'ngDialog', '$filter', '$uibModal', '$stateParams'];

function sssCtrl($scope, $state, toastr, auth, $timeout, sss, ngTableParams, ngDialog, $filter, $uibModal, $stateParams) {
    $scope.sssdata = [];
    $scope.IsDisable = true;
    $scope.dataSearch = {
        text: ''
    };
    $scope.selectedItem = {};
    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        filter: $scope.dataSearch,
        counts: []
    }, {
        total: 0,
        counts: [],
        getData: function($defer, params) {
            sss.getAllSSS({
                action: 'picker',
                limit: 1000
            }).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var sssdata = data.response.result;

                    if (params.filter().text) {
                        $scope.sssdata = $filter('filter')(sssdata, params.filter().text);
                    } else {
                        $scope.sssdata = sssdata;
                    }

                    _.each($scope.sssdata, function(row) {
                        row.selected = false;
                    });
                    console.log('$scope.sssdata;', $scope.sssdata)
                    params.total($scope.sssdata.length);
                    $defer.resolve($scope.sssdata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                } else {
                    toastr.error(data.response.msg, 'Error');
                }
            });
        }
    });



    $scope.viewSSSData = function(id) {
        $state.go('main.sss_detail', {
            sss_id: id
        });
    };

    $scope.updateSSS = function(id) {
        $scope.IsDisable = false;
        console.log('id:', id)
        if (id) {
            sss.getSSSByID(id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    $scope.sss = data.response.result;
                    console.log('$scope.sss:', $scope.sss);
                }
            });
        }


    }

    $scope.searchSSS = function() {
        $scope.tableParams.reload();

    };

    $scope.deleteSSSTable = function(id) {
        console.log('id:', id)
        ngDialog.openConfirm({
            templateUrl: './public/dialogs/delete.dialog.html',
            scope: $scope,
            className: 'ngdialog-theme-default'
        }).then(function() {
            sss.DelSSSTable(id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success')
                    $scope.tableParams.reload();

                }
            });
        });
    }


    $scope.saveEntry = function() {
        if ($scope.sss)
            if ($scope.sss.SSS_ID) {
                console.log('$scope.sss:', $scope.sss)
                sss.UpdateSSSTable($scope.sss.UUID, $scope.sss).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $uibModalInstance.close('save');
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            } else {

                console.log('sss:', $scope.sss)
                sss.saveEntry($scope.sss).then(function(data) {
                    console.log('data:', data)
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $uibModalInstance.close('save');
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            }
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };



    $scope.addSSS = function() {
        $scope.sss = {};
        $scope.IsDisable = false;


    };


}


sssDetailCtrl.$inject = ['$scope', '$state', 'toastr', 'auth', '$timeout', '$uibModal', '$stateParams', 'sss', 'ngTableParams', 'ngDialog', 'company', '$filter'];

function sssDetailCtrl($scope, $state, toastr, auth, $timeout, $uibModal, $stateParams, sss, ngTableParams, ngDialog, company, $filter) {
    $scope.sss = [];
    $scope.action = {};
    if ($stateParams.sss_id) {
        sss.getSSSByID($stateParams.sss_id).then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                var ss = data.response.result;
                console.log('ss: ', ss);
                if (!_.isEmpty(ss)) {
                    $scope.ss = ss;
                }
            }
        });
    }

    $scope.deleteSSSBracket = function(id) {
        console.log('id:', id)
        ngDialog.openConfirm({
            templateUrl: './public/dialogs/delete.dialog.html',
            scope: $scope,
            className: 'ngdialog-theme-default'
        }).then(function() {
            sss.DelSSSBracket(id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success')
                    $scope.tableSSSData.reload();

                }
            });
        });
    }

    $scope.addSSSBracket = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: './public/hris/templates/accounting/sss/sss.detail.modal.html',
            controller: 'sssModalCtrl',
            resolve: {
                sss_id: function() {
                    return $scope.ss.SSS_ID + (1000)
                }

            },
            size: 'md'
        });

        modalInstance.result.then(function(selectedItem) {
            $scope.refreshData();
        }, function() {});
    };

    $scope.updateSSSBracket = function(id) {
        console.log('id:', id)
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: './public/hris/templates/accounting/sss/sss.detail.modal.html',
            controller: 'sssModalCtrl',
            resolve: {
                sss_id: function() {
                    return id;
                },

            },
            size: 'md'
        });
        $scope.cancel = function() {
            modalInstance.dismiss('cancel');
        };


    }


    $scope.sssdetaildata = [];
    $scope.IsDisable = true;
    $scope.dataSearch = {
        text: ''
    }
    $scope.selectedItem = {};
    $scope.tableSSSData = new ngTableParams({
        page: 1, // show first page
        count: 100, // count per page
        filter: $scope.dataSearch,
        counts: []
    }, {
        total: 0,
        counts: [],
        getData: function($defer, params) {
            sss.getSSSAllDetails($stateParams.sss_id, {
                action: 'SSS',
                limit: 1000
            }).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var sssdetaildata = data.response.result;

                    if (params.filter().text) {
                        $scope.sssdetaildata = $filter('filter')(sssdetaildata, params.filter().text)
                    } else {
                        $scope.sssdetaildata = sssdetaildata;
                    }

                    _.each($scope.sssdetaildata, function(row) {
                        row.selected = false;
                    });
                    console.log('$scope.sssdetaildata;', $scope.sssdetaildata)
                    params.total($scope.sssdetaildata.length);
                    $defer.resolve($scope.sssdetaildata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                } else {
                    toastr.error(data.response.msg, 'Error');
                }
            });
        }
    });

    company.getCompanyInfo().then(function(data) {
        if (data.statusCode == 200 && data.response.success) {
            var company = data.response.result;
            if (!_.isEmpty(company)) {
                $scope.company = company;
                if ($scope.company.ci_logo) {
                    $scope.profileImage = 'data:image/png;base64,' + $scope.company.ci_logo;

                }
            }
        }
    });

    $scope.Refresh = function() {
        $scope.tableSSSData.reload();
        $scope.company = {};
        company.getCompanyInfo().then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                var company = data.response.result;
                if (!_.isEmpty(company)) {
                    $scope.company = company;
                    if ($scope.company.ci_logo) {
                        $scope.profileImage = 'data:image/png;base64,' + $scope.company.ci_logo;

                    }
                }
            }
        });

    }


    $scope.refreshData = function() {
        $scope.tableSSSData.reload();
    }

    $scope.printSSSBrackets = function() {
        $state.go('main.sssbracket_print', {
            sss_id: $scope.ss.UUID
        });
    };




}



sssModalCtrl.$inject = ['$scope', '$uibModalInstance', '$uibModal', 'sss_id', 'toastr', 'sss'];

function sssModalCtrl($scope, $uibModalInstance, $uibModal, sss_id, toastr, sss) {

    $scope.sssdat = {};
    if (sss_id) {
        sss.GetSSSBracketByID(sss_id).then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                $scope.sssdat = data.response.result;
                if ($scope.sssdat.length == 0) {
                    $scope.sssdat = {};
                }
                console.log('$scope.sssdat:', $scope.sssdat);

            }
        });
    }

    $scope.saveSSDetails = function() {
        if ($scope.sssdat)
            if ($scope.sssdat.sb_id) {
                console.log('$scope.sssdat:', $scope.sssdat)
                sss.UpdateSSSBracket($scope.sssdat.sb_id, $scope.sssdat).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $uibModalInstance.close('save');

                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            } else {

                console.log('sssdat:', $scope.sssdat)
                console.log('sss_id', sss_id)
                sss.saveBracket(sss_id, $scope.sssdat).then(function(data) {
                    console.log('data:', data)
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $uibModalInstance.close('save');
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            }
    };


    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };




}