'use strict';

angular.module('hris')
    .controller('taxModalCtrl', taxModalCtrl);


taxModalCtrl.$inject = ['$scope', 'auth', '$uibModalInstance', 'wtax', 'type', 'taxexemption', 'toastr', 'tax_id'];

function taxModalCtrl($scope, auth, $uibModalInstance, wtax, type, taxexemption, toastr, tax_id) {

    if (tax_id) {
        wtax.getTaxDetails(tax_id).then(function(data) {
            console.log('wtax:', wtax)
            console.log('tax_id:', tax_id)
            if (data.statusCode == 200 && data.response.success) {
                $scope.taxes = data.response.result;
                if ($scope.taxes.length == 0) {
                    $scope.taxes = {};
                }
                console.log('data:', data);
                console.log('$scope.taxes:', $scope.taxes);
            }
        });
    }

    $scope.taxtype = type;
    $scope.CategoryID = null;
    console.log('type:', type)

    if (type == 'monthly') {
        $scope.CategoryID = 1;
    } else if (type == 'semi-monthly') {
        $scope.CategoryID = 2;
    } else if (type == 'weekly') {
        $scope.CategoryID = 3;
    } else if (type == 'daily') {
        $scope.CategoryID = 4;
    }

    taxexemption.getAllTaxExemptions().then(function(data) {
        if (data.statusCode == 200 && data.response.success) {
            var taxexemption = data.response.result;
            if (!_.isEmpty(taxexemption)) {
                $scope.taxexemption = taxexemption;
            }
        }
    });

    $scope.saveTaxDetails = function() {
        if ($scope.taxes)
            if ($scope.taxes.IndexID) {
                $scope.taxes.CategoryID = $scope.CategoryID;
                console.log('$scope.taxes:', $scope.taxes)
                wtax.updateTaxDetails($scope.taxes.IndexID, $scope.taxes).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $uibModalInstance.close('save');
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            } else {

                $scope.taxes.CategoryID = $scope.CategoryID;
                console.log('taxes:', $scope.taxes)
                console.log('tax_id:', tax_id)
                wtax.saveHoldingTax(tax_id, $scope.taxes).then(function(data) {
                    console.log('data:', data)
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $uibModalInstance.close('save');
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            }
    };



    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
};