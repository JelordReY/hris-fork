'use strict';

angular.module('hris')
    .controller('taxPercentCtrl', taxPercentCtrl);

taxPercentCtrl.$inject = ['$scope', '$uibModalInstance', 'wtax', 'toastr', 'tax_id'];

function taxPercentCtrl($scope, $uibModalInstance, wtax, toastr, tax_id) {

    if (tax_id) {
        wtax.getTaxPercentByID(tax_id).then(function(data) {
            console.log('wtax:', wtax)
            console.log('tax_id:', tax_id)
            if (data.statusCode == 200 && data.response.success) {
                $scope.taxpercentage = data.response.result;
                if ($scope.taxpercentage.length == 0) {
                    $scope.taxpercentage = {};
                }
                console.log('$scope.taxpercentage:', $scope.taxpercentage);
            }
        });
    }

    console.log('wtax:', wtax)
    console.log('tax_id:', tax_id)
    $scope.saveTaxPercentage = function() {
        console.log('wtax:', wtax)
        console.log('tax_id:,', tax_id)
        if ($scope.taxpercentage)
            if ($scope.taxpercentage.IndexID) {
                console.log('$scope.taxpercentage:', $scope.taxpercentage)
                wtax.updateTaxPercent($scope.taxpercentage.IndexID, $scope.taxpercentage).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $uibModalInstance.close('save');
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            } else {

                console.log('taxpercentage:', $scope.taxpercentage)
                console.log('tax_id:', tax_id)
                wtax.saveTaxPercentage(tax_id, $scope.taxpercentage).then(function(data) {
                    console.log('data:', data)
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $uibModalInstance.close('save');
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            }
    };


    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
};