'use strict';

angular.module('hris')
    .controller('taxDetailCtrl', taxDetailCtrl);




taxDetailCtrl.$inject = ['$scope', '$state', 'toastr', 'wtax', '$timeout', '$uibModal', '$stateParams', 'ngTableParams', 'ngDialog'];

function taxDetailCtrl($scope, $state, toastr, wtax, $timeout, $uibModal, $stateParams, ngTableParams, ngDialog) {

    $scope.tax = {};
    $scope.taxsumcopy = [];
    $scope.taxsummary = {};
    $scope.IsDisable = true;
    $scope.txtFind = '';
    $scope.selectedItem = {};
    $scope.tableParamsDaily = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        counts: []
    }, {
        total: 0,
        counts: [],
        getData: function($defer, params) {
            wtax.getAllTaxSummary($stateParams.tax_id, {
                action: 'summary',
                limit: 1000
            }).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    $scope.taxsumcopy = angular.copy(data.response.result);
                    console.log('halloworld')
                    var taxsummary = data.response.result.daily;

                    if ($scope.txtFind.length > 0) {
                        $scope.taxsummary = $filter('filter')(taxsummary, $scope.txtFind);
                    } else {
                        $scope.taxsummary = taxsummary;
                    }

                    _.each($scope.taxsummary, function(row) {
                        row.selected = false;
                    });
                    console.log('$scope.taxsummary;', $scope.taxsummary)
                    params.total($scope.taxsummary.length);
                    $defer.resolve($scope.taxsummary.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                } else {
                    toastr.error(data.response.msg, 'Error');
                }
            });
        }
    });

    $scope.tableParamsWeekly = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        counts: []
    }, {
        total: 0,
        counts: [],
        getData: function($defer, params) {
            wtax.getAllTaxSummary($stateParams.tax_id, {
                action: 'summary',
                limit: 1000
            }).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var taxsummary = data.response.result.weekly;

                    if ($scope.txtFind.length > 0) {
                        $scope.taxsummary = $filter('filter')(taxsummary, $scope.txtFind);
                    } else {
                        $scope.taxsummary = taxsummary;
                    }

                    _.each($scope.taxsummary, function(row) {
                        row.selected = false;
                    });
                    console.log('$scope.taxsummary;', $scope.taxsummary)
                    params.total($scope.taxsummary.length);
                    $defer.resolve($scope.taxsummary.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                } else {
                    toastr.error(data.response.msg, 'Error');
                }
            });
        }
    });

    $scope.tableParamsSemiMonthly = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        counts: []
    }, {
        total: 0,
        counts: [],
        getData: function($defer, params) {
            wtax.getAllTaxSummary($stateParams.tax_id, {
                action: 'summary',
                limit: 1000
            }).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var taxsummary = data.response.result.semimonthly;

                    if ($scope.txtFind.length > 0) {
                        $scope.taxsummary = $filter('filter')(taxsummary, $scope.txtFind);
                    } else {
                        $scope.taxsummary = taxsummary;
                    }

                    _.each($scope.taxsummary, function(row) {
                        row.selected = false;
                    });
                    console.log('$scope.taxsummary;', $scope.taxsummary)
                    params.total($scope.taxsummary.length);
                    $defer.resolve($scope.taxsummary.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                } else {
                    toastr.error(data.response.msg, 'Error');
                }
            });
        }
    });

    $scope.tableParamsMonthly = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        counts: []
    }, {
        total: 0,
        counts: [],
        getData: function($defer, params) {
            wtax.getAllTaxSummary($stateParams.tax_id, {
                action: 'summary',
                limit: 1000
            }).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var taxsummary = data.response.result.monthly;

                    if ($scope.txtFind.length > 0) {
                        $scope.taxsummary = $filter('filter')(taxsummary, $scope.txtFind);
                    } else {
                        $scope.taxsummary = taxsummary;
                    }

                    _.each($scope.taxsummary, function(row) {
                        row.selected = false;
                    });
                    console.log('$scope.taxsummary;', $scope.taxsummary)
                    params.total($scope.taxsummary.length);
                    $defer.resolve($scope.taxsummary.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                } else {
                    toastr.error(data.response.msg, 'Error');
                }
            });
        }
    });

    $scope.tableParamsTaxCeil = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        counts: []
    }, {
        total: 0,
        counts: [],
        getData: function($defer, params) {
            wtax.getAllTaxSummary($stateParams.tax_id, {
                action: 'summary',
                limit: 1000
            }).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var taxsummary = data.response.result.taxceil;

                    if ($scope.txtFind.length > 0) {
                        $scope.taxsummary = $filter('filter')(taxsummary, $scope.txtFind);
                    } else {
                        $scope.taxsummary = taxsummary;
                    }

                    _.each($scope.taxsummary, function(row) {
                        row.selected = false;
                    });
                    console.log('$scope.taxsummary;', $scope.taxsummary)
                    params.total($scope.taxsummary.length);
                    $defer.resolve($scope.taxsummary.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                } else {
                    toastr.error(data.response.msg, 'Error');
                }
            });
        }
    });

    $scope.tableParamsTaxPercentage = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        counts: []
    }, {
        total: 0,
        counts: [],
        getData: function($defer, params) {
            wtax.getAllTaxSummary($stateParams.tax_id, {
                action: 'summary',
                limit: 1000
            }).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var taxsummary = data.response.result.taxpercentage;

                    if ($scope.txtFind.length > 0) {
                        $scope.taxsummary = $filter('filter')(taxsummary, $scope.txtFind);
                    } else {
                        $scope.taxsummary = taxsummary;
                    }

                    _.each($scope.taxsummary, function(row) {
                        row.selected = false;
                    });
                    console.log('$scope.taxsummary;', $scope.taxsummary)
                    params.total($scope.taxsummary.length);
                    $defer.resolve($scope.taxsummary.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                } else {
                    toastr.error(data.response.msg, 'Error');
                }
            });
        }
    });

    $scope.tableParamsTaxDue = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        counts: []
    }, {
        total: 0,
        counts: [],
        getData: function($defer, params) {
            wtax.getAllTaxSummary($stateParams.tax_id, {
                action: 'summary',
                limit: 1000
            }).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var taxsummary = data.response.result.taxdue;

                    if ($scope.txtFind.length > 0) {
                        $scope.taxsummary = $filter('filter')(taxsummary, $scope.txtFind);
                    } else {
                        $scope.taxsummary = taxsummary;
                    }

                    _.each($scope.taxsummary, function(row) {
                        row.selected = false;
                    });
                    console.log('$scope.taxsummary;', $scope.taxsummary)
                    params.total($scope.taxsummary.length);
                    $defer.resolve($scope.taxsummary.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                } else {
                    toastr.error(data.response.msg, 'Error');
                }
            });
        }
    });

    if ($stateParams.tax_id) {
        wtax.getTaxTableByID($stateParams.tax_id).then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                var tax = data.response.result;
                console.log('tax: ', tax);
                if (!_.isEmpty(tax)) {
                    $scope.tax = tax;
                }
            }
        });
    }

    $scope.updateTax = function(id) {
        console.log('id:', id)
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            backdrop : 'static' ,
            templateUrl: './public/hris/templates/accounting/wtax/taxtable/tax.modal.html',
            controller: 'taxModalCtrl',
             resolve: {
                tax_id: function() {
                    return id;
                },
                type : function () {
                    return null

                }
            },
            size: 'md'
        });
        $scope.cancel = function() {
            modalInstance.dismiss('cancel');
        };


    }

    $scope.refreshPage = function () {
        $scope.refreshPage = function () {
            window.location.reload();
        }
    }

    $scope.updateTaxDue = function(id) {
        console.log('id:', id)
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            backdrop : 'static' ,
            templateUrl: './public/hris/templates/accounting/wtax/taxtable/tax.rate.modal.html',
            controller: 'taxRateCtrl',
            resolve: {
                tax_id : function () {
                    return id;
                }

            },
            size: 'md'
        });
        $scope.cancel = function() {
            modalInstance.dismiss('cancel');
        };


    }

    $scope.updateTaxPercentage = function(id) {
        console.log('id:', id)
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            backdrop : 'static' ,
            templateUrl: './public/hris/templates/accounting/wtax/taxtable/tax.percent.modal.html',
            controller: 'taxPercentCtrl',
            resolve: {
                tax_id : function () {
                    return id;
                }

            },
            size: 'md'
        });
        $scope.cancel = function() {
            modalInstance.dismiss('cancel');
        };


    }

    $scope.updateTaxCeiling = function(id) {
        console.log('id:', id)
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            backdrop : 'static' ,
            templateUrl: './public/hris/templates/accounting/wtax/taxtable/tax.ceiling.modal.html',
            controller: 'taxCeilingCtrl',
            resolve: {
                tax_id : function () {
                    return id;
                }

            },
            size: 'md'
        });
        $scope.cancel = function() {
            modalInstance.dismiss('cancel');
        };


    }

    $scope.deleteTax = function(id) {
        console.log('id:', id)
        ngDialog.openConfirm({
            templateUrl: './public/dialogs/delete.dialog.html',
            scope: $scope,
            className: 'ngdialog-theme-default'
        }).then(function() {
            wtax.removeTaxDetailsByTAXID(id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success')
                    $scope.tableParamsDaily.reload();

                }
            });
        });
    }

    $scope.deleteTaxCeiling = function(id) {
        console.log('id:', id)
        ngDialog.openConfirm({
            templateUrl: './public/dialogs/delete.dialog.html',
            scope: $scope,
            className: 'ngdialog-theme-default'
        }).then(function() {
            wtax.removeTaxCeilByTAXID(id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success')
                    $scope.tableParamsTaxCeil.reload();

                }
            });
        });
    }

    $scope.deleteTaxPercentage = function(id) {
        console.log('id:', id)
        ngDialog.openConfirm({
            templateUrl: './public/dialogs/delete.dialog.html',
            scope: $scope,
            className: 'ngdialog-theme-default'
        }).then(function() {
            wtax.removeTaxPercentByTAXID(id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success')
                    $scope.tableParamsTaxPercentage.reload();

                }
            });
        });
    }

    $scope.deleteTaxDue = function(id) {
        console.log('id:', id)
        ngDialog.openConfirm({
            templateUrl: './public/dialogs/delete.dialog.html',
            scope: $scope,
            className: 'ngdialog-theme-default'
        }).then(function() {
            wtax.removeTaxdueByTAXID(id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success')
                    $scope.tableParamsTaxDue.reload();

                }
            });
        });
    }

    $scope.refreshData = function() {
        $scope.tableParamsDaily.reload();
    };

    $scope.searchData = function() {

    };

    $scope.addTAX = function(type) {
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            backdrop : 'static' ,
            templateUrl: './public/hris/templates/accounting/wtax/taxtable/tax.modal.html',
            controller: 'taxModalCtrl',
            size: 'md',
            resolve: {
                type: function() {
                    return type
                },
                tax_id: function() {
                    return $scope.tax.TAXID
                }
            }
        });

        modalInstance.result.then(function(selectedItem) {
            $scope.refreshData();
        }, function() {});
    };

    $scope.addTaxCeiling = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            backdrop : 'static' ,
            templateUrl: './public/hris/templates/accounting/wtax/taxtable/tax.ceiling.modal.html',
            controller: 'taxCeilingCtrl',
            resolve: {
                tax_id: function() {
                    return $scope.tax.TAXID
                }
            },
            size: 'md'
        });

        modalInstance.result.then(function(selectedItem) {
            $scope.refreshData();
        }, function() {});
    };

    $scope.addTaxPercentage = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            backdrop : 'static' ,
            templateUrl: './public/hris/templates/accounting/wtax/taxtable/tax.percent.modal.html',
            controller: 'taxPercentCtrl',
            resolve: {
                tax_id: function() {
                    return $scope.tax.TAXID

                }
            },
            size: 'md'
        });
        modalInstance.result.then(function(selectedItem) {
            $scope.refreshData();
        }, function() {});
    };

    $scope.addTaxRate = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            backdrop : 'static' ,
            templateUrl: './public/hris/templates/accounting/wtax/taxtable/tax.rate.modal.html',
            controller: 'taxRateCtrl',
            resolve: {
                tax_id: function() {
                    return $scope.tax.TAXID

                }
            },
            size: 'md'
        });

        modalInstance.result.then(function(selectedItem) {
            $scope.refreshData();
        }, function() {});
    };

    $scope.viewTaxSummary = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            backdrop : 'static' ,
            templateUrl: './public/hris/templates/accounting/wtax/taxtable/tax.summary.html',
            controller: 'taxSummaryModalCtrl',
            resolve: {
                summary: function() {
                    return $scope.taxsumcopy;
                }
            },
            size: 'lg'
        });

        modalInstance.result.then(function(selectedItem) {
            $scope.refreshData();
        }, function() {});
    }


}