'use strict';

angular.module('hris')
    .controller('taxCtrl', taxCtrl)
    .controller('taxSummaryModalCtrl', taxSummaryModalCtrl);

taxCtrl.$inject = ['$scope', '$state', 'auth', '$timeout', 'ngDialog', '$filter', '$uibModal', 'wtax', 'toastr', 'ngTableParams'];

function taxCtrl($scope, $state, auth, $timeout, ngDialog, $filter, $uibModal, wtax, toastr, ngTableParams) {
    $scope.yourtax = [];
    $scope.IsDisable = true;
    $scope.dataSearch = {
        text: '' 
    };
    $scope.selectedItem = {};
    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        filter : $scope.dataSearch,
        counts: []
    }, {
        total: 0,
        counts: [],
        getData: function($defer, params) {
            wtax.getAllWithholdingTax({
                action: 'picker',
                limit: 1000
            }).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var yourtax = data.response.result;

                    if (params.filter().text) {
                        $scope.yourtax = $filter('filter')(yourtax, params.filter().text);
                    } else {
                        $scope.yourtax = yourtax;
                    }

                    _.each($scope.yourtax, function(row) {
                        row.selected = false;
                    });
                    console.log('$scope.yourtax;', $scope.yourtax)
                    params.total($scope.yourtax.length);
                    $defer.resolve($scope.yourtax.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                } else {
                    toastr.error(data.response.msg, 'Error');
                }
            });
        }
    });

    $scope.updateholdingTax = function(id) {
        $scope.IsDisable = false;
        console.log('id:', id)
        if (id) {
            wtax.getTaxTableByID(id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    $scope.wtax = data.response.result;
                    console.log('$scope.wtax:', $scope.wtax);
                }
            });
        }

    };

    $scope.deleteHoldingTax = function(id) {
        console.log('id:', id)
        ngDialog.openConfirm({
            templateUrl: './public/dialogs/delete.dialog.html',
            scope: $scope,
            msg: 'Test',
            className: 'ngdialog-theme-default'
        }).then(function() {
            wtax.removeWithholdingTax(id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success')
                    $scope.tableParams.reload();

                }
            });
        });
    };

    $scope.viewHoldingTax = function(id) {
        $state.go('main.wtax_detail', {
            tax_id: id
        });
    };

    $scope.saveEntry = function() {
            if ($scope.wtax)
                if ($scope.wtax.TAXID) {
                    console.log('$scope.wtax:', $scope.wtax)
                    wtax.updateWithholdingTax($scope.wtax.UUID, $scope.wtax).then(function(data) {
                        if (data.statusCode == 200 && data.response.success) {
                            toastr.success(data.response.msg, 'Success');
                             $scope.tableParams.reload();
                        } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else {
                            toastr.error(data.msg, 'Error');
                        }
                    });
                } else {

                    console.log('wtax:', $scope.wtax)
                    wtax.saveEntry($scope.wtax).then(function(data) {
                        console.log('data:', data)
                        if (data.statusCode == 200 && data.response.success) {
                            toastr.success(data.response.msg, 'Success');
                             $scope.tableParams.reload();
                        } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                            _.each(data.response.result, function(msg) {
                                toastr.warning(msg.msg, 'Warning');
                            });
                        } else {
                            toastr.error(data.msg, 'Error');
                        }
                    });
                }
        },
        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };

    $scope.addTax = function() {
        $scope.wtax = {};
        $scope.IsDisable = false;
    };

    $scope.refreshData = function() {
        $scope.tableParams.reload();

    };

    $scope.searchHoldingTax = function() {
        $scope.tableParams.reload();

    };
}

taxSummaryModalCtrl.$inject = ['$scope', '$uibModalInstance', 'ngDialog', 'ngTableParams', 'wtax', '$stateParams', 'summary', '$filter'];

function taxSummaryModalCtrl($scope, $uibModalInstance, ngDialog, ngTableParams, wtax, $stateParams, summary, $filter) {
    console.log('wtax:', wtax)
    console.log('summary:', summary)
    var sum = [];
    $scope.tax = {};
    $scope.taxsummary = [];
    $scope.IsDisable = true;
    $scope.txtFind = '';
    $scope.selectedItem = {};
    sum.push(summary.daily)
    sum.push(summary.weekly)
    sum.push(summary.semimonthly)
    sum.push(summary.monthly)
    $scope.taxsummary = _.flattenDeep(sum);
    console.log('$scope.taxsummary:', $scope.taxsummary)

    $scope.tableParams1 = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        counts: []
    }, {
        total: 0,
        counts: [],
        getData: function($defer, params) {
            wtax.getAllTaxSummary($stateParams.tax_id, {
                action: 'summary',
                limit: 1000
            }).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var taxsummary = data.response.result.taxsummary;
                    _.each($scope.taxsummary, function(row) {
                        row.selected = false;
                    });
                    console.log('$scope.taxsummary;', $scope.taxsummary)
                    params.total($scope.taxsummary.length);
                    $defer.resolve($scope.taxsummary.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                } else {
                    toastr.error(data.response.msg, 'Error');
                }
            });
        }
    });

    if ($stateParams.tax_id) {
        wtax.getTaxTableByID($stateParams.tax_id).then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                var tax = data.response.result;
                console.log('tax: ', tax);
                if (!_.isEmpty(tax)) {
                    $scope.tax = tax;
                }
            }
        });
    }


    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

}