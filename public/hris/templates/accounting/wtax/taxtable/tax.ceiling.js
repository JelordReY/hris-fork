'use strict';

angular.module('hris')
    .controller('taxCeilingCtrl', taxCeilingCtrl);

taxCeilingCtrl.$inject = ['$scope', '$state', '$uibModalInstance', 'wtax', 'Payroll', 'toastr', 'tax_id'];

function taxCeilingCtrl($scope, $state, $uibModalInstance, wtax, Payroll, toastr, tax_id) {

    if (tax_id) {
        wtax.getTaxCeilingByID(tax_id).then(function(data) {
            console.log('wtax:', wtax)
            console.log('tax_id:', tax_id)
            if (data.statusCode == 200 && data.response.success) {
                $scope.taxceil = data.response.result;
                if ($scope.taxceil.length == 0) {
                    $scope.taxceil = {};
                }
                console.log('data:', data);
                console.log('$scope.taxceil:', $scope.taxceil);
            }
        });
    }


    Payroll.getAllPayTypes().then(function(data) {
        if (data.statusCode == 200 && data.response.success) {
            var paytypes = data.response.result;
            if (!_.isEmpty(paytypes)) {
                $scope.paytypes = paytypes;
            }
        }
    });

    $scope.saveTaxCeiling = function() {
        if ($scope.taxceil)
            if ($scope.taxceil.IndexID) {
                console.log('$scope.taxceil:', $scope.taxceil)
                wtax.updateTaxCeil($scope.taxceil.IndexID, $scope.taxceil).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $uibModalInstance.close('save');
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            } else {

                wtax.saveTaxCeiling(tax_id, $scope.taxceil).then(function(data) {
                    console.log('data:', data)
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $uibModalInstance.close('save');
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            }
    };


    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
};