'use strict';

angular.module('hris')
    .controller('taxExemptionCtrl', taxExemptionCtrl);

taxExemptionCtrl.$inject = ['$scope', '$uibModalInstance', 'ngDialog', '$filter', '$uibModal', 'taxexemption', 'toastr', 'auth', '$state', 'ngTableParams'];

function taxExemptionCtrl($scope, $uibModalInstance, ngDialog, $filter, $uibModal, taxexemption, toastr, auth, $state, ngTableParams) {
    $scope.taxexcemp = {};
    $scope.IsDisable = true;
    $scope.txtFind = '';
    $scope.dataSearch = {
        text: ''
    };
    $scope.selectedItem = {};
    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 5, // count per page
        filter: $scope.dataSearch,
        counts: []
    }, {
        total: 0,
        counts: [],
        getData: function($defer, params) {
            taxexemption.getAllTaxExemptions({
                action: 'picker',
                limit: 1000
            }).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var taxexcemp = data.response.result;

                    if (params.filter().text) {
                        $scope.taxexcemp = $filter('filter')(taxexcemp, params.filter().text);
                    } else {
                        $scope.taxexcemp = taxexcemp;
                    }

                    _.each($scope.taxexcemp, function(row) {
                        row.selected = false;
                    });
                    console.log('$scope.taxexcemp;', $scope.taxexcemp)
                    params.total($scope.taxexcemp.length);
                    $defer.resolve($scope.taxexcemp.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                } else {
                    toastr.error(data.response.msg, 'Error');
                }
            });
        }
    });

    $scope.saveEntry = function() {
        if ($scope.taxexemption)
            if ($scope.taxexemption.IndexID) {
                console.log('$scope.taxexemption:', $scope.taxexemption)
                taxexemption.Update_Tax_Exemption($scope.taxexemption.UUID, $scope.taxexemption).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $scope.tableParams.reload();
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            } else {

                console.log(' taxexemption:', $scope.taxexemption)
                taxexemption.saveEntry($scope.taxexemption).then(function(data) {
                    console.log('data:', data)
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $scope.tableParams.reload();
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            }
    };



    $scope.deleteTaxExemp = function(id) {
        console.log('id:', id)
        ngDialog.openConfirm({
            templateUrl: './public/dialogs/delete.dialog.html',
            scope: $scope,
            className: 'ngdialog-theme-default'
        }).then(function() {
            taxexemption.Delete_Tax_Exemption(id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success')
                    $scope.tableParams.reload();

                }
            });
        });
    }

    $scope.searchTaxExemption = function() {
        $scope.tableParams.reload();

    };
    $scope.updateTaxExemp = function(id) {
        $scope.IsDisable = false;
        console.log('id:', id)
        if (id) {
            taxexemption.GetExemptionByID(id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    $scope.taxexemption = data.response.result;
                    console.log('$scope.taxexemption:', $scope.taxexemption);
                }
            });
        }

    }

    $scope.RefreshData = function() {
        $scope.ngTableParams.reload();
    }

    $scope.addTaxExemp = function() {
        $scope.taxexemption = {};
        $scope.IsDisable = false;
    }

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
}