'use strict';

angular.module('hris')
    .controller('accountsCtrl', accountsCtrl)
    .controller('accountsModalCtrl', accountsModalCtrl)
    .controller('accountGroupCtrl', accountGroupCtrl)
    .controller('accountClassCtrl', accountClassCtrl)
    .controller('accountTypeCtrl', accountTypeCtrl)
    .controller('accountsPrintCtrl', accountsPrintCtrl);

accountsCtrl.$inject = ['$scope', '$state', 'toastr', 'auth', '$timeout', 'ngTableParams', '$filter', 'accounts', '$uibModal', 'ngDialog'];

function accountsCtrl($scope, $state, toastr, auth, $timeout, ngTableParams, $filter, accounts, $uibModal, ngDialog) {
    $scope.accountdata = [];
    $scope.IsDisable = true;
    $scope.action = {};
    $scope.dataSearch = {
        text: ''
    };
    $scope.selectedItem = {};
    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        filter: $scope.dataSearch,
        counts: []
    }, {
        total: 0,
        counts: [],
        getData: function($defer, params) {
            accounts.getAllAccounts({
                action: 'picker',
                limit: 1000
            }).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var accountdata = data.response.result;

                    if (params.filter().text) {
                        $scope.accountdata = $filter('filter')(accountdata, params.filter().text);
                    } else {
                        $scope.accountdata = accountdata;
                    }

                    _.each($scope.accountdata, function(row) {
                        row.selected = false;
                    });
                    console.log('$scope.accountdata;', $scope.accountdata)
                    params.total($scope.accountdata.length);
                    $defer.resolve($scope.accountdata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                } else {
                    toastr.error(data.response.msg, 'Error');
                }
            });
        }
    });




    $scope.updateAccounts = function(id) {
        console.log('id:', id)
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            backdrop: 'static',
            templateUrl: './public/hris/templates/accounting/chartsofaccounts/accounts.modal.html',
            controller: 'accountsModalCtrl',
            resolve: {
                acct_id: function() {
                    return id;
                },
            },
            size: 'lg'
        });
        $scope.cancel = function() {
            modalInstance.dismiss('cancel');
        };


    }

    $scope.deleteAccounts = function(id) {
        console.log('id:', id)
        ngDialog.openConfirm({
            templateUrl: './public/dialogs/delete.dialog.html',
            scope: $scope,
            className: 'ngdialog-theme-default'
        }).then(function() {
            accounts.Delete_Accounts(id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success')
                    $scope.tableParams.reload();
                }
            });
        });
    }


    $scope.addAccounts = function() {
        console.log('Add+ Accounts')
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            backdrop: 'static',
            ariaDescribedBy: 'modal-body',
            templateUrl: './public/hris/templates/accounting/chartsofaccounts/accounts.modal.html',
            controller: 'accountsModalCtrl',
            resolve: {
                acct_id: function() {
                    return null
                }
            },
            size: 'lg'
        });

        modalInstance.result.then(function(selectedItem) {}, function() {});
    };
    $scope.searchAccounts = function() {
        $scope.tableParams.reload();
    };
    $scope.refreshData = function() {
        $scope.tableParams.reload();
    };

    $scope.addGroup = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            backdrop: 'static',
            ariaDescribedBy: 'modal-body',
            templateUrl: './public/hris/templates/accounting/chartsofaccounts/accountgroup.modal.html',
            controller: 'accountGroupCtrl',
            resolve: {
                acct_id: function() {
                    return null
                }
            },
            size: 'modal'
        });

        modalInstance.result.then(function(selectedItem) {}, function() {});
    };

    $scope.addClass = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            backdrop: 'static',
            ariaDescribedBy: 'modal-body',
            templateUrl: './public/hris/templates/accounting/chartsofaccounts/accountclass.modal.html',
            controller: 'accountClassCtrl',
            resolve: {
                acct_id: function() {
                    return null
                }
            },
            size: 'modal'
        });

        modalInstance.result.then(function(selectedItem) {}, function() {});
    };

    $scope.addType = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            backdrop: 'static',
            templateUrl: './public/hris/templates/accounting/chartsofaccounts/accounttype.modal.html',
            controller: 'accountTypeCtrl',
            resolve: {
                acct_id: function() {
                    return null
                }
            },
            size: 'modal'
        });

        modalInstance.result.then(function(selectedItem) {}, function() {});
    };

    $scope.searchAccounts = function() {
        $scope.tableParams.reload();
    };
    $scope.refreshData = function() {
        $scope.tableParams.reload();
    };

    $scope.printAccounts = function() {
        $state.go('main.accounts_print', { action: $scope.action.action, _id: $scope.action._id });
    };

}

accountsModalCtrl.$inject = ['$scope', '$state', 'toastr', 'auth', '$timeout', '$uibModalInstance', '$uibModal', 'accounts', 'acct_id', '$filter'];


function accountsModalCtrl($scope, $state, toastr, auth, $timeout, $uibModalInstance, $uibModal, accounts, acct_id, $filter) {

    if (acct_id) {
        accounts.GetAccountByID(acct_id).then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                $scope.accounts = data.response.result;
                console.log('$scope.accounts:', $scope.accounts);
            }
        });
    }

    async.waterfall([
        function(callback) {
            accounts.getAllAccountGroups().then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var accountgroup = data.response.result;
                    if (!_.isEmpty(accountgroup)) {
                        $scope.accountgroup = accountgroup;
                    }
                    callback();
                }
            });
        },
        function(callback) {
            accounts.getAllAccountTypes().then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var accounttype = data.response.result;
                    if (!_.isEmpty(accounttype)) {
                        $scope.accounttype = accounttype;
                    }
                    callback();
                }
            });
        },
        function(callback) {
            accounts.getAllAccountClass().then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var accountclass = data.response.result;
                    if (!_.isEmpty(accountclass)) {
                        $scope.accountclass = accountclass;
                    }
                    callback();
                }
            });
        },
    ])

    $scope.saveEntry = function() {
        if (acct_id) {
            console.log('$scope.accounts:', $scope.accounts)
            accounts.Update_Accounts(acct_id, $scope.accounts).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success');
                    $uibModalInstance.close('save');
                } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                    _.each(data.response.result, function(msg) {
                        toastr.warning(msg.msg, 'Warning');
                    });
                } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                    _.each(data.response.result, function(msg) {
                        toastr.warning(msg.msg, 'Warning');
                    });
                } else {
                    toastr.error(data.msg, 'Error');
                }
            });
        } else {

            console.log('accounts:', $scope.accounts)
            accounts.saveEntry($scope.accounts).then(function(data) {
                console.log('data:', data)
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success');
                    $uibModalInstance.close('save');
                } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                    _.each(data.response.result, function(msg) {
                        toastr.warning(msg.msg, 'Warning');
                    });
                } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                    _.each(data.response.result, function(msg) {
                        toastr.warning(msg.msg, 'Warning');
                    });
                } else {
                    toastr.error(data.msg, 'Error');
                }
            });
        }
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

}

accountGroupCtrl.$inject = ['$scope', '$state', 'toastr', 'auth', '$timeout', '$uibModalInstance', '$uibModal', 'accounts', 'acct_id', 'ngTableParams', 'ngDialog'];

function accountGroupCtrl($scope, $state, toastr, auth, $timeout, $uibModalInstance, $uibModal, accounts, acct_id, ngTableParams, ngDialog) {
    $scope.groupdata = [];
    $scope.IsDisable = true;
    $scope.txtFind = '';
    $scope.selectedItem = {};
    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 5, // count per page
        counts: []
    }, {
        total: 0,
        counts: [],
        getData: function($defer, params) {
            accounts.getAllAccountGroups({
                action: 'picker',
                limit: 1000
            }).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var groupdata = data.response.result;

                    if ($scope.txtFind.length > 0) {
                        $scope.groupdata = $filter('filter')(groupdata, $scope.txtFind);
                    } else {
                        $scope.groupdata = groupdata;
                    }

                    _.each($scope.groupdata, function(row) {
                        row.selected = false;
                    });
                    console.log('$scope.groupdata;', $scope.groupdata)
                    params.total($scope.groupdata.length);
                    $defer.resolve($scope.groupdata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                } else {
                    toastr.error(data.response.msg, 'Error');
                }
            });
        }
    });

    $scope.updateGroups = function(id) {
        $scope.IsDisable = false;
        console.log('id:', id)
        if (id) {
            accounts.getAccountGroup(id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    $scope.groups = data.response.result;
                    console.log('$scope.groups:', $scope.groups);
                }
            });
        }

    }

    $scope.deleteGroups = function(id) {
        console.log('id:', id)
        ngDialog.openConfirm({
            templateUrl: './public/dialogs/delete.dialog.html',
            scope: $scope,
            className: 'ngdialog-theme-default'
        }).then(function() {
            accounts.Delete_Account_Groups(id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success')
                    $scope.tableParams.reload();
                }
            });
        });

    }

    $scope.saveEntry2 = function() {
        if ($scope.groups)
            if ($scope.groups.GroupID) {
                console.log('$scope.groups:', $scope.groups)
                accounts.Update_Account_Groups($scope.groups.UUID, $scope.groups).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $scope.tableParams.reload();
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            } else {


                console.log('groups:', $scope.groups)
                accounts.saveEntry2($scope.groups).then(function(data) {
                    console.log('data:', data)
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $scope.tableParams.reload();
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            }
    };
    $scope.refreshData = function() {
        $scope.tableParams.reload();
    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.addGroup = function() {
        $scope.groups = {};
        $scope.IsDisable = false;
    };

    $scope.searchAccountGroup = function() {
        $scope.tableParams.reload();
    };



}


accountClassCtrl.$inject = ['$scope', '$state', 'toastr', 'auth', '$timeout', '$uibModalInstance', '$uibModal', 'accounts', 'acct_id', 'ngDialog', '$filter', 'ngTableParams'];

function accountClassCtrl($scope, $state, toastr, auth, $timeout, $uibModalInstance, $uibModal, accounts, acct_id, ngDialog, $filter, ngTableParams) {

    $scope.classdata = [];
    $scope.IsDisable = true;
    $scope.txtFind = '';
    $scope.selectedItem = {};
    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 5, // count per page
        counts: []
    }, {
        total: 0,
        counts: [],
        getData: function($defer, params) {
            accounts.getAllAccountClass({
                action: 'picker',
                limit: 1000
            }).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var classdata = data.response.result;

                    if ($scope.txtFind.length > 0) {
                        $scope.classdata = $filter('filter')(classdata, $scope.txtFind);
                    } else {
                        $scope.classdata = classdata;
                    }

                    _.each($scope.classdata, function(row) {
                        row.selected = false;
                    });
                    console.log('$scope.classdata;', $scope.classdata)
                    params.total($scope.classdata.length);
                    $defer.resolve($scope.classdata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                } else {
                    toastr.error(data.response.msg, 'Error');
                }
            });
        }
    });

    $scope.updateClass = function(id) {
        $scope.IsDisable = false;
        console.log('id:', id)
        if (id) {
            accounts.getAccountClass(id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    $scope.classification = data.response.result;
                    console.log('$scope.classification:', $scope.classification);
                }
            });
        }

    }

    $scope.deleteClass = function(id) {
        console.log('id:', id)
        ngDialog.openConfirm({
            templateUrl: './public/dialogs/delete.dialog.html',
            scope: $scope,
            className: 'ngdialog-theme-default'
        }).then(function() {
            accounts.Delete_Account_Class(id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success')
                    $scope.tableParams.reload();
                }
            });
        });
    }

    $scope.saveEntry3 = function() {
        if ($scope.classification)
            if ($scope.classification.ClassID) {
                console.log('$scope.classification:', $scope.classification)
                accounts.Update_Account_Class($scope.classification.UUID, $scope.classification).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $scope.tableParams.reload();
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            } else {

                console.log('classification:', $scope.classification)
                accounts.saveEntry3($scope.classification).then(function(data) {
                    console.log('data:', data)
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $scope.tableParams.reload();
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            }
    };

    $scope.refreshData = function() {
        $scope.tableParams.reload();
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.addClass = function() {
        $scope.classification = {};
        $scope.IsDisable = false;
    };

    $scope.searchAccountGroup = function() {
        $scope.tableParams.reload();
    };
}

accountTypeCtrl.$inject = ['$scope', '$state', 'toastr', 'auth', '$timeout', '$uibModalInstance', '$filter', 'accounts', 'acct_id', 'ngTableParams', 'ngDialog'];

function accountTypeCtrl($scope, $state, toastr, auth, $timeout, $uibModalInstance, $filter, accounts, acct_id, ngTableParams, ngDialog) {

    $scope.accounttypedata = [];
    $scope.IsDisable = true;
    $scope.txtFind = '';
    $scope.selectedItem = {};
    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 5, // count per page
        counts: []
    }, {
        total: 0,
        counts: [],
        getData: function($defer, params) {
            accounts.getAllAccountTypes({
                action: 'picker',
                limit: 1000
            }).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var accounttypedata = data.response.result;

                    if ($scope.txtFind.length > 0) {
                        $scope.accounttypedata = $filter('filter')(accounttypedata, $scope.txtFind);
                    } else {
                        $scope.accounttypedata = accounttypedata;
                    }

                    _.each($scope.accounttypedata, function(row) {
                        row.selected = false;
                    });
                    console.log('$scope.accounttypedata;', $scope.accounttypedata)
                    params.total($scope.accounttypedata.length);
                    $defer.resolve($scope.accounttypedata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                } else {
                    toastr.error(data.response.msg, 'Error');
                }
            });
        }
    });


    $scope.updateType = function(id) {
        $scope.IsDisable = false;
        console.log('id:', id)
        if (id) {
            accounts.getAccountType(id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    $scope.accounttype = data.response.result;
                    console.log('$scope.accounttype:', $scope.accounttype);
                }
            });
        }

    }

    $scope.deleteType = function(id) {
        console.log('id:', id)
        ngDialog.openConfirm({
            templateUrl: './public/dialogs/delete.dialog.html',
            scope: $scope,
            className: 'ngdialog-theme-default'
        }).then(function() {
            accounts.Delete_Account_Types(id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success')
                    $scope.tableParams.reload();
                }
            });
        });
    }

    $scope.saveEntry4 = function() {
        if ($scope.accounttype)
            if ($scope.accounttype.TypeID) {
                console.log('$scope.accounttype:', $scope.accounttype)
                accounts.Update_Account_Types($scope.accounttype.UUID, $scope.accounttype).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $scope.tableParams.reload();
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            } else {

                console.log('accounttype:', $scope.accounttype)
                accounts.saveEntry4($scope.accounttype).then(function(data) {
                    console.log('data:', data)
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $scope.tableParams.reload();
                    } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.msg, 'Error');
                    }
                });
            }
    };

    $scope.refreshData = function() {
        $scope.tableParams.reload();
    };

    $scope.addType = function() {
        $scope.accounttype = {};
        $scope.IsDisable = false;
    };


    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };



}

accountsPrintCtrl.$inject = ['$scope', '$state', 'auth', '$filter', 'accounts', '$uibModal', 'ngDialog', 'company', '$stateParams'];

function accountsPrintCtrl($scope, $state, auth, $filter, accounts, $uibModal, ngDialog, company, $stateParams) {


    $scope.company = {};
    /* $scope.todalData = 10;*/
    async.waterfall([
        function(callback) {
            company.getCompanyInfo().then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var company = data.response.result;
                    if (!_.isEmpty(company)) {
                        $scope.company = company;
                        if ($scope.company.ci_logo) {
                            $scope.profileImage = 'data:image/png;base64,' + $scope.company.ci_logo;
                        }
                        callback();
                    }
                }
            });
        },
        function(callback) {
            $scope.action = {
                action: $stateParams.action,
                _id: $stateParams._id
            };
            accounts.getAllAccounts($scope.action).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    var accounts = data.response.result;
                    if (!_.isEmpty(accounts)) {
                        $scope.accounts = accounts;
                    }
                }
            });
        }
    ]);

    $scope.Refresh = function() {
        $scope.accounts = [];
        $scope.company = {};

        async.waterfall([
            function(callback) {
                company.getCompanyInfo().then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var company = data.response.result;
                        if (!_.isEmpty(company)) {
                            $scope.company = company;
                            if ($scope.company.ci_logo) {
                                $scope.profileImage = 'data:image/png;base64,' + $scope.company.ci_logo;
                            }
                            callback();
                        }
                    }
                });
            },
            function(callback) {
                $scope.action = {
                    action: $stateParams.action,
                    _id: $stateParams._id
                };
                accounts.getAllAccounts($scope.action).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var accounts = data.response.result;
                        if (!_.isEmpty(accounts)) {
                            $scope.accounts = accounts;
                        }
                    }
                });
            }
        ]);



    }


}