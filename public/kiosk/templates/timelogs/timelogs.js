'use strict';

angular.module('kiosk')
    .controller('timelogsCtrl', timelogsCtrl);

timelogsCtrl.$inject = ['$scope', '$state', '$activityIndicator', 'toastr', 'ngTableParams', 'timelogs'];

function timelogsCtrl($scope, $state, $activityIndicator, toastr, ngTableParams, timelogs) {

    $scope.payrollConfig = [];
    $scope.timelogs = [];
    $scope.payroll = {};
    $scope.isExport = false;

    $scope.infiniteScrollContacts = {};
    $scope.infiniteScrollContacts.numToAdd = 20;
    $scope.infiniteScrollContacts.currentItems = 10;

    $scope.resetInfScroll = function () {
        $scope.infiniteScrollContacts.currentItems = $scope.infiniteScrollContacts.numToAdd;
    };
    $scope.addMoreItems = function () {
        $scope.infiniteScrollContacts.currentItems += $scope.infiniteScrollContacts.numToAdd;
    };


    timelogs.getPayrollConfig().then(function (data) {
        if (data.statusCode == 200 && data.response.success) {
            $scope.payrollConfig = data.response.result;
        }
    });

    $scope.loadData = function () {
        var payroll = $scope.payroll.config;
        if (!_.isUndefined(payroll)) {
            window.localStorage.setItem('payroll', JSON.stringify(payroll))
            $scope.tableParams = new ngTableParams({
                page: 1, // show first page
                count: 50, // count per page
            }, {
                getData: function ($defer, params) {
                    var employeeID = $scope.$parent.currentUser.EmployeeID || $scope.$parent.currentUser.UserName;
                    $activityIndicator.startAnimating();
                    timelogs.getEmployeeTimeLogs(employeeID, payroll.FirstDate, payroll.SecondDate).then(function (data) {
                        if (data.statusCode == 200 && data.response.success) {
                            $scope.isExport = true;
                            // $scope.timelogs = data.response.result;
                            /*var orderedData = {};
        
                                        if ($scope.searchAcctChart) {
                                            orderedData = $filter('filter')(data, $scope.searchAcctChart);
                                            orderedData = params.sorting() ? $filter('orderBy')(orderedData, params.orderBy()) : orderedData;
                                        } else {
                                            orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                                        }*/

                            params.total(data.response.result.length);
                            $defer.resolve(data.response.result.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                        } else {
                            $activityIndicator.stopAnimating();
                        }
                    });
                }
            });
        } else {
            $activityIndicator.stopAnimating();
            toastr.warning('Please select Payroll Config', 'WARNING');
        }

    };

    $scope.myFunction = function () {
        angular.element(document).ready(function () {
            $('#myTable').each(function () {
                var Column_number_to_Merge = 1;
                var Previous_TD = null;
                var i = 1;
                $("tbody", this).find('tr').each(function () {
                    var Current_td = $(this).find('td:nth-child(' + Column_number_to_Merge + ')');
                    if (Previous_TD == null) {
                        Previous_TD = Current_td;
                        i = 1;
                    } else if ($(Current_td).scope().row.formattedTimeLog == $(Previous_TD).scope().row.formattedTimeLog) {
                        Current_td.remove();
                        Previous_TD.attr('rowspan', i + 1);
                        i = i + 1;
                    } else {
                        Previous_TD = Current_td;
                        i = 1;
                    }
                });
            });
            $activityIndicator.stopAnimating();
        });
    };

}