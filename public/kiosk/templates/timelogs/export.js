'use strict';

angular.module('kiosk')
    .controller('timelogsExportCtrl', timelogsExportCtrl);

timelogsExportCtrl.$inject = ['$scope', '$state', '$activityIndicator', 'toastr', 'ngTableParams', 'timelogs']

function timelogsExportCtrl($scope, $state, $activityIndicator, toastr, ngTableParams, timelogs) {

    $scope.timelogs = [];
    $scope.payroll = {};
    $scope.employee = {};

    $scope.totalDays = 0;
    $scope.totalWorkHours = 0;

    var payroll = window.localStorage.getItem('payroll');
    if (payroll) {
        $scope.payroll = JSON.parse(payroll);
        var employeeID = $scope.$parent.currentUser.EmployeeID || $scope.$parent.currentUser.UserName;

        $scope.employee = {
            Fullname: $scope.$parent.currentUser.FullName,
            EmployeeID: $scope.$parent.currentUser.EmployeeID
        };

        $activityIndicator.startAnimating();

        timelogs.getEmployeeTimeLogs(employeeID, $scope.payroll.FirstDate, $scope.payroll.SecondDate).then(function (data) {
            if (data.statusCode == 200 && data.response.success) {
                // $scope.timelogs = data.response.result;
                var timelogs = data.response.result;
                var tempLogs = [];
                var formattedLogs = [];

                _.each(timelogs, function (row) {
                    var dayofweek = moment(row.formattedTimeLog);
                    row.dayofweek = dayofweek.format('dddd');
                });

                var newlogs = _.uniq(_.map(timelogs, function (row) {
                    return row.formattedTimeLog
                }));

                _.each(newlogs, function (row) {
                    var result = _.filter(timelogs, {
                        'formattedTimeLog': row
                    });
                    if (result) {
                        var data = {};
                        data.date = row;

                        if (result[0]) {
                            data.TimeLogAM_IN = result[0].TimeLog;
                            data.LogTimeAM_IN = result[0].LogTime;
                            data.dayofweek = result[0].dayofweek;
                            data.formattedLogTimeAM_IN = result[0].formattedLogTime;
                            data.formattedTimeLog = result[0].formattedTimeLog;
                            data.EntryID = result[0].EntryID;
                        }

                        if (result[1]) {
                            data.TimeLogAM_OUT = result[1].TimeLog;
                            data.LogTimeAM_OUT = result[1].LogTime;
                            data.dayofweek = result[1].dayofweek;
                            data.formattedLogTimeAM_OUT = result[1].formattedLogTime;
                            data.formattedTimeLog = result[1].formattedTimeLog;
                            data.EntryID = result[1].EntryID;
                        }

                        if (result[2]) {
                            data.TimeLogPM_IN = result[2].TimeLog;
                            data.LogTimePM_IN = result[2].LogTime;
                            data.dayofweek = result[2].dayofweek;
                            data.formattedLogTimePM_IN = result[2].formattedLogTime;
                            data.formattedTimeLog = result[2].formattedTimeLog;
                            data.EntryID = result[2].EntryID;
                        }

                        if (result[3]) {
                            data.TimeLogPM_OUT = result[3].TimeLog;
                            data.LogTimePM_OUT = result[3].LogTime;
                            data.dayofweek = result[3].dayofweek;
                            data.formattedLogTimePM_OUT = result[3].formattedLogTime;
                            data.formattedTimeLog = result[3].formattedTimeLog;
                            data.EntryID = result[3].EntryID;
                        }

                        var hoursAM = Math.floor(Math.abs(new Date(data.TimeLogAM_IN) - new Date(data.TimeLogAM_OUT)) / 36e5);
                        var hoursPM = Math.floor(Math.abs(new Date(data.TimeLogPM_IN) - new Date(data.TimeLogPM_OUT)) / 36e5);
                        var totalHours = 0;

                        if (_.isNaN(hoursAM) && !_.isNaN(hoursPM)) {
                            totalHours = 0 + hoursPM
                        } else if (!_.isNaN(hoursAM) && _.isNaN(hoursPM)) {
                            totalHours = hoursAM + 0
                        } else {
                            totalHours = hoursAM + hoursPM
                        }
                        data.totalHours = totalHours;
                        tempLogs.push(data);
                    }
                });
                console.log('tempLogs: ', tempLogs);
                $scope.timelogs = tempLogs;

                $scope.totalDays = $scope.timelogs.length;
                $scope.totalWorkHours = _.sum(_.map($scope.timelogs, function (row) {
                    return row.totalHours;
                }));


            } else {
                $activityIndicator.stopAnimating();
                toastr.warning(data.response.msg, 'ERROR');
            }
        });

    } else {
        $activityIndicator.stopAnimating();
        toastr.warning('Please select Payroll Config', 'WARNING');
    }

    // $scope.myFunction = function() {
    //     angular.element(document).ready(function() {
    //         $('#myTable').each(function() {
    //             var Column_number_to_Merge = 1;
    //             var Previous_TD = null;
    //             var i = 1;
    //             $("tbody", this).find('tr').each(function() {
    //                 var Current_td = $(this).find('td:nth-child(' + Column_number_to_Merge + ')');
    //                 if (Previous_TD == null) {
    //                     Previous_TD = Current_td;
    //                     i = 1;
    //                 } else if ($(Current_td).scope().row.formattedTimeLog == $(Previous_TD).scope().row.formattedTimeLog) {
    //                     Current_td.remove();
    //                     Previous_TD.attr('rowspan', i + 1);
    //                     i = i + 1;
    //                 } else {
    //                     Previous_TD = Current_td;
    //                     i = 1;
    //                 }
    //             });
    //         });
    //         $activityIndicator.stopAnimating();
    //     });
    // };

}