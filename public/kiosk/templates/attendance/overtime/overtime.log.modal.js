(function() {
    'use strict';

    angular.module('kiosk')
        .controller('overtimeLogModalCtrl', overtimeLogModalCtrl);

    overtimeLogModalCtrl.$inject = ['$scope', '$uibModalInstance', 'attendance', 'details', 'localStorageService', 'employee', 'toastr', '$timeout'];

    function overtimeLogModalCtrl($scope, $uibModalInstance, attendance, details, localStorageService, employee, toastr, $timeout) {
        console.log('overtimeLogModalCtrl');

        $scope.overtime = {};
        $scope.overtime.NoHrs = 0;
        $scope.overtimeTypes = [];
        $scope.currentPayroll = {};

        if (localStorageService.get('hris.payroll')) {
            $scope.currentPayroll = localStorageService.get('hris.payroll');
        }

        var currDate = new Date();

        $scope.hstep = 1;
        $scope.mstep = 1
        $scope.ismeridian = true;

        $scope.popup1 = {
            opened: false
        };
        $scope.popup2 = {
            opened: false
        };
        $scope.dateOptions = {
            formatYear: 'yyyy'
        };

        if (details) {
            $scope.overtime.TimeIn = _.isEmpty(details.TimeIn) ? currDate : details.TimeIn;
            $scope.overtime.TimeOut = _.isEmpty(details.TimeOut) ? currDate : details.TimeOut;
            $scope.overtime.InclusiveDates = new Date(details.InclusiveDates);
            $scope.overtime.OTType = details.OTType;
            $scope.overtime.Remarks = details.Remarks;
            $scope.overtime.RefNo = details.RefNo;
            $scope.overtime.IsApproved = details.IsApproved;
            $scope.overtime.IsPreApproved = details.IsPreApproved;
            $scope.overtime.Requestor = details.Requestor;
            $scope.overtime.Project = details.Project;
            $scope.overtime.IndexID = details.IndexID;
            $scope.overtime.IsExpired = details.IsExpired;
            $scope.overtime.OTExpiry = new Date(details.OTExpiry);
            $scope.overtime.PreApprovedUserID = details.PreApprovedUserID;
            $scope.overtime.ApprovedUserID = details.ApprovedUserID;
            console.log('$scope.overtime: ', $scope.overtime);
        }

        $scope.open1 = function() {
            $scope.popup1.opened = true;
        };

        $scope.open2 = function() {
            $scope.popup2.opened = true;
        };

        $scope.changed = function() {
            if ($scope.overtime) {
                var diff = $scope.overtime.TimeOut.valueOf() - $scope.overtime.TimeIn.valueOf();
                var diffInHours = Math.floor(diff / 1000 / 60 / 60); // Convert milliseconds to hours
                if (diffInHours < 0) {
                    diffInHours = 24 + diffInHours;
                }
                $scope.overtime.NoHrs = diffInHours;
            }
        };


        $scope.saveEntry = function() {
            if ($scope.overtime.TimeIn) {
                $scope.overtime.TimeIn = moment($scope.overtime.TimeIn).format('YYYY-MM-DD HH:mm:ss')
                console.log('$scope.overtime.TimeIn: ', $scope.overtime.TimeIn)
            }

            if ($scope.overtime.TimeOut) {
                $scope.overtime.TimeOut = moment($scope.overtime.TimeOut).format('YYYY-MM-DD HH:mm:ss')
                console.log('$scope.overtime.TimeOut: ', $scope.overtime.TimeOut)
            }

            if ($scope.overtime.OTExpiry) {
                $scope.overtime.OTExpiry = moment(new Date($scope.overtime.OTExpiry)).format('YYYY-MM-DD')
            }

            if ($scope.overtime.InclusiveDates) {
                $scope.overtime.InclusiveDates = moment(new Date($scope.overtime.InclusiveDates)).format('YYYY-MM-DD')
            }

            attendance.Update_OverTime_Details(employee.UUID, $scope.currentPayroll.UUID, details.IndexID, $scope.overtime).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'SUCCESS');
                    $timeout(function() {
                        $uibModalInstance.close('save');
                    }, 300)
                } else {
                    toastr.error(data.response.msg, 'ERROR');
                    return;
                }
            });
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();