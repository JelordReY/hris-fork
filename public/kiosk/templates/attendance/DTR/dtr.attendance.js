(function () {
    'use strict';

    angular.module('kiosk')
        .controller('dtrEmployeeSchedCtrl', dtrEmployeeSchedCtrl);

    dtrEmployeeSchedCtrl.$inject = ['$scope', '$uibModalInstance', 'workschedules', '$timeout', 'TemplateID'];

    function dtrEmployeeSchedCtrl($scope, $uibModalInstance, workschedules, $timeout, TemplateID) {
        $scope.schedules = {};
        $scope.workDays = [];

        async.waterfall([
            function (callback) {
                workschedules.getAllDays().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var workDays = data.response.result;
                        if (!_.isEmpty(workDays)) {
                            $scope.workDays = workDays;
                        }
                        callback();
                    }
                });
            },
            function (callback) {
                if (TemplateID) {
                    var hashTemplateID = CryptoJS.MD5(TemplateID.toString());
                    workschedules.getWorkScheduleDetails(hashTemplateID).then(function (data) {
                        if (data.statusCode == 200 && data.response.success) {
                            var schedules = data.response.result;
                            if (!_.isEmpty(schedules)) {
                                $scope.schedules = schedules;
                            }
                        }
                    });
                }
            }
        ]);

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

})();