(function() {
    'use strict';

    angular.module('kiosk')
        .controller('dailyTimeRecordPrintCtrl', ['$scope', '$stateParams', 'attendance', 'company', function($scope, $stateParams, attendance, company) {
            console.log('dailyTimeRecordPrintCtrl: ');

            $scope.company = {};
            $scope.action = {};
            $scope.attendance = [];
            $scope.employee = {};
            $scope.payroll = {};

            $scope.curDate = moment(new Date()).format('MM/DD/YYYY');


            console.log('$stateParams: ', $stateParams);

            async.waterfall([
                function(callback) {
                    company.getCompanyInfo().then(function(data) {
                        if (data.statusCode == 200 && data.response.success) {
                            var company = data.response.result;
                            if (!_.isEmpty(company)) {
                                $scope.company = company;
                                if ($scope.company.ci_logo) {
                                    $scope.profileImage = 'data:image/png;base64,' + $scope.company.ci_logo;
                                }
                                callback();
                            }
                        }
                    });
                },
                function(callback) {
                    if ($stateParams.employee) {
                        $scope.employee = $stateParams.employee;
                    }

                    if ($stateParams.payroll) {
                        $scope.payroll = $stateParams.payroll;
                    }

                    if ($stateParams.attendance) {
                        $scope.attendance = $stateParams.attendance;
                    }

                    /*$scope.action = {
                        action: $stateParams.action,
                        _id: $stateParams._id
                    };
                    employee.getEmployeeMasterlist($scope.action).then(function(data) {
                        if (data.statusCode == 200 && data.response.success) {
                            var employees = data.response.result;
                            if (!_.isEmpty(employees)) {
                                $scope.employee = employees;
                            }
                        }
                    });*/
                }
            ]);





            $scope.refreshData = function() {
                $scope.employee = [];
                $scope.company = {};

                async.waterfall([
                    function(callback) {
                        company.getCompanyInfo().then(function(data) {
                            if (data.statusCode == 200 && data.response.success) {
                                var company = data.response.result;
                                if (!_.isEmpty(company)) {
                                    $scope.company = company;
                                    if ($scope.company.ci_logo) {
                                        $scope.profileImage = 'data:image/png;base64,' + $scope.company.ci_logo;
                                    }
                                    callback();
                                }
                            }
                        });
                    },
                    function(callback) {
                        if ($stateParams.employee) {
                            $scope.employee = $stateParams.employee;
                        }

                        if ($stateParams.payroll) {
                            $scope.payroll = $stateParams.payroll;
                        }

                        if ($stateParams.attendance) {
                            $scope.attendance = $stateParams.attendance;
                        }

                        /*$scope.action = {
                            action: $stateParams.action,
                            _id: $stateParams._id
                        };
                        employee.getEmployeeMasterlist($scope.action).then(function(data) {
                            if (data.statusCode == 200 && data.response.success) {
                                var employees = data.response.result;
                                if (!_.isEmpty(employees)) {
                                    $scope.employee = employees;
                                }
                            }
                        });*/
                    }
                ]);
            };
        }]);
})();