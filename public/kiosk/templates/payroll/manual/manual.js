(function () {
    'use strict';

    angular.module('kiosk')
        .controller('manualPayrollCtrl', manualPayrollCtrl);

    manualPayrollCtrl.$inject = ['$scope', '$state', 'toastr', 'localStorageService', '$location', '$uibModal', 'employee','$timeout','$stateParams'];

    function manualPayrollCtrl($scope, $state, toastr, localStorageService, $location, $uibModal, employee,$timeout,$stateParams) {
        $scope.currentPayroll = {};

        if (localStorageService.get('hris.payroll')) {
            $scope.currentPayroll = localStorageService.get('hris.payroll');
        }

        $scope.getEmployeeProfile = function (e_id) {
            employee.getEmployeeProfile(e_id, {}).then(function (data) {
                if (data.statusCode == 200 && data.response.success) {
                    var employeeD = data.response.result;
                    if (!_.isEmpty(employeeD)) {
                        $scope.employee = employeeD;
                        if ($scope.employee.Photo) {
                            $scope.profileImage = 'data:image/png;base64,' + $scope.employee.Photo;
                        }


                    }
                } else {
                    toastr.warning(data.response.msg +
                        "Please contact the HUMAN RESOURCE OFFICER or SYSTEM ADMINISTRATOR for this matter", 'WARNING');
                    $timeout(function () {
                        $state.go('main.manualpayroll', {
                            emp_id: null
                        });
                    }, 300);
                    return;
                }
            });
        };

        if (localStorageService.get('kiosk.user')) {
            $scope.currentUser = localStorageService.get('kiosk.user').user;
            $scope.getEmployeeProfile($scope.currentUser.E_UUID);
        }

    }
})();