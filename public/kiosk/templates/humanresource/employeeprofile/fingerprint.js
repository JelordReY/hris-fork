(function () {
    'use strict';
    angular.module('kiosk')
        .controller('employeeFingerPrintCtrl', employeeFingerPrintCtrl);

    employeeFingerPrintCtrl.$inject = ['$scope', '$uibModalInstance', 'employee', '$timeout', 'employeeD'];

    function employeeFingerPrintCtrl($scope, $uibModalInstance, employee, $timeout, employeeD) {
        console.log('employeeD: ', employeeD);
        $scope.finger = {};
        if (employeeD.Emp_UUID) {
            employee.Get_Employee_Fingerprint(employeeD.Emp_UUID).then(function (data) {
                if (data.statusCode == 200 && data.response.success) {
                    var finger = data.response.result;
                    if (!_.isEmpty(finger)) {
                        if (finger.RThumb_Pic1) {
                            finger.RThumb_Pic1 = 'data:image/png;base64,' + finger.RThumb_Pic1;
                        }
                        if (finger.RThumb_Pic2) {
                            finger.RThumb_Pic2 = 'data:image/png;base64,' + finger.RThumb_Pic2;
                        }
                        if (finger.RThumb_Pic3) {
                            finger.RThumb_Pic3 = 'data:image/png;base64,' + finger.RThumb_Pic3;
                        }
                        if (finger.RThumb_Pic4) {
                            finger.RThumb_Pic4 = 'data:image/png;base64,' + finger.RThumb_Pic4;
                        }


                        if (finger.LThumb_Pic1) {
                            finger.LThumb_Pic1 = 'data:image/png;base64,' + finger.LThumb_Pic1;
                        }
                        if (finger.LThumb_Pic2) {
                            finger.LThumb_Pic2 = 'data:image/png;base64,' + finger.LThumb_Pic2;
                        }
                        if (finger.LThumb_Pic3) {
                            finger.LThumb_Pic3 = 'data:image/png;base64,' + finger.LThumb_Pic3;
                        }
                        if (finger.LThumb_Pic4) {
                            finger.LThumb_Pic4 = 'data:image/png;base64,' + finger.LThumb_Pic4;
                        }


                        if (finger.FingerBck1_pic1) {
                            finger.FingerBck1_pic1 = 'data:image/png;base64,' + finger.FingerBck1_pic1;
                        }
                        if (finger.FingerBck1_pic2) {
                            finger.FingerBck1_pic2 = 'data:image/png;base64,' + finger.FingerBck1_pic2;
                        }
                        if (finger.FingerBck1_pic3) {
                            finger.FingerBck1_pic3 = 'data:image/png;base64,' + finger.FingerBck1_pic3;
                        }
                        if (finger.FingerBck1_pic4) {
                            finger.FingerBck1_pic4 = 'data:image/png;base64,' + finger.FingerBck1_pic4;
                        }


                        if (finger.FingerBck2_pic1) {
                            finger.FingerBck2_pic1 = 'data:image/png;base64,' + finger.FingerBck2_pic1;
                        }
                        if (finger.FingerBck2_pic2) {
                            finger.FingerBck2_pic2 = 'data:image/png;base64,' + finger.FingerBck2_pic2;
                        }
                        if (finger.FingerBck2_pic3) {
                            finger.FingerBck2_pic3 = 'data:image/png;base64,' + finger.FingerBck2_pic3;
                        }
                        if (finger.FingerBck2_pic4) {
                            finger.FingerBck2_pic4 = 'data:image/png;base64,' + finger.FingerBck2_pic4;
                        }

                        $scope.finger = finger;
                    }
                }
            });
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

})();