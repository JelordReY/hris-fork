(function () {
    'use strict';

    angular.module('kiosk')
        .controller('employeeUpdateModalCtrl', employeeUpdateModalCtrl);

    employeeUpdateModalCtrl.$inject = ['$scope', '$uibModalInstance', 'employee', 'civilstatus', 'employmentstatus', 'branch', 'division', 'taxexemption', 'department', 'jobposition', 'Payroll', '$state', 'toastr', 'employeeD','$timeout'];

    function employeeUpdateModalCtrl($scope, $uibModalInstance, employee, civilstatus, employmentstatus, branch, division, taxexemption, department, jobposition, Payroll, $state, toastr, employeeD,$timeout) {
        $scope.civilstatus = [];
        $scope.employmentstatus = [];
        $scope.branches = [];
        $scope.divisions = [];
        $scope.taxexemption = [];
        $scope.departments = [];
        $scope.jobpositions = [];
        $scope.paytypes = [];

        $scope.employee = {};
        $scope.employee.e_birthdate = new Date();
        $scope.employee.AppointedDate = new Date();
        $scope.employee.e_dateemployed = new Date();
        $scope.employee.e_gender = 'M';
        $scope.employee.IsTin = false;
        $scope.employee.TAX_FIXED = false;
        $scope.employee.IsSSS = false;
        $scope.employee.SSS_FIXED = false;
        $scope.employee.IsPh = false;
        $scope.employee.PH_FIXED = false;
        $scope.employee.IsPagIbig = false;
        $scope.employee.PAGIBIG_FIXED = false;
        $scope.employee.InActive = 0;
        $scope.employee.chkSeperation = false;
        $scope.employee.chkRetired = false;

        $scope.popup1 = {
            opened: false
        };
        $scope.popup2 = {
            opened: false
        };
        $scope.popup3 = {
            opened: false
        };

        $scope.dateOptions = {
            formatYear: 'yyyy'
        };

        async.waterfall([
            function (callback) {
                civilstatus.getAllCivilStatus().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var civilstatus = data.response.result;
                        if (!_.isEmpty(civilstatus)) {
                            $scope.civilstatus = civilstatus;
                        }
                        callback()
                    }
                });
            },
            function (callback) {
                employmentstatus.getAllEmploymentStatus().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var employmentstatus = data.response.result;
                        if (!_.isEmpty(employmentstatus)) {
                            $scope.employmentstatus = employmentstatus;
                        }
                        callback();
                    }
                });
            },
            function (callback) {
                branch.getAllBranch().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var branches = data.response.result;
                        if (!_.isEmpty(branches)) {
                            $scope.branches = branches;
                        }
                        callback();
                    }
                });
            },
            function (callback) {
                branch.getAllBranch().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var branches = data.response.result;
                        if (!_.isEmpty(branches)) {
                            $scope.branches = branches;
                        }
                        callback();
                    }
                });
            },
            function (callback) {
                taxexemption.getAllTaxExemptions().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var taxexemption = data.response.result;
                        if (!_.isEmpty(taxexemption)) {
                            $scope.taxexemption = taxexemption;
                        }
                        callback();
                    }
                });
            },
            function (callback) {
                department.getAllDepartments().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var departments = data.response.result;
                        if (!_.isEmpty(departments)) {
                            $scope.departments = departments;
                        }
                    }
                    callback();
                });
            },
            function (callback) {
                jobposition.getAllJobPosition().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var jobpositions = data.response.result;
                        if (!_.isEmpty(jobpositions)) {
                            $scope.jobpositions = jobpositions;
                        }
                        callback();
                    }
                });
            },
            function (callback) {
                Payroll.getAllPayTypes().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var paytypes = data.response.result;
                        if (!_.isEmpty(paytypes)) {
                            $scope.paytypes = paytypes;
                        }
                        callback();
                    }
                });
            },
            function (callback) {
                division.getAllDivisions().then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var divisions = data.response.result;
                        if (!_.isEmpty(divisions)) {
                            $scope.divisions = divisions;
                        }
                        callback();
                    }
                });
            },
            function (callback) {
                employee.getEmployeeProfile(employeeD.Emp_UUID, {}).then(function (data) {
                    if (data.statusCode == 200 && data.response.success) {
                        var employee = data.response.result;
                        if (!_.isEmpty(employee)) {
                            $scope.employee = employee;
                            $scope.employee.e_birthdate = new Date($scope.employee.e_birthdate);

                            if (!_.isEmpty($scope.employee.DateRetired)) {
                                $scope.employee.DateRetired = new Date($scope.employee.DateRetired);
                                $scope.employee.chkRetired = true;
                            } else {
                                $scope.employee.chkRetired = false;
                            }

                            if (!_.isEmpty($scope.employee.DateResigned)) {
                                $scope.employee.DateResigned = new Date($scope.employee.DateResigned);
                                $scope.employee.chkSeperation = true;
                            } else {
                                $scope.employee.chkSeperation = false;
                            }

                            $scope.employee.e_dateemployed = new Date($scope.employee.e_dateemployed);
                            $scope.employee.AppointedDate = new Date($scope.employee.AppointedDate);
                        }
                    }
                });
            }
        ])


        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };
        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };
        $scope.open3 = function () {
            $scope.popup3.opened = true;
        };

        $scope.genderChanged = function () {
            console.log('genderChanged: ', $scope.employee.e_gender);
        };

        $scope.chkSSS_CheckedChanged = function () {
            console.log('chkSSS_CheckedChanged: ', $scope.employee.IsSSS);
        };

        $scope.chkSSSAttrib_CheckedChanged = function () {
            console.log('chkSSSAttrib_CheckedChanged: ', $scope.employee.SSS_FIXED);
        };

        $scope.chkPhilHealth_CheckedChanged = function () {
            console.log('chkPhilHealth_CheckedChanged', $scope.employee.IsPh);
        };

        $scope.chkPHAttrib_CheckedChanged = function () {
            console.log('chkPHAttrib_CheckedChanged: ', $scope.employee.PH_FIXED);
        };

        $scope.chkPAGIBIG_CheckedChanged = function () {
            console.log('chkPAGIBIG_CheckedChanged: ', $scope.employee.IsPagIbig)
        };

        $scope.chkLoveAttrib_CheckedChanged = function () {
            console.log('chkLoveAttrib_CheckedChanged: ', $scope.employee.PAGIBIG_FIXED);
        };

        $scope.chkTIN_CheckedChanged = function () {
            console.log('chkTIN_CheckedChanged: ', $scope.employee.IsTin);
        };

        $scope.chkTINAttrib_CheckedChanged = function () {
            console.log('chkTINAttrib_CheckedChanged: ', $scope.employee.TAX_FIXED);
        };

        $scope.chkSeperation_CheckedChanged = function () {
            if ($scope.employee.chkSeperation) {
                $scope.employee.DateResigned = new Date();
            } else {
                delete $scope.employee.DateResigned;
            }
        };

        $scope.chkRetired_CheckedChanged = function () {
            if ($scope.employee.chkRetired) {
                $scope.employee.DateRetired = new Date();
            } else {
                delete $scope.employee.DateRetired;
            }
        };

        $scope.saveEntry = function () {
            console.log('employee: ', $scope.employee);
            employee.correctionEmployee($scope.employee.Emp_UUID, $scope.employee).then(function (data) {
                if (data.statusCode == 200 && data.response.success) {
                    var result = data.response.result;
                    toastr.success(data.response.msg, 'Success');
                    $timeout(function(){
                        $uibModalInstance.close(result);
                    },300);
                } else if (data.statusCode === 400 && !data.response.success && _.isArray(data.response.result)) {
                    _.each(data.response.result, function (msg) {
                        toastr.warning(msg.msg, 'Warning');
                    });
                } else if (data.statusCode === 401 && !data.response.success && _.isArray(data.response.result)) {
                    _.each(data.response.result, function (msg) {
                        toastr.warning(msg.msg, 'Warning');
                    });
                } else {
                    toastr.error(data.msg, 'Error');
                }
            });
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

})();