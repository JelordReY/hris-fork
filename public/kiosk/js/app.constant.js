(function () {
    'use strict';

    angular.module('kiosk')
        .constant('API_URL', window.location.origin)
        // .constant('API_URL', 'http://52.63.183.21:4000')
        .constant('API_VERSION', '/api/v1');
})();