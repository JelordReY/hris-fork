(function(){
    'use strict';
    
    angular.module('kiosk', [
            'ngAnimate', 'ui.select',
            'ngResource', 'ngSanitize',
            'ui.router', 'ui.bootstrap',
            'toastr', 'ngDialog',
            'ngTable', 'ngCookies',
            'angular-loading-bar', 'ngActivityIndicator',
            'angularMoment', 'LocalStorageModule',
            'restangular', 'angularTreeview', 'ngFileUpload', 'ngImgCrop',
            'AngularPrint'
        ]);
})();