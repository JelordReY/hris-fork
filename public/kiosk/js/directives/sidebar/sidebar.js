'use strict';

angular.module('kiosk')
    .directive('sidebar', function() {
        return {
            restrict: 'AE',
            templateUrl: 'public/kiosk/js/directives/sidebar/sidebar.html'
        };
    });
  