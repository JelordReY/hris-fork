(function() {
    'use strict';

    angular.module('kiosk')
        .factory('workschedules', ['Restangular', function(Restangular) {
            return {
                getAllDays: function() {
                    return Restangular.all('days').customGET().then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },
                getAllWorkSchedules: function() {
                    return Restangular.all('workschedules').customGET().then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },
                getWorkSchedulesTemplates: function(sched_id) {
                    return Restangular.all('workschedules').customGET(sched_id).then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },
                getWorkScheduleDetails: function(sched_id) {
                    return Restangular.all('workschedules/' + sched_id + '/details').customGET().then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                }
            };
        }]);

})();