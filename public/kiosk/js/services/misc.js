(function() {
    'use strict';

    angular.module('kiosk')
        .factory('miscFctry', ['Restangular', function(Restangular) {
            return {
                getAllReligion: function() {
                    return Restangular.all('religions').customGET().then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                },
                getAllNationality: function() {
                    return Restangular.all('nationality').customGET().then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                }
            };
        }])
        .factory('authInterceptor', authInterceptor);

    authInterceptor.$inject = ['$rootScope', '$q', '$location', '$injector', 'localStorageService'];


    function authInterceptor($rootScope, $q, $location, $injector, localStorageService) {
        return {
            request: function(config) {
                config.headers = config.headers || {};

                if (localStorageService.get('kiosk.user')) {
                    var token = localStorageService.get('kiosk.user').token;
                    var authdata = localStorageService.get('kiosk.authdata');

                    config.headers.auth_token = token;
                    if (authdata) {
                        config.headers.Authorization = 'Basic ' + authdata;
                    }
                }

                return config || $q.when(config);
            },
            response: function(response) {
                return response || $q.when(response);
            },
            responseError: function(response) {
                if (response.status === 401) {
                    $rootScope.$broadcast('unauthorized');
                    $location.path('/login');
                } else if (response.status === 403) {
                    $rootScope.$broadcast('unauthorized');
                    $location.path('/login');
                }
                return $q.reject(response);
            }
        };
    }

})();