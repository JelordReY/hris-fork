(function() {
    'use strict';

    angular.module('kiosk')
        .factory('taxexemption', ['Restangular', function(Restangular) {
            return {
                getAllTaxExemptions: function() {
                    return Restangular.all('taxexemption').customGET().then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                }
            };
        }]);

})();