(function () {
    'use strict';

    angular.module('kiosk')
        .factory('employmentstatus', ['Restangular', function (Restangular) {
            return {
                getEmploymentStatusById: function (id) {
                    return Restangular.all('employmentstatus').customGET(id).then(function (res) {
                        return res;
                    }, function (err) {
                        return err.data;

                    });
                },
                getAllEmploymentStatus: function () {
                    return Restangular.all('employmentstatus').customGET().then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                }
            };
        }]);
})();