(function () {
    'use strict';

    angular.module('kiosk')
        .factory('jobposition', ['Restangular', function (Restangular) {
            return {
                getGroupById: function (id) {
                    return Restangular.all('jobposition').customGET(id).then(function (res) {
                        return res;
                    }, function (err) {
                        return err.data;

                    });
                },
                getJobPositionById: function (id) {
                    return Restangular.all('jobposition').customGET(id).then(function (res) {
                        return res;
                    }, function (err) {
                        return err.data;

                    });
                },
                getAllGroup: function () {
                    return Restangular.all('jobposition/group').customGET().then(function (res) {
                        return res;
                    }, function (err) {
                        return err.data;

                    });
                },
                getAllClass: function () {
                    return Restangular.all('jobposition/class').customGET().then(function (res) {
                        return res;
                    }, function (err) {
                        return err.data;

                    });
                },
                getAllJobPosition: function () {
                    return Restangular.all('jobposition').customGET().then(function (res) {
                            return res;
                        },
                        function (err) {
                            return err.data;
                        });
                }
            };
        }]);
})();