(function() {
    'use strict';

    angular.module('kiosk')
        .factory('department', ['Restangular', function(Restangular) {
            return {
                getDepartmentsById: function(id) {
                    return Restangular.all('departments').customGET(id).then(function(res) {
                        return res;
                    }, function(err) {
                        return err.data;

                    });
                },
                getAllDepartments: function() {
                    return Restangular.all('departments').customGET().then(function(res) {
                            return res;
                        },
                        function(err) {
                            return err.data;
                        });
                }
            };
        }]);

})();