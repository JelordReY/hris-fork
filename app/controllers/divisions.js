'use strict';

var divisionDao = require('../services/divisions');

function Divisions() {
    this.divisionDao = divisionDao;
}

Divisions.prototype.getAllDivisions = (next) => {
    divisionDao.getAllDivisions((err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};


Divisions.prototype.Add_Divisions = (data, next) => {
    divisionDao.Add_Divisions(data, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }
        next(null, {


            msg: 'Division successfully saved',
            result: response,
            success: true
        });
    });
};

Divisions.prototype.getDivisionsById = (div_id, next) => {
    divisionDao.getDivisionsById(div_id, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        if (response && response.length > 0) {
            response = response[0];
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};



Divisions.prototype.deleteDivision = (div_id, next) => {
    divisionDao.deleteDivision(div_id, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Division successfully deleted',
            result: response,
            success: true
        });
    });
};


Divisions.prototype.updateDivision = (div_id, data, next) => {
    divisionDao.updateDivision(div_id, data, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Division successfully updated',
            result: response,
            success: true
        });
    });
};

exports.Divisions = Divisions;