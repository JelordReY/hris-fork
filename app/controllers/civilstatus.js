'use strict';

var civilStatusDao = require('../services/civilstatus');

function CivilStatus() {
    this.civilStatusDao = civilStatusDao;
}


CivilStatus.prototype.Add_CivilStatus = (data, next) => {
    civilStatusDao.Add_CivilStatus(data, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};

CivilStatus.prototype.getAllCivilStatus = (next) => {
    civilStatusDao.getAllCivilStatus((err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

CivilStatus.prototype.getCivilStatusById = (civilstat_id, next) => {
    civilStatusDao.getCivilStatusById(civilstat_id, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        if (response && response.length > 0) {
            response = response[0];
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};


CivilStatus.prototype.deleteCivilStatus = (civilstat_id, next) => {
    civilStatusDao.deleteCivilStatus(civilstat_id, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};

CivilStatus.prototype.updateCivilStatus = (cs_id, data, next) => {
    civilStatusDao.updateCivilStatus(cs_id, data, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully updated',
            result: response,
            success: true
        });
    });
};
exports.CivilStatus = CivilStatus;