'use strict';

var employeeDao = require('../services/employees');

var func = require('../../app/utils/func');
var _ = require('lodash-node');
var async = require('async');
var fs = require('fs');

function Employees() {
    this.employeeDao = employeeDao;
}

Employees.prototype.GetNewEmployeeID = (next) => {
    employeeDao.GetNewEmployeeID((err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        if (response && response.length > 0) {
            response = response[0];
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Employees.prototype.Add_Employee = (data, next) => {
    employeeDao.Add_Employee(data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Employee successfully added',
            result: response,
            success: true
        });
    });
};

Employees.prototype.Add_Employee_Family_Background = (e_id, data, next) => {
    async.waterfall([
        (callback) => {
            employeeDao.GetEmployeeFamilyBackground(e_id, (err, response) => {
                if (err) {
                    next({
                        msg: '',
                        result: err,
                        success: false
                    }, null);
                }
                if (response && response.length > 0) {
                    callback(null, 'update');
                } else {
                    callback(null, 'insert');
                }
            });
        },
        (action, callback) => {
            console.log('Add_Employee_Family_Background action: ', action);
            if (action == 'insert') {
                employeeDao.Add_Employee_Family_Background(data, (err, response) => {
                    if (err) {
                        next({
                            msg: '',
                            result: err,
                            success: false
                        }, null);
                    }

                    callback(null, {
                        msg: 'Employee Family Record Successfully saved',
                        result: response,
                        success: true
                    });
                });
            } else {
                employeeDao.Update_Employee_Family_Background(e_id, data, (err, response) => {
                    if (err) {
                        next({
                            msg: '',
                            result: err,
                            success: false
                        }, null);
                    }

                    callback(null, {
                        msg: 'Employee Family Record Successfully updated',
                        result: response,
                        success: true
                    });
                });
            }
        }
    ], next);
};

Employees.prototype.Add_Employee_Children = (e_id, data, next) => {
    employeeDao.Add_Employee_Children(e_id, data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Employee children successfully saved',
            result: response,
            success: true
        });
    });
};

Employees.prototype.Correction_Employee = (e_id, data, next) => {
    employeeDao.Correction_Employee(e_id, data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Employee Information Successfully updated',
            result: response,
            success: true
        });
    });
};

Employees.prototype.Update_Employee = (id, data, next) => {
    employeeDao.Update_Employee(id, data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Employee Information Successfully updated',
            result: response,
            success: true
        });
    });
};

Employees.prototype.Update_Employee_Education = (e_id, data, next) => {
    async.waterfall([
        (callback) => {
            employeeDao.GetEmployeeEducation(e_id, (err, response) => {
                if (err) {
                    next({
                        msg: '',
                        result: err,
                        success: false
                    }, null);
                }
                if (response && response.length > 0) {
                    callback(null, 'update');
                } else {
                    callback(null, 'insert');
                }
            })
        },
        (action, callback) => {
            console.log('Update_Employee_Education action: ', action);
            if (action == 'insert') {
                employeeDao.Add_Employee_Education(e_id, data, (err, response) => {
                    if (err) {
                        next({
                            msg: '',
                            result: err,
                            success: false
                        }, null);
                    }
                    callback(null, {
                        msg: 'Employee Education Successfully saved',
                        result: response,
                        success: true
                    });
                });
            } else {
                employeeDao.Update_Employee_Education(e_id, data, (err, response) => {
                    if (err) {
                        next({
                            msg: '',
                            result: err,
                            success: false
                        }, null);
                    }
                    callback(null, {
                        msg: 'Employee Education Successfully updated',
                        result: response,
                        success: true
                    });
                });
            }
        }
    ], next);
};

Employees.prototype.Delete_Employee = (id, next) => {
    employeeDao.Delete_Employee(id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Employee Successfully deleted',
            result: response,
            success: true
        });
    });
};


Employees.prototype.GetEmployeeProfile = (id, next) => {
    async.waterfall([
        (callback) => {
            employeeDao.IsEmployeeActive(id, (err, response) => {
                if (err) {
                    next({
                        msg: '',
                        result: err,
                        success: false
                    }, null);
                }
                if (response && response.length > 0) {
                    return next(null, {
                        msg: 'Unable to proceed.The system found that the selected employee is INACTIVE ON SERVICE.',
                        result: null,
                        success: false
                    });
                } else {
                    callback();
                }
            });
        },
        (callback) => {
            employeeDao.IsEmployeeResigned(id, (err, response) => {
                if (err) {
                    next({
                        msg: '',
                        result: err,
                        success: false
                    }, null);
                }
                if (response && response.length > 0) {
                    return next(null, {
                        msg: 'Unable to proceed.The system found that the selected employee is RESIGNED ON SERVICE.',
                        result: null,
                        success: false
                    });
                } else {
                    callback();
                }
            });
        },
        (callback) => {
            employeeDao.IsEmployeeRetired(id, (err, response) => {
                if (err) {
                    next({
                        msg: '',
                        result: err,
                        success: false
                    }, null);
                }
                if (response && response.length > 0) {
                    return next(null, {
                        msg: 'Unable to proceed.The system found that the selected employee is RETIRED ON SERVICE.',
                        result: null,
                        success: false
                    });
                } else {
                    callback();
                }
            });
        },
        (callback) => {
            employeeDao.GetEmployeeProfile(id, (err, response) => {
                if (err) {
                    next({
                        msg: '',
                        result: err,
                        success: false
                    }, null);
                }

                if (response && response.length > 0) {
                    response = response[0];
                    // delete response.Photo;
                    if (response.Photo) {
                        var buffer = new Buffer(response.Photo, 'binary');
                        var bufferBase64 = buffer.toString('base64');
                        response.Photo = bufferBase64;
                    }

                    response.e_civil_status = parseInt(response.e_civil_status);
                    response.e_religion = parseInt(response.e_religion);

                    if (response.DateResigned == '0001-01-01') {
                        response.DateResigned = null;
                    }
                    if (response.DateRetired == '0001-01-01') {
                        response.DateRetired = null;
                    }
                    if (response.e_dateemployed == '0001-01-01') {
                        response.e_dateemployed = null;
                    }
                    if (response.e_birthdate == '0001-01-01') {
                        response.e_birthdate = null;
                    }
                    if (!_.isNull(response.e_dateemployed)) {
                        response.yearsOfService = func.getTotalYears(response.e_dateemployed);
                    } else {
                        response.yearsOfService = 0;
                    }
                    if (!_.isNull(response.e_birthdate)) {
                        response.employee_age = func.getTotalYears(response.e_birthdate);
                    } else {
                        response.employee_age = 0;
                    }
                }
                callback(null, {
                    msg: '',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};

Employees.prototype.GetEmployeeFamilyBackground = (e_id, next) => {
    employeeDao.GetEmployeeFamilyBackground(e_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        if (response && response.length > 0) {
            response = response[0];
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Employees.prototype.GetEmployeeChildren = (e_id, next) => {
    employeeDao.GetEmployeeChildren(e_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Employees.prototype.deleteEmployeeChildren = (e_id, _id, next) => {
    employeeDao.deleteEmployeeChildren(e_id, _id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};

Employees.prototype.GetEmployeeEducation = (e_id, next) => {
    employeeDao.GetEmployeeEducation(e_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        if (response && response.length > 0) {
            response = response[0];
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Employees.prototype.employeeMasterlist = (params, next) => {
    employeeDao.employeeMasterlist(params, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        _.each(response, (row) => {
            if (row.Photo) {
                var buffer = new Buffer(row.Photo, 'binary');
                var bufferBase64 = buffer.toString('base64');
                row.Photo = bufferBase64;
            }
        });

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};


Employees.prototype.employeePickerData = (limit, next) => {
    employeeDao.employeePickerData(limit, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Employees.prototype.TagEmployeeWorkSchedule = (emp_id, sched_id, next) => {
    employeeDao.TagEmployeeWorkSchedule(emp_id, sched_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Employee Schedule tagged successfully',
            result: response,
            success: true
        });
    });
};

Employees.prototype.UnTagEmployeeWorkSchedule = (emp_id, sched_id, next) => {
    employeeDao.UnTagEmployeeWorkSchedule(emp_id, sched_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Employee Schedule Untag successfully',
            result: response,
            success: true
        });
    });
};

Employees.prototype.Update_Employee_ID = (emp_id, data, next) => {
    async.waterfall([
        (callback) => {
            employeeDao.getEmployeeID(data.new_e_id, (err, response) => {
                if (err) {
                    next({
                        msg: '',
                        result: err,
                        success: false
                    }, null);
                }

                if (response && response.length > 0) {
                    next(null, {
                        msg: 'Employee ID already existed in the records',
                        result: response,
                        success: false
                    });
                } else {
                    callback();
                }
            });
        },
        (callback) => {
            employeeDao.Update_Employee_ID(emp_id, data, (err, response) => {
                if (err) {
                    next({
                        msg: '',
                        result: err,
                        success: false
                    }, null);
                }
                callback(null, {
                    msg: 'Employee ID successfully updated',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};

Employees.prototype.Remove_Employee_Photo = (emp_id, next) => {
    employeeDao.Remove_Employee_Photo(emp_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Employee Photo successfully deleted',
            result: response,
            success: true
        });
    });
};

Employees.prototype.Add_Employee_Photo = (emp_id, data, next) => {
    employeeDao.Add_Employee_Photo(emp_id, data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        fs.unlinkSync(data.path);
        next(null, {
            msg: 'Employee Photo successfully saved',
            result: response,
            success: true
        });
    });
};

Employees.prototype.getEmployeeFingerPrint = (emp_id, next) => {
    employeeDao.getEmployeeFingerPrint(emp_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        _.each(response, (row) => {
            if (row.RThumb_Pic1) {
                var buffer = new Buffer(row.RThumb_Pic1, 'binary');
                var bufferBase64 = buffer.toString('base64');
                row.RThumb_Pic1 = bufferBase64;
            }
            if (row.RThumb_Pic2) {
                var buffer = new Buffer(row.RThumb_Pic2, 'binary');
                var bufferBase64 = buffer.toString('base64');
                row.RThumb_Pic2 = bufferBase64;
            }
            if (row.RThumb_Pic3) {
                var buffer = new Buffer(row.RThumb_Pic3, 'binary');
                var bufferBase64 = buffer.toString('base64');
                row.RThumb_Pic3 = bufferBase64;
            }
            if (row.RThumb_Pic4) {
                var buffer = new Buffer(row.RThumb_Pic4, 'binary');
                var bufferBase64 = buffer.toString('base64');
                row.RThumb_Pic4 = bufferBase64;
            }


            if (row.LThumb_Pic1) {
                var buffer = new Buffer(row.LThumb_Pic1, 'binary');
                var bufferBase64 = buffer.toString('base64');
                row.LThumb_Pic1 = bufferBase64;
            }
            if (row.LThumb_Pic2) {
                var buffer = new Buffer(row.LThumb_Pic2, 'binary');
                var bufferBase64 = buffer.toString('base64');
                row.LThumb_Pic2 = bufferBase64;
            }
            if (row.LThumb_Pic3) {
                var buffer = new Buffer(row.LThumb_Pic3, 'binary');
                var bufferBase64 = buffer.toString('base64');
                row.LThumb_Pic3 = bufferBase64;
            }
            if (row.LThumb_Pic4) {
                var buffer = new Buffer(row.LThumb_Pic4, 'binary');
                var bufferBase64 = buffer.toString('base64');
                row.LThumb_Pic4 = bufferBase64;
            }


            if (row.FingerBck1_pic1) {
                var buffer = new Buffer(row.FingerBck1_pic1, 'binary');
                var bufferBase64 = buffer.toString('base64');
                row.FingerBck1_pic1 = bufferBase64;
            }
            if (row.FingerBck1_pic2) {
                var buffer = new Buffer(row.FingerBck1_pic2, 'binary');
                var bufferBase64 = buffer.toString('base64');
                row.FingerBck1_pic2 = bufferBase64;
            }
            if (row.FingerBck1_pic3) {
                var buffer = new Buffer(row.FingerBck1_pic3, 'binary');
                var bufferBase64 = buffer.toString('base64');
                row.FingerBck1_pic3 = bufferBase64;
            }
            if (row.FingerBck1_pic4) {
                var buffer = new Buffer(row.FingerBck1_pic4, 'binary');
                var bufferBase64 = buffer.toString('base64');
                row.FingerBck1_pic4 = bufferBase64;
            }

            if (row.FingerBck2_pic1) {
                var buffer = new Buffer(row.FingerBck2_pic1, 'binary');
                var bufferBase64 = buffer.toString('base64');
                row.FingerBck2_pic1 = bufferBase64;
            }
            if (row.FingerBck2_pic2) {
                var buffer = new Buffer(row.FingerBck2_pic2, 'binary');
                var bufferBase64 = buffer.toString('base64');
                row.FingerBck2_pic2 = bufferBase64;
            }
            if (row.FingerBck2_pic3) {
                var buffer = new Buffer(row.FingerBck2_pic3, 'binary');
                var bufferBase64 = buffer.toString('base64');
                row.FingerBck2_pic3 = bufferBase64;
            }
            if (row.FingerBck2_pic4) {
                var buffer = new Buffer(row.FingerBck2_pic4, 'binary');
                var bufferBase64 = buffer.toString('base64');
                row.FingerBck2_pic4 = bufferBase64;
            }
        });

        if(response && response.length > 0){
            response = response[0];
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};


exports.Employees = Employees;