'use strict';

var companyDao = require('../services/company');
var _ = require('lodash-node');
var async = require('async');
var fs = require('fs');

function Company() {
    this.companyDao = companyDao;
}

Company.prototype.Add_Company_Info = function(data, next) {
    companyDao.Add_Company_Info(data, function(err, response) {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

<<<<<<< HEAD
        next(null, {
            msg: 'Company information successfully saved',
=======
        next(null,{
            msg: 'Record successfully saved',
>>>>>>> adcc73cce1947a0128f68dd5435f944c7c06c82c
            result: response,
            success: true
        });
    });
};


Company.prototype.DeleteCompanyProfile = (ci_id, next) => {
    companyDao.DeleteCompanyProfile(ci_id, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};


Company.prototype.getCompanyInfo = (next) => {
    companyDao.getCompanyInfo((err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        if (response && response.length > 0) {
            response = response[0];
            if (response.ci_logo || response.ci_mission || response.ci_vision || response.ci_history) {
                var buffer = new Buffer(response.ci_logo, 'binary');
                var buffer1 = new Buffer(response.ci_mission, 'binary');
                var buffer2 = new Buffer(response.ci_vision, 'binary');
                var buffer3 = new Buffer(response.ci_history, 'binary');
                var bufferBase64 = buffer.toString('base64');
                var missionBase = buffer1.toString('base64');
                var visionBase = buffer2.toString('base64');
                var historyBase = buffer3.toString('base64');
                response.ci_logo = bufferBase64;
                response.ci_mission = missionBase;
                response.ci_vision = visionBase;
                response.ci_history = historyBase;

            }
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Company.prototype.deleteCompany = (ci_id, next) => {
    companyDao.deleteCompany(ci_id, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

<<<<<<< HEAD
        next(null, {
            msg: 'Company details successfully deleted',
=======
        next(null,{
            msg: 'Record successfully deleted',
>>>>>>> adcc73cce1947a0128f68dd5435f944c7c06c82c
            result: response,
            success: true
        });
    });
};

Company.prototype.updateCompanyInfo = (ci_id, data, next) => {
    companyDao.updateCompanyInfo(ci_id, data, (err, response) => {
        if (err) {
<<<<<<< HEAD
            msg: 'Error Occured',
            next(err, null);
=======
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
>>>>>>> adcc73cce1947a0128f68dd5435f944c7c06c82c
        }

        next(null, {
            msg: 'Record successfully updated',
            result: response,
            success: true
        });
    });
};

Company.prototype.getCompanyById = (ci_id, next) => {
    companyDao.getCompanyById(ci_id, (err, response) => {
        if (err) {
            next({
                msg: err.message,
                result: err,
                success: false
            }, null);
        }

        if (response && response.length > 0) {
            response = response[0];
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};


Company.prototype.AddCompanyProfile = (ci_id, data, next) => {
    companyDao.AddCompanyProfile(ci_id, data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        fs.unlinkSync(data.path);
        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};


exports.Company = Company;