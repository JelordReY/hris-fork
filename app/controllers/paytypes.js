'use strict';

var payTypesDao = require('../services/paytypes');

function PayTypes() {
    this.payTypesDao = payTypesDao;
}


PayTypes.prototype.getAllPayTypes = (next) => {
    payTypesDao.getAllPayTypes((err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

PayTypes.prototype.updatePayTypeDivisor = (tp_id, data, next) => {
    payTypesDao.updatePayTypeDivisor(tp_id, data.divisor, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }

        next(null, {
            msg: 'Record successfully updated',
            result: response,
            success: true
        });
    });
};

exports.PayTypes = PayTypes;