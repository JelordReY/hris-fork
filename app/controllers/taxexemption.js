'use strict';

var taxExemptDao = require('../services/taxexemption');

function TaxExempt() {
    this.taxExemptDao = taxExemptDao;
}

TaxExempt.prototype.getAllTaxExemptions = (next) => {
    taxExemptDao.getAllTaxExemptions((err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

TaxExempt.prototype.GetExemptionByID = (IndexID, next) => {
    taxExemptDao.GetExemptionByID(IndexID, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        if (response && response.length > 0) {
            response = response[0];
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

TaxExempt.prototype.Add_Tax_Exemption = (data, next) => {
    taxExemptDao.Add_Tax_Exemption(data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};

TaxExempt.prototype.Update_Tax_Exemption = (IndexID, data, next) => {
    taxExemptDao.Update_Tax_Exemption(IndexID, data, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully updated',
            result: response,
            success: true
        });
    });
};

TaxExempt.prototype.Delete_Tax_Exemption = (IndexID, next) => {
    taxExemptDao.Delete_Tax_Exemption(IndexID, (err, response) => {
        if (err) {
            next({
                msg: '',
                result: err,
                success: false
            }, null);
        }
        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};

exports.TaxExempt = TaxExempt;