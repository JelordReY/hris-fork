'use strict';

var env = process.env.NODE_ENV || 'development';
var config = require('../../config/environment/' + env);

var functions = require('../../app/utils/func');
var _ = require('lodash-node');
var fs = require('fs');
var mkdirp = require('mkdirp');
var path = require('path');
var request = require('request');


function Sharer() {}


Sharer.prototype.shareExperience = function(data, next) {
    var objectData = {
        from: data.from,
        to: data.email,
        subject: data.subject,
        html: data.message,
        pdf: data.pdf
    };

    var options = {
        format: data.format || 'Letter',
        orientation: data.orientation || 'portrait',
        border: {
            top: '0.5in', // default is 0, units: mm, cm, in, px
            right: '0.5in',
            bottom: '0.5in',
            left: '0.5in'
        },
        zoom: 0.1
    };

    var sendMail = function(mailData) {

        var helper = require('sendgrid').mail;
        var from_email = new helper.Email(mailData.from);
        var to_email = new helper.Email(mailData.to);
        var subject = mailData.subject;
        var content = new helper.Content('text/html', mailData.html);


        var mail = new helper.Mail(from_email, subject, to_email, content);

        if (data.pdf) {
            var attachment = new helper.Attachment();
            var file = fs.readFileSync(mailData.attachment);
            var base64File = new Buffer(file).toString('base64');
            attachment.setContent(base64File);
            attachment.setType('application/text');
            attachment.setFilename(mailData.filename);
            attachment.setDisposition('attachment');
            mail.addAttachment(attachment);
        }


        var sg = require('sendgrid')(config.sendgrid_key);
        var request = sg.emptyRequest({
            method: 'POST',
            path: '/v3/mail/send',
            body: mail.toJSON(),
        });

        sg.API(request, function(error, response) {
            if (error) {
                next(error, null);
            }

            if (response.statusCode == 200 || response.statusCode == 202) {
                next(null, {
                    result: response.body,
                    msg: 'Email Successfully Sent',
                    success: true
                });
            } else {
                next({
                    result: error,
                    msg: error.message,
                    success: false
                }, null);
            }
        });
    };

    if (data._id) {
        objectData.filename = data._id;
        var filename = objectData.filename + '.pdf';
        objectData.filename = filename;

        var dir = 'public/tmp';

        if (!functions.existsSync(dir)) {
            mkdirp(dir, function(err) {
                if (err) {
                    next({
                        result: err,
                        msg: err.message,
                        success: false
                    }, null);
                }
            });
        }

        pdf.create(objectData.pdf, options).toFile(dir + '/' + filename, function(err, res) {
            if (err) {
                console.log('err: ', err);
                next({
                    result: err,
                    msg: err.message,
                    success: false
                }, null);
            }

            objectData.attachment = res.filename;
            sendMail(objectData);
        });
    } else {
        sendMail(objectData);
    }
};

Sharer.prototype.sendMail = function(data, next) {
    var request = require('request');
    var objectData = {};

    if (data.attachment) {
        var file = request(data.attachment);
        objectData = {
            from: data.from,
            to: data.email,
            subject: data.subject,
            html: data.message,
            attachment: file
        };

    } else {
        objectData = {
            from: data.from,
            to: data.email,
            subject: data.subject,
            html: data.message,
            attachment: null
        };
    }

    var helper = require('sendgrid').mail;
    var from_email = new helper.Email(objectData.from);
    var to_email = new helper.Email(objectData.to);
    var subject = objectData.subject;
    var content = new helper.Content('text/html', objectData.html);

    var mail = new helper.Mail(from_email, subject, to_email, content);
    
    if (objectData.attachment) {
        var attachment = new helper.Attachment();
        var file = fs.readFileSync(objectData.attachment);
        var base64File = new Buffer(file).toString('base64');
        attachment.setContent(base64File);
        attachment.setType('application/text');
        attachment.setFilename(objectData.filename);
        attachment.setDisposition('attachment');
        mail.addAttachment(attachment);
    }

    var sg = require('sendgrid')(config.sendgrid_key);
    var request = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: mail.toJSON(),
    });

    sg.API(request, function(error, response) {
        if (error) {
            next(error, null);
        }

        if (response.statusCode == 200 || response.statusCode == 202) {
            next(null, {
                result: response.body,
                msg: 'Email Successfully Sent',
                success: true
            });
        } else {
            next({
                result: error,
                msg: error.message,
                success: false
            }, null);
        }
    });
};

exports.Sharer = Sharer;
