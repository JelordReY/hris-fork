'use strict';

var usersDao = require('../services/users');
var func = require('../utils/func');

var emailCtrl = require('../controllers/email').Sharer;
var Email = new emailCtrl();

var _ = require('lodash-node');
var async = require('async');
var ejs = require('ejs');
var path = require('path');
var moment = require('moment');

function Users() {
    this.usersDao = usersDao;
}


Users.prototype.IsUserExisted = function(sUserName, next) {
    usersDao.IsUserExisted(sUserName, function(err, response) {
        if (err) {
            next(err, null);
        }

        if (!_.isEmpty(response)) {
            next(null, {
                msg: '',
                result: response,
                success: true,
                IsUserExisted: true
            });
        } else {
            next(null, {
                msg: '',
                result: response,
                success: false,
                IsUserExisted: false
            });
        }
    });
};

Users.prototype.Add_User = function(data, next) {
    async.waterfall([
        (callback) => {
            usersDao.IsUserExisted(data.UserName, function(err, response) {
                if (err) {
                    next(err, null);
                }
                if (response && response.length > 0) {
                    next(null, {
                        msg: 'Username already existed',
                        result: null,
                        success: false,
                        IsUserExisted: true
                    });
                } else {
                    callback();
                }
            });
        }, (callback) => {
            usersDao.Add_User(data, function(err, response) {
                if (err) {
                    next(err, null);
                }
                next(null, {
                    msg: 'Record Successfully saved',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};

Users.prototype.Update_User = function(id, data, next) {
    async.waterfall([
        (callback) => {
            usersDao.IsUserExisted2(id, data.UserName, function(err, response) {
                if (err) {
                    next(err, null);
                }
                if (response && response.length > 0) {
                    next(null, {
                        msg: 'Username already existed',
                        result: null,
                        success: false,
                        IsUserExisted: true
                    });
                } else {
                    callback();
                }
            });
        }, (callback) => {
            usersDao.Update_User(id, data, function(err, response) {
                if (err) {
                    next(err, null);
                }

                next(null, {
                    msg: 'Record Successfully updated',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};

Users.prototype.Update_User_Password = function(id, data, next) {
    usersDao.Update_User_Password(id, data, function(err, response) {
        if (err) {
            next(err, null);
        }
        next(null, {
            msg: 'User Password Successfully updated',
            result: response,
            success: true
        });
    });
};

Users.prototype.Update_Users_Group = function(id, data, next) {
    usersDao.Update_Users_Group(id, data, function(err, response) {
        if (err) {
            next(err, null);
        }
        next(null, {
            msg: 'User Password Successfully updated',
            result: response,
            success: true
        });
    });
};

Users.prototype.Delete_User = function(id, next) {
    usersDao.Delete_User(id, function(err, response) {
        if (err) {
            next(err, null);
        }
        next(null, {
            msg: 'Record Successfully deleted',
            result: response,
            success: true
        });
    });
};

Users.prototype.GetUserByUserID = function(id, next) {
    usersDao.GetUserByUserID(id, function(err, response) {
        if (err) {
            next(err, null);
        }
        if (!_.isEmpty(response)) {
            if (response && response.length > 0) {
                response = response[0];
                delete response.Password;
            }

            next(null, {
                msg: '',
                result: response,
                success: true,
                GetUserByUserID: true
            });
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true,
                GetUserByUserID: false
            });
        }
    });
};

Users.prototype.GetAllUsers = (next) => {
    usersDao.GetAllUsers((err, response) => {
        if (err) {
            next(err, null);
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Users.prototype.setUserPrivilege = (UserID, data, next) => {
    // usersDao.setUserPrivilege(UserID, data, (err, response) => {
    usersDao.setUserPrivilege2(UserID, data, (err, response) => {
        if (err) {
            next(err, null);
        }
        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};

Users.prototype.getUserPrivilege = (UserID, next) => {
    usersDao.getUserPrivilege(UserID, (err, response) => {
        if (err) {
            next(err, null);
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Users.prototype.setUserGroup = (UserID, data, next) => {
    usersDao.setUserGroup(UserID, data, (err, response) => {
        if (err) {
            next(err, null);
        }

        next(null, {
            msg: 'User Group successfully set',
            result: response,
            success: true
        });
    });
};

Users.prototype.Add_User_Group = (data, next) => {
    async.waterfall([
        (callback) => {
            usersDao.User_Group_isExisted(data.GroupName, (err, response) => {
                if (err) {
                    next(err, null);
                }
                if (response && response.length > 0) {
                    next(null, {
                        msg: 'User Group already existed',
                        result: null,
                        success: false
                    });
                } else {
                    callback();
                }
            });
        },
        (callback) => {
            usersDao.Add_User_Group(data, (err, response) => {
                if (err) {
                    next(err, null);
                }
                callback(null, {
                    msg: 'Record successfully saved',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};

Users.prototype.Update_User_Group = (GroupID, data, next) => {
    async.waterfall([
        (callback) => {
            usersDao.User_Group_isExisted2(GroupID, data.GroupName, (err, response) => {
                if (err) {
                    next(err, null);
                }
                if (response && response.length > 0) {
                    next(null, {
                        msg: 'User Group already existed',
                        result: null,
                        success: false
                    });
                } else {
                    callback();
                }
            });
        },
        (callback) => {
            usersDao.Update_User_Group(GroupID, data, (err, response) => {
                if (err) {
                    next(err, null);
                }
                callback(null, {
                    msg: 'Record successfully updated',
                    result: response,
                    success: true
                });
            });
        }
    ], next);
};

Users.prototype.Delete_User_Group = (GroupID, next) => {
    usersDao.Delete_User_Group(GroupID, (err, response) => {
        if (err) {
            next(err, null);
        }
        next(null, {
            msg: 'Record successfully deleted',
            result: response,
            success: true
        });
    });
};

Users.prototype.GetAllUserGroups = (next) => {
    usersDao.GetAllUserGroups((err, response) => {
        if (err) {
            next(err, null);
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Users.prototype.GetUserGroupByID = (GroupID, next) => {
    usersDao.GetUserGroupByID(GroupID, (err, response) => {
        if (err) {
            next(err, null);
        }
        if (response && response.length > 0) {
            response = response[0];
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Users.prototype.setGroupPrivilege = (GroupID, data, next) => {
    // usersDao.setGroupPrivilege(GroupID, data, (err, response) => {
    usersDao.setGroupPrivilege2(GroupID, data, (err, response) => {
        if (err) {
            next(err, null);
        }
        next(null, {
            msg: 'Record successfully saved',
            result: response,
            success: true
        });
    });
};

Users.prototype.getGroupPrivilege = (GroupID, next) => {
    usersDao.getGroupPrivilege(GroupID, (err, response) => {
        if (err) {
            next(err, null);
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Users.prototype.PopulateAccessPriviledge = (next) => {
    usersDao.PopulateAccessPriviledge((err, response) => {
        if (err) {
            next(err, null);
        }
        next(null, {
            msg: '',
            result: response,
            success: true
        });
    });
};

Users.prototype.forgotPassword = (data, next) => {
    usersDao.getUserByUsernameAndEmail(data.username, (err, response) => {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        }
        if (response && response.length > 0) {
            response = response[0];
            var newPassword = func.generateString(8);
            usersDao.Update_User_Password(response.UUID, newPassword, (err, result) => {
                if (err) {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                }

                var template = path.join(__dirname, '..', 'views/emails/forgot.ejs');
                var ejstemp = {
                    Fullname: response.FullName,
                    Email: response.Email,
                    Password: newPassword,
                    curDate: moment().format('MM-DD-YYYY')
                };
                console.log('ejstemp: ', ejstemp);
                ejs.renderFile(template, ejstemp, (err, html) => {
                    if (err) console.log(err); // Handle error
                    var mailData = {
                        from: 'info@4loop.ph',
                        email: response.Email,
                        subject: 'HRIS Password Recovery',
                        message: html,
                        attachment: null
                    };
                    // console.log('mailData: ', mailData);
                    func.sendMail(mailData, (err, mailresponse) => {
                        if (err) {
                            console.log('err: ', err.body);
                        }
                    })
                });

                next(null, {
                    success: true,
                    msg: 'Password has been successfully resetted. Kindly check your email for your new password.',
                    result: {
                        Email: response.Email,
                        Fullname: response.Fullname,
                        UserName: response.UserName,
                        EmployeeID: response.EmployeeID,
                        UUID: response.UUID
                    }
                });
            });
        } else {
            next(null, {
                result: null,
                msg: 'User does not exist with the given info.',
                success: false
            });
        }
    });
};


exports.Users = Users;