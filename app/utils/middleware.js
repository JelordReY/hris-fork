'use strict';

var env = process.env.NODE_ENV;
var config = require('../../config/environment/' + env);

var jwt = require('jsonwebtoken');
var _ = require('lodash-node');
var async = require('async');
var moment = require('moment');

var UserRights = {
    AllowRead: 1,
    AllowWrite: 2,
    AllowDelete: 3,
    AllowPrint: 4,
    AllowExport: 5
};

var isLoggedIn = function (err, req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.user) {
        return next();
    } else {
        res.send(401).json({
            response: {
                result: 'UnauthorizedError',
                success: false,
                msg: err.inner
            },
            statusCode: 401
        });
    }
};

var authorizeBasicAuth = function (req, res, next) {
    if (!req.header('auth_token')) {
        return res.status(401).json({
            response: {
                msg: 'Access to this service or resource is forbidden with the given authorization. Authorization header is missing!',
                result: null,
                success: false
            },
            statusCode: 403
        });
    }

    var payload = null;
    try {
        var token = req.headers.auth_token;
        var payload = jwt.decode(token, config.token_secret);
        // console.log('decoded: ', payload);
        if (payload.exp <= moment().unix()) {
            return res.status(401).send({
                response: {
                    msg: 'Token has expired',
                    result: null,
                    success: false
                },
                statusCode: 401
            });
        }
        
        delete payload.iat;
        delete payload.exp;
        req.user = {
            msg: 'Login successfully',
            success: true,
            result: {
                user: payload,
                token: token
            }
        };
        next();
    } catch (err) {
        return res.status(403).send({
            response: {
                msg: err.message,
                result: err,
                success: false
            },
            statusCode: 403
        });
    }
};


var allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, Authorization, X-Requested-With, Content-Type, Accept, Cache-Control,Auth_Token');
    // next();
    if (req.method === 'OPTIONS') {
        res.statusCode = 204;
        return res.end();
    } else {
        return next();
    }
};

exports.isLoggedIn = isLoggedIn;
exports.allowCrossDomain = allowCrossDomain;
exports.authorizeBasicAuth = authorizeBasicAuth;