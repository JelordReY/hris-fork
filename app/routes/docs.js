'use strict';

var fs = require('fs-extra');

module.exports = function(app) {
    app.route('/docs-v1')
        .get(function onRequest(req, res) {
            var file = 'public/docs/swagger.json';
            console.log('file: ',file);
            fs.readFile(file, 'utf8', function(err, data) {
                if (err) {
                    console.log('Error: ' + err);
                    return;
                }
                data = JSON.parse(data);
                res.send(data);
            });
        });


    app.route('/docs')
        .get(function onRequest(req, res) {
            var file = 'public/docs/index.html';
            fs.readFile(file, 'utf8', function(err, data) {
                if (err) {
                    console.log('Error: ' + err);
                    return;
                }
                res.send(data);
            });
        });

};
