'use strict';

var cb = require('./../../utils/callback');
var payrollctrl = require('../../controllers/payroll').Payroll;
var payroll = new payrollctrl();

exports.getAllMonths = (req,res)=>{
    payroll.getAllMonths(cb.setupResponseCallback(res));
};

exports.GetAllPayrollTypes = (req,res)=>{
    payroll.GetAllPayrollTypes(cb.setupResponseCallback(res));
};

exports.GetAllPayrollTypesID = (req,res)=>{
    payroll.GetAllPayrollTypesID(req.params.ptypeid,cb.setupResponseCallback(res));
};

exports.GetAllPaymentTypes = (req,res)=>{
    payroll.GetAllPaymentTypes(cb.setupResponseCallback(res));
};

exports.payrollConfig = (req, res)=> {
    payroll.payrollConfig(cb.setupResponseCallback(res));
};

exports.createPayrollConfig = (req,res)=>{
    if(req.user){
        payroll.createPayrollConfig(req.body,cb.setupResponseCallback(res));
    }else{
        res.sendStatus(401);
    }
};

exports.getPayrollConfig = (req,res)=>{
    payroll.getPayrollConfig(req.params.payroll_id,cb.setupResponseCallback(res));
};

exports.Delete_Payroll = (req,res)=>{
    payroll.Delete_Payroll(req.params.payroll_id,cb.setupResponseCallback(res));
};

exports.Update_PayrollConfig = (req,res)=>{
    payroll.Update_PayrollConfig(req.params.payroll_id,req.body,cb.setupResponseCallback(res));
};