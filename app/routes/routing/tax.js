'use strict';

var cb = require('./../../utils/callback');
var taxCtrl = require('../../controllers/tax').WTAX;
var wtax = new taxCtrl();

exports.getAllWithholdingTax = (req, res) => {
    wtax.getAllWithholdingTax(cb.setupResponseCallback(res));
};

exports.newWithholdingTax = (req, res) => {
    if (req.user) {
        req.body.UserID = req.user.result.user.UserID;
        wtax.newWithholdingTax(req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.getTaxTableByID = (req, res) => {
    if (req.query) {
        if (req.query.action == 'summary') {
            wtax.getAllTaxSummary(req.params.tax_id, cb.setupResponseCallback(res))
        } else {
            wtax.getTaxTableByID(req.params.tax_id, cb.setupResponseCallback(res));
        }
    } else {
        wtax.getTaxTableByID(req.params.tax_id, cb.setupResponseCallback(res));
    }
};

exports.updateWithholdingTax = (req, res) => {
    if (req.user) {
        req.body.UserID = req.user.result.user.UserID;
        wtax.updateWithholdingTax(req.params.tax_id, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.removeWithholdingTax = (req, res) => {
    wtax.removeWithholdingTax(req.params.tax_id, cb.setupResponseCallback(res));
};



exports.newTaxDetails = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        wtax.newTaxDetails(req.params.tax_id, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.updateTaxDetails = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        wtax.updateTaxDetails(req.params.tax_id, req.params.index_id, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.removeTaxDetailsByTAXID = (req, res) => {
    wtax.removeTaxDetailsByTAXID(req.params.tax_id, cb.setupResponseCallback(res));
};


exports.getTaxDetails = (req, res) => {
    wtax.getTaxDetails(req.params.tax_id, cb.setupResponseCallback(res));
};


exports.newTaxCeil = (req, res) => {
    wtax.newTaxCeil(req.params.tax_id, req.body, cb.setupResponseCallback(res));
};

exports.updateTaxCeil = (req, res) => {
    wtax.updateTaxCeil(req.params.index_id, req.params.tax_id, req.body, cb.setupResponseCallback(res));
};

exports.removeTaxCeilByTAXID = (req, res) => {
    wtax.removeTaxCeilByTAXID(req.params.tax_id, cb.setupResponseCallback(res));
};


exports.getTaxDueByID = (req, res) => {
    wtax.getTaxDueByID(req.params.tax_id, cb.setupResponseCallback(res));
};


exports.getTaxCeilingByID = (req, res) => {
    wtax.getTaxCeilingByID(req.params.tax_id, cb.setupResponseCallback(res));
};


exports.getTaxPercentByID = (req, res) => {
    wtax.getTaxPercentByID(req.params.tax_id, cb.setupResponseCallback(res));
};


exports.newTaxPercent = (req, res) => {
    wtax.newTaxPercent(req.params.tax_id, req.body, cb.setupResponseCallback(res));
};

exports.updateTaxPercent = (req, res) => {
    wtax.updateTaxPercent(req.params.index_id, req.params.tax_id, req.body, cb.setupResponseCallback(res));
};

exports.removeTaxPercentByTAXID = (req, res) => {
    wtax.removeTaxPercentByTAXID(req.params.tax_id, cb.setupResponseCallback(res));
};


exports.newTaxDue = (req, res) => {
    wtax.newTaxDue(req.params.tax_id, req.body, cb.setupResponseCallback(res));
};

exports.updateTaxDue = (req, res) => {
    wtax.updateTaxDue(req.params.index_id, req.params.tax_id, req.body, cb.setupResponseCallback(res));
};

exports.removeTaxdueByTAXID = (req, res) => {
    wtax.removeTaxdueByTAXID(req.params.tax_id, cb.setupResponseCallback(res));
};

exports.removeTaxdue = (req, res) => {
    wtax.removeTaxdue(req.params.index_id, cb.setupResponseCallback(res));
};