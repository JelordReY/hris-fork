'use strict';

var cb = require('./../../utils/callback');
var pagibigCtrl = require('../../controllers/pagibig').PAGIBIG;
var pagibig = new pagibigCtrl();

exports.getAllPagIBIG = (req, res) => {
    pagibig.getAllPagIBIG(cb.setupResponseCallback(res));
};

exports.Add_HDMF = (req, res) => {
    if (req.user) {
        req.body.UserID = req.user.result.user.UserID;
        pagibig.Add_HDMF(req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.Update_HDMF = (req, res) => {
    if (req.user) {
        req.body.UserID = req.user.result.user.UserID;
        pagibig.Update_HDMF(req.params.hdmf_id, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.GetHDMF_ById = (req, res) => {
    pagibig.GetHDMF_ById(req.params.hdmf_id, cb.setupResponseCallback(res));

};

exports.DeleteHDMF = (req, res) => {
    pagibig.DeleteHDMF(req.params.hdmf_id, cb.setupResponseCallback(res));
};