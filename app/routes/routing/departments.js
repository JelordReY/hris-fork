'use strict';

var cb = require('./../../utils/callback');
var departmentCtrl = require('../../controllers/departments').Departments;
var departments = new departmentCtrl();



exports.Add_Departments = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        departments.Add_Departments(req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};
exports.getAllDepartments = (req, res) => {
    departments.getAllDepartments(cb.setupResponseCallback(res));
};


exports.getDepartmentsById = (req, res) => {
    departments.getDepartmentsById(req.params.dep_id, cb.setupResponseCallback(res));
};


exports.deleteDepartments = (req, res) => {
    departments.deleteDepartments(req.params.dep_id, cb.setupResponseCallback(res));
};


exports.updateDepartments = (req, res) => {
	if(req.user) {
		req.body.CURRENT_USER = req.user.result.user
    departments.updateDepartments(req.params.dep_id, req.body, cb.setupResponseCallback(res));
}else {
	res.sendStatus(401);
}
};