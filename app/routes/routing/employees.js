'use strict';

var cb = require('./../../utils/callback');
var employeectrl = require('../../controllers/employees').Employees;
var employee = new employeectrl();

var multer = require('multer');
var fs = require('fs');
var _ = require('lodash-node');
var mkdirp = require('mkdirp');
var path = require('path');

exports.employeeMasterlist = (req, res) => {
    if (req.query) {
        if (req.query.action) {
            var action = req.query.action;
            if (action == 'picker') {
                employee.employeePickerData(req.query.limit, cb.setupResponseCallback(res));
            } else if (action == 'branch') {
                employee.employeeMasterlist({
                    action: 'branch',
                    _id: req.query._id
                }, cb.setupResponseCallback(res));
            } else if (action == 'department') {
                employee.employeeMasterlist({
                    action: 'department',
                    _id: req.query._id
                }, cb.setupResponseCallback(res));
            } else if (action == 'generate_id') {
                employee.GetNewEmployeeID(cb.setupResponseCallback(res));
            }
        } else {
            employee.employeeMasterlist({}, cb.setupResponseCallback(res));
        }
    } else {
        employee.employeeMasterlist({}, cb.setupResponseCallback(res));
    }
};

exports.Add_Employee = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        employee.Add_Employee(req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.Add_Employee_Family_Background = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        employee.Add_Employee_Family_Background(req.params.emp_id, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.Add_Employee_Children = (req, res) => {
    employee.Add_Employee_Children(req.params.emp_id, req.body, cb.setupResponseCallback(res));
};

exports.Delete_Employee = (req, res) => {
    employee.Delete_Employee(req.params.emp_id, cb.setupResponseCallback(res));
};

exports.Correction_Employee = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        employee.Correction_Employee(req.params.emp_id, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.Update_Employee = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        employee.Update_Employee(req.params.emp_id, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};


exports.Update_Employee_Education = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        employee.Update_Employee_Education(req.params.emp_id, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};


exports.GetEmployeeProfile = (req, res) => {
    if (req.query) {
        if (req.query.action) {
            var action = req.query.action;
            if (action == 'profile') {
                employee.GetEmployeeProfile(req.params.emp_id, cb.setupResponseCallback(res));
            } else if (action == 'family') {
                employee.GetEmployeeFamilyBackground(req.params.emp_id, cb.setupResponseCallback(res));
            } else if (action == 'children') {
                employee.GetEmployeeChildren(req.params.emp_id, cb.setupResponseCallback(res));
            } else if (action == 'education') {
                employee.GetEmployeeEducation(req.params.emp_id, cb.setupResponseCallback(res));
            }
        } else {
            employee.GetEmployeeProfile(req.params.emp_id, cb.setupResponseCallback(res));
        }
    } else {
        employee.GetEmployeeProfile(req.params.emp_id, cb.setupResponseCallback(res));
    }
};

exports.GetEmployeeChildren = (req, res) => {
    employee.GetEmployeeChildren(req.params.emp_id, cb.setupResponseCallback(res));
};

exports.deleteEmployeeChildren = (req, res) => {
    employee.deleteEmployeeChildren(req.params.emp_id, req.params._id, cb.setupResponseCallback(res));
};

exports.GetEmployeeFamilyBackground = (req, res) => {
    employee.GetEmployeeFamilyBackground(req.params.emp_id, cb.setupResponseCallback(res));
};

exports.GetEmployeeEducation = (req, res) => {
    employee.GetEmployeeEducation(req.params.emp_id, cb.setupResponseCallback(res));
};

exports.TagEmployeeWorkSchedule = (req, res) => {
    employee.TagEmployeeWorkSchedule(req.params.emp_id, req.params.sched_id, cb.setupResponseCallback(res));
};

exports.UnTagEmployeeWorkSchedule = (req, res) => {
    employee.UnTagEmployeeWorkSchedule(req.params.emp_id, req.params.sched_id, cb.setupResponseCallback(res));
};

exports.Update_Employee_ID = (req, res) => {
    employee.Update_Employee_ID(req.params.emp_id, req.body, cb.setupResponseCallback(res));
};

exports.Remove_Employee_Photo = (req, res) => {
    employee.Remove_Employee_Photo(req.params.emp_id, cb.setupResponseCallback(res));
};

exports.Add_Employee_Photo = (req, res) => {
    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
            var dir = 'public/uploads';
            mkdirp(dir, function (err) {
                cb(err, dir);
            });
        },
        filename: function (req, file, cb) {
            cb(null, Math.floor(Date.now() / 1000) + '_' + file.originalname);
        }
    });

    var limits = {
        fileSize: 1024 * 1024 * 2
    };

    var fileFilter = function(req, file, cb) {
        if ((file.mimetype !== 'image/png') &&
            (file.mimetype !== 'image/jpeg') &&
            (file.mimetype !== 'image/jpg') &&
            (file.mimetype !== 'image/gif') &&
            (file.mimetype !== 'image/x-ms-bmp')) {
            req.fileValidationError = 'Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp';
            return cb(null, false, new Error('Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp'));
        }

        cb(null, true);
    };

    var uploadImg = multer({
        storage: storage,
        fileFilter: fileFilter,
        limits: limits
    }).single('emp_image');


    uploadImg(req, res, function(err) {
        if (err) {
            if (err.code == 'LIMIT_FILE_SIZE') {
                return res.status(400).json({
                    response: {
                        result: 'LIMIT_FILE_SIZE',
                        success: false,
                        msg: 'File Size Limit Exceeded'
                    },
                    statusCode: 400
                });
            } else {
                return res.status(500).json({
                    response: {
                        result: err,
                        success: false,
                        msg: err.inner
                    },
                    statusCode: 500
                });
            }
        }

        if (req.fileValidationError) {
            return res.status(400).json({
                response: {
                    result: null,
                    msg: req.fileValidationError,
                    success: false
                },
                statusCode: 400
            });
        }

        console.log('req.file: ',req.file);
        req.body.toDeleteFile = false;
        if (!_.isUndefined(req.file)) {
            req.body.toDeleteFile = true;
            req.body.path = req.file.path;
            req.body.image_originalname = req.file.originalname;
            req.body.image_mimetype = req.file.mimetype;
            req.body.image_size = req.file.size;
        }
        console.log('req.body: ',req.body);
        employee.Add_Employee_Photo(req.params.emp_id, req.body, cb.setupResponseCallback(res));
    });    
};

exports.getEmployeeFingerPrint = (req,res)=>{
    employee.getEmployeeFingerPrint(req.params.emp_id,cb.setupResponseCallback(res));
};


