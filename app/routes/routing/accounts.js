'use strict';

var cb = require('./../../utils/callback');
var accountsCtrl = require('../../controllers/accounts').Accounts;
var accounts = new accountsCtrl();


exports.getAllAccountClass = (req, res) => {
    accounts.getAllAccountClass(cb.setupResponseCallback(res));
};

exports.getAccountClass = (req, res) => {
    accounts.getAccountClass(req.params.class_id, cb.setupResponseCallback(res));
};

exports.Add_Account_Class = (req, res) => {
    accounts.Add_Account_Class(req.body, cb.setupResponseCallback(res));
};

exports.Update_Account_Class = (req, res) => {
    accounts.Update_Account_Class(req.params.class_id, req.body, cb.setupResponseCallback(res));
};

exports.Delete_Account_Class = (req, res) => {
    accounts.Delete_Account_Class(req.params.class_id, cb.setupResponseCallback(res));
};


exports.getAllAccountGroups = (req, res) => {
    accounts.getAllAccountGroups(cb.setupResponseCallback(res));
};

exports.getAccountGroup = (req, res) => {
    accounts.getAccountGroup(req.params.group_id, cb.setupResponseCallback(res));
};

exports.Add_Account_Groups = (req, res) => {
    accounts.Add_Account_Groups(req.body, cb.setupResponseCallback(res));
};

exports.Update_Account_Groups = (req, res) => {
    accounts.Update_Account_Groups(req.params.group_id, req.body, cb.setupResponseCallback(res));
};

exports.Delete_Account_Groups = (req, res) => {
    accounts.Delete_Account_Groups(req.params.group_id, cb.setupResponseCallback(res));
};


exports.getAllAccountTypes = (req, res) => {
    accounts.getAllAccountTypes(cb.setupResponseCallback(res));
};

exports.getAccountType = (req, res) => {
    accounts.getAccountType(req.params.type_id, cb.setupResponseCallback(res));
};

exports.Add_Account_Types = (req, res) => {
    accounts.Add_Account_Types(req.body, cb.setupResponseCallback(res));
};

exports.Update_Account_Types = (req, res) => {
    accounts.Update_Account_Types(req.params.type_id, req.body, cb.setupResponseCallback(res));
};

exports.Delete_Account_Types = (req, res) => {
    accounts.Delete_Account_Types(req.params.type_id, cb.setupResponseCallback(res));
};


exports.getAllAccounts = (req, res) => {
    accounts.getAllAccounts(cb.setupResponseCallback(res));
};

exports.GetAccountByID = (req, res) => {
    accounts.GetAccountByID(req.params.acct_id, cb.setupResponseCallback(res));
};

exports.Add_Accounts = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        accounts.Add_Accounts(req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.Update_Accounts = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        accounts.Update_Accounts(req.params.acct_id, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};


exports.Delete_Accounts = (req, res) => {
    accounts.Delete_Accounts(req.params.acct_id, cb.setupResponseCallback(res));
};