'use strict';

var cb = require('./../../utils/callback');
var philhealthCtrl = require('../../controllers/philhealth').PHIC;
var philhealth = new philhealthCtrl();

exports.getAllPHIC = (req, res) => {
    philhealth.getAllPHIC(cb.setupResponseCallback(res));
};

exports.GetPHDetailsByPHICID = (req,res)=>{
    philhealth.GetPHDetailsByPHICID(req.params.phic_id,cb.setupResponseCallback(res));
};

exports.getPHTableById = (req, res) => {
    if (req.query) {
        if (req.query.action == 'PH') {
            philhealth.getPhilHealthDetails(req.params.phic_id, cb.setupResponseCallback(res))
        } else {
            philhealth.getPHTableById(req.params.phic_id, cb.setupResponseCallback(res));
        }
    } else {
        philhealth.getPHTableById(req.params.phic_id, cb.setupResponseCallback(res));
    }
};

exports.AddPHTable = (req, res) => {
    if (req.user) {
        req.body.UserID = req.user.result.user.UserID;
        philhealth.AddPHTable(req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.UpdatePHTable = (req,res)=>{
    if (req.user) {
        req.body.UserID = req.user.result.user.UserID;
        philhealth.UpdatePHTable(req.params.phic_id, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.DelPHTable = (req,res)=>{
    philhealth.DelPHTable(req.params.phic_id,cb.setupResponseCallback(res));
};

exports.GetPHDetailsByID = (req,res)=>{
    philhealth.GetPHDetailsByID(req.params.phic_id,req.params.indexid,cb.setupResponseCallback(res));
};

exports.AddPHDetails = (req,res)=>{
    philhealth.AddPHDetails(req.params.phic_id,req.body,cb.setupResponseCallback(res));
};

exports.UpdatePHDetails = (req,res)=>{
    philhealth.UpdatePHDetails(req.params.phic_id,req.params.indexid,req.body,cb.setupResponseCallback(res));
};

exports.DelPHDetails = (req,res)=>{
    philhealth.DelPHDetails(req.params.phic_id, req.params.indexid ,cb.setupResponseCallback(res));
};