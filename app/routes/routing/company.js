'use strict';

var cb = require('./../../utils/callback');
var companyCtrl = require('../../controllers/company').Company;
var company = new companyCtrl();

var multer = require('multer');
var fs = require('fs');
var _ = require('lodash-node');
var mkdirp = require('mkdirp');
var path = require('path');

exports.getCompanyInfo = (req, res) => {
    company.getCompanyInfo(cb.setupResponseCallback(res));
};

exports.deleteCompany = (req, res) => {
    company.deleteCompany(req.params.ci_id, cb.setupResponseCallback(res));
};

exports.updateCompanyInfo = (req, res) => {
    if (req.user) {
        req.body.UserID = req.user.result.user.UserID;
        company.updateCompanyInfo(req.params.ci_id, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.getCompanyById = (req, res) => {
    company.getCompanyById(req.params.ci_id, cb.setupResponseCallback(res));
};

exports.Add_Company_Info = (req, res) => {
    if (req.user) {
        req.body.UserID = req.user.result.user.UserID;
        company.Add_Company_Info(req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};


exports.DeleteCompanyProfile = (req, res) => {
    company.DeleteCompanyProfile(req.params.ci_id, cb.setupResponseCallback(res));
};


exports.AddCompanyProfile = (req, res) => {
    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
            var dir = 'public/uploads';
            mkdirp(dir, function (err) {
                cb(err, dir);
            });
        },
        filename: function (req, file, cb) {
            cb(null, Math.floor(Date.now() / 1000) + '_' + file.originalname);
        }
    });

    var limits = {
        fileSize: 1024 * 1024 * 2
    };

    var fileFilter = function(req, file, cb) {
        if ((file.mimetype !== 'image/png') &&
            (file.mimetype !== 'image/jpeg') &&
            (file.mimetype !== 'image/jpg') &&
            (file.mimetype !== 'image/gif') &&
            (file.mimetype !== 'image/x-ms-bmp')) {
            req.fileValidationError = 'Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp';
            return cb(null, false, new Error('Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp'));
        }

        cb(null, true);
    };

    var uploadImg = multer({
        storage: storage,
        fileFilter: fileFilter,
        limits: limits
    }).single('comp_image');


    uploadImg(req, res, function(err) {
        if (err) {
            if (err.code == 'LIMIT_FILE_SIZE') {
                return res.status(400).json({
                    response: {
                        result: 'LIMIT_FILE_SIZE',
                        success: false,
                        msg: 'File Size Limit Exceeded'
                    },
                    statusCode: 400
                });
            } else {
                return res.status(500).json({
                    response: {
                        result: err,
                        success: false,
                        msg: err.inner
                    },
                    statusCode: 500
                });
            }
        }

        if (req.fileValidationError) {
            return res.status(400).json({
                response: {
                    result: null,
                    msg: req.fileValidationError,
                    success: false
                },
                statusCode: 400
            });
        }

        console.log('req.file: ',req.file);
        req.body.toDeleteFile = false;
        if (!_.isUndefined(req.file)) {
            req.body.toDeleteFile = true;
            req.body.path = req.file.path;
            req.body.image_originalname = req.file.originalname;
            req.body.image_mimetype = req.file.mimetype;
            req.body.image_size = req.file.size;
        }
        console.log('req.body: ',req.body);
        company.AddCompanyProfile(req.params.ci_id, req.body, cb.setupResponseCallback(res));
    });    
};
