'use strict';

var cb = require('./../../utils/callback');
var civilStatusCtrl = require('../../controllers/civilstatus').CivilStatus;
var civilstatus = new civilStatusCtrl();


exports.Add_CivilStatus = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        civilstatus.Add_CivilStatus(req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.getAllCivilStatus = (req, res) => {
    civilstatus.getAllCivilStatus(cb.setupResponseCallback(res));
};


exports.getCivilStatusById = (req, res) => {
    civilstatus.getCivilStatusById(req.params.civilstat_id, cb.setupResponseCallback(res));
};

exports.deleteCivilStatus = (req, res) => {
    civilstatus.deleteCivilStatus(req.params.civilstat_id, cb.setupResponseCallback(res));
};


exports.updateCivilStatus = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        civilstatus.updateCivilStatus(req.params.civilstat_id, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};