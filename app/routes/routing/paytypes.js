'use strict';

var cb = require('./../../utils/callback');
var paytypesCtrl = require('../../controllers/paytypes').PayTypes;
var paytypes = new paytypesCtrl();

exports.getAllPayTypes = (req, res) => {
    paytypes.getAllPayTypes(cb.setupResponseCallback(res));
};

exports.updatePayTypeDivisor = (req, res) => {
    paytypes.updatePayTypeDivisor(req.params.tp_id, req.body, cb.setupResponseCallback(res));
};
