'use strict';

var cb = require('./../../utils/callback');
var attendancectrl = require('../../controllers/attendance').Attendance;
var attendance = new attendancectrl();

exports.employeeRawTimelogs = function (req, res) {
    attendance.employeeRawTimelogs(req.params.deviceno, req.query, cb.setupResponseCallback(res));
};

exports.saveEmployeeRawTimeLogs = (req, res) => {
    attendance.saveEmployeeRawTimeLogs(req.params.deviceno, req.body, cb.setupResponseCallback(res));
};

exports.deleteEmployeeRawTimelogs = (req, res) => {
    attendance.deleteEmployeeRawTimelogs(req.params.entry_id, cb.setupResponseCallback(res));
};

exports.updateEmployeeRawTimelogs = (req, res) => {
    attendance.updateEmployeeRawTimelogs(req.params.entry_id, req.body, cb.setupResponseCallback(res));
};

exports.GetEmployeeAttendance = (req, res) => {
    attendance.GetEmployeeAttendance(req.params.emp_id, req.params.payroll_id, cb.setupResponseCallback(res));
};

exports.Add_TimeKeeping = (req, res) => {
    if (req.user) {
        attendance.Add_TimeKeeping(req.params.emp_id, req.params.payroll_id, req.user.result, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.Delete_TimeKeeping = (req, res) => {
    attendance.Delete_TimeKeeping(req.params.emp_id, req.params.payroll_id, cb.setupResponseCallback(res));
};

exports.IsTimeAttendancePostedByPayrollTerm = (req, res) => {
    attendance.IsTimeAttendancePostedByPayrollTerm(req.params.emp_id, req.params.payroll_id, cb.setupResponseCallback(res));
};

exports.Post_TimeKeeping = (req, res) => {
    if (req.user) {
        attendance.Post_TimeKeeping(req.params.emp_id, req.params.payroll_id, req.user.result, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.getAllOvertime = (req, res) => {
    attendance.getAllOvertime(req.params.payroll_id, cb.setupResponseCallback(res));
};

exports.getAllOvertimeTypes = (req, res) => {
    attendance.getAllOvertimeTypes(cb.setupResponseCallback(res));
};

exports.Add_Overtime = (req, res) => {
    attendance.Add_Overtime(req.body, cb.setupResponseCallback(res));
};

exports.Add_Overtime_Details = (req, res) => {
    attendance.Add_Overtime_Details(req.params.ot_id, req.body, cb.setupResponseCallback(res));
};

exports.getEmployeeOvertime = (req, res) => {
    attendance.getEmployeeOvertime(req.params.emp_id, req.params.payroll_id, cb.setupResponseCallback(res));
};

exports.PreApprovedOvertime = (req, res) => {
    if (req.user) {
        attendance.PreApprovedOvertime(req.params.refno, req.user.result, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.PreApprovedEmployeeOvertime = (req, res) => {
    if (req.user) {
        attendance.PreApprovedEmployeeOvertime(req.params.emp_id, req.params.payroll_id, req.params.ot_id, req.user.result, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.ApprovedOvertime = (req, res) => {
    if (req.user) {
        attendance.ApprovedOvertime(req.params.refno, req.user.result, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.ApprovedEmployeeOvertime = (req, res) => {
    if (req.user) {
        attendance.ApprovedEmployeeOvertime(req.params.emp_id, req.params.payroll_id, req.params.ot_id, req.user.result, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.DeleteOvertime = (req, res) => {
    attendance.DeleteOvertime(req.params.refno, cb.setupResponseCallback(res));
};

exports.Update_OverTime_Details = (req, res) => {
    if (req.user) {
        attendance.Update_OverTime_Details(req.params.emp_id, req.params.payroll_id, req.params.ot_id, req.user.result, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.Delete_OverTime_Details = (req, res) => {
    if (req.user) {
        attendance.Delete_OverTime_Details(req.params.emp_id, req.params.payroll_id, req.params.ot_id, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};