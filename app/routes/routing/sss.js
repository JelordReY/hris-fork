'use strict';

var cb = require('./../../utils/callback');
var sssCtrl = require('../../controllers/sss').SSS;
var sss = new sssCtrl();

exports.getAllSSS = (req, res) => {
    sss.getAllSSS(req.query, cb.setupResponseCallback(res));
};

exports.AddSSSTable = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        sss.AddSSSTable(req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.UpdateSSSTable = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        sss.UpdateSSSTable(req.params.sss_id, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.getSSSByID = (req, res) => {
    if (req.query) {
        if (req.query.action == 'SSS') {
            sss.getSSSAllDetails(req.params.sss_id, cb.setupResponseCallback(res))
        } else {
            sss.getSSSByID(req.params.sss_id, req.query, cb.setupResponseCallback(res));
        }
    } else {
        sss.getSSSByID(req.params.sss_id, req.query, cb.setupResponseCallback(res));
    }
};


exports.DelSSSTable = (req, res) => {
    sss.DelSSSTable(req.params.sss_id, cb.setupResponseCallback(res));
};

exports.AddSSSBracket = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        sss.AddSSSBracket(req.params.sss_id, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.GetSSSBracketByID = (req, res) => {
    sss.GetSSSBracketByID(req.params.sss_id, req.params.sb_id, cb.setupResponseCallback(res));
};

exports.DelSSSBracket = (req, res) => {
    sss.DelSSSBracket(req.params.sss_id, req.params.sb_id, cb.setupResponseCallback(res));
};

exports.UpdateSSSBracket = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user
        sss.UpdateSSSBracket(req.params.sss_id, req.params.sb_id, req.body ,cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};