'use strict';

var cb = require('./../../utils/callback');
var thisCtrl = require('../../controllers/workschedules').WorkSchedules;
var workschedules = new thisCtrl();

exports.getAllWorkSchedules = (req, res) => {
    workschedules.getAllWorkSchedules(cb.setupResponseCallback(res));
};

exports.getWorkSchedulesTemplates = (req, res) => {
    workschedules.getWorkSchedulesTemplates(req.params.sched_id, cb.setupResponseCallback(res));
};

exports.getAllDays = (req, res) => {
    workschedules.getAllDays(cb.setupResponseCallback(res));
};

exports.getWorkScheduleDetails = (req, res) => {
    workschedules.getWorkScheduleDetails(req.params.sched_id, cb.setupResponseCallback(res));
};

exports.Add_Work_Schedule = (req, res) => {
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user;
        workschedules.Add_Work_Schedule(req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.Delete_Work_Schedule = (req,res)=>{
    workschedules.Delete_Work_Schedule(req.params.sched_id,cb.setupResponseCallback(res));
};

exports.Update_Work_Schedule = (req,res)=>{
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user;
        workschedules.Update_Work_Schedule(req.params.sched_id,req.body,cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.saveWorkScheduleTemplates = (req,res)=>{
    workschedules.saveWorkScheduleTemplates(req.params.sched_id,req.body,cb.setupResponseCallback(res));
};


exports.updateWorkScheduleTemplates = (req,res)=>{
    if (req.user) {
        req.body.CURRENT_USER = req.user.result.user;
        workschedules.updateWorkScheduleTemplates(req.params.sched_id,req.body,cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};
