'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();

var async = require('async');
var _ = require('lodash-node');



exports.getSSSAllDetails = (sss_id, next) => {
    console.log('strSQL :', strSQL)
    var strSQL = mysql.format('SELECT * , MD5(SSS_ID) AS UUID FROM sss_bracket where MD5(SSS_ID) =? ', [sss_id]);
    db.query(strSQL, next);
};


exports.getAllSSS = (params, next) => {
    if (params) {
        if (params.bracket == 'yes') {
            async.waterfall([
                (callback) => {
                    var strSQL = mysql.format('SELECT S.*,MD5(S.SSS_ID) AS UUID FROM ssstable S ORDER BY SSSCode;');
                    db.query(strSQL, (err, ssstable) => {
                        if (err) {
                            next(err, null);
                        }
                        callback(null, ssstable);
                    });
                },
                (ssstable, callback) => {
                    var strSQL = mysql.format('SELECT S.*,MD5(S.sb_id) AS UUID FROM sss_bracket S;');
                    db.query(strSQL, (err, sss_bracket) => {
                        if (err) {
                            next(err, null);
                        }
                        _.each(ssstable, (row) => {
                            row.sss_bracket = _.filter(sss_bracket, {
                                'SSS_ID': row.SSS_ID
                            }) || [];
                        });
                        callback(null, ssstable);
                    });
                }
            ], next);
        } else {
            var strSQL = mysql.format('SELECT S.*,MD5(S.SSS_ID) AS UUID FROM ssstable S ORDER BY SSSCode;');
            db.query(strSQL, next);
        }
    } else {
        var strSQL = mysql.format('SELECT S.*,MD5(S.SSS_ID) AS UUID FROM ssstable S ORDER BY SSSCode;');
        db.query(strSQL, next);
    }
};

exports.getSSSByID = (sss_id, params, next) => {
    if (params) {
        if (params.bracket == 'yes') {
            async.waterfall([
                (callback) => {
                    console.log('strSQL:', strSQL)
                    var strSQL = mysql.format('SELECT S.*,MD5(S.SSS_ID) AS UUID FROM ssstable S WHERE MD5(S.SSS_ID)=? LIMIT 1;', [sss_id]);
                    db.query(strSQL, (err, ssstable) => {
                        if (err) {
                            next(err, null);
                        }
                        callback(null, ssstable);
                    });
                },
                (ssstable, callback) => {
                    var strSQL = mysql.format('SELECT S.*,MD5(S.sb_id) AS UUID FROM sss_bracket S;');
                    db.query(strSQL, (err, sss_bracket) => {
                        if (err) {
                            next(err, null);
                        }
                        _.each(ssstable, (row) => {
                            row.sss_bracket = _.filter(sss_bracket, {
                                'SSS_ID': row.SSS_ID
                            }) || [];
                        });
                        callback(null, ssstable);
                    });
                }
            ], next);
        } else {
            var strSQL = mysql.format('SELECT S.*,MD5(S.SSS_ID) AS UUID FROM ssstable S WHERE MD5(S.SSS_ID)=? LIMIT 1;', [sss_id]);
            db.query(strSQL, next);
        }
    } else {
        var strSQL = mysql.format('SELECT S.*,MD5(S.SSS_ID) AS UUID FROM ssstable S WHERE MD5(S.SSS_ID)=? LIMIT 1;', [sss_id]);
        db.query(strSQL, next);
    }
};

exports.AddSSSTable = (S, next) => {
    var strSQL = mysql.format('INSERT INTO ssstable(SSSCode, SSSName, Description, Inactive, DateCreated, CreatedBy) VALUE (?,?,?,?,CURDATE(),?);', [
        S.SSSCode, S.SSSName, S.Description, S.Inactive || 0, S.UserID
    ]);
    console.log('strSQL:', strSQL)
    db.insertWithId(strSQL, next);
};

exports.UpdateSSSTable = (sss_id, S, next) => {
    var strSQL = mysql.format('UPDATE ssstable SET SSSCode=?,SSSName=?,Description=?,Inactive=?,DateModified = CURDATE(),ModifiedBy=? WHERE MD5(SSS_ID)=?;', [
        S.SSSCode, S.SSSName, S.Description, S.Inactive || 0, S.UserID, sss_id
    ]);
    db.actionQuery(strSQL, next);
};

exports.DelSSSTable = (sss_id, next) => {
    var strSQL = mysql.format('DELETE FROM ssstable WHERE MD5(SSS_ID)=?;', [sss_id]);
    db.actionQuery(strSQL, next);
};

exports.AddSSSBracket = (sss_id, S, next) => {
    console.log('strSQL:' , strSQL)
    var strSQL = mysql.format('INSERT INTO sss_bracket(sb_from , sb_to, sb_employer_contrib, sb_employee_contrib , sb_emergency_contrib  , sb_addedby, sb_dateadded , SSS_ID , over , sb_monthly_credit) VALUES (?,?,?,?,?,?, NOW(), ?,?,?);', [
        S.sb_from, S.sb_to, S.sb_employer_contrib, S.sb_employee_contrib, S.sb_emergency_contrib, S.CURRENT_USER.UserID, sss_id - (1000), S.over, S.sb_monthly_credit
    ]);
    db.insertWithId(strSQL, next);
};

exports.UpdateSSSBracket = (sb_id, sss_id , S, next) => {
    var strSQL = mysql.format('UPDATE sss_bracket SET sb_from=?, sb_to=?,sb_employer_contrib=?, sb_employee_contrib=?, sb_emergency_contrib =?,sb_modifiedby=?, sb_datemodified = CURDATE() , over =?, sb_monthly_credit=? WHERE sb_id=?;', [
        S.sb_from, S.sb_to, S.sb_employer_contrib, S.sb_employee_contrib, S.sb_emergency_contrib,  S.CURRENT_USER.UserID , S.over, S.sb_monthly_credit, sb_id
    ]);
    db.actionQuery(strSQL, next);
};

exports.DelSSSBracket = (sb_id, sss_id, next) => {
    var strSQL = mysql.format('DELETE FROM sss_bracket WHERE sb_id =?;', [sb_id]);
    db.actionQuery(strSQL, next);
};  

exports.GetSSSBracketByID = (sb_id, sss_id, next) => {
    var strSQL = mysql.format('SELECT * FROM sss_bracket S WHERE sb_id =? LIMIT 1;', [sb_id]);
    db.query(strSQL, next);
};