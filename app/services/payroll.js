'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();
var async = require('async');
var _ = require('lodash-node');
var moment = require('moment');

exports.getAllMonths = (next) => {
    var strSQL = mysql.format('SELECT * FROM months;');
    db.query(strSQL, next);
};

exports.GetAllPayrollTypes = (next) => {
    var strSQL = mysql.format('SELECT p.*, MD5(p.PTypeID) AS UUID FROM payrolltypes p;');
    db.query(strSQL, next);
};

exports.GetAllPayrollTypesID = (PTypeID, next) => {
    var strSQL = mysql.format('SELECT p.*, MD5(p.PTypeID) AS UUID FROM payrolltypes p WHERE MD5(p.PTypeID)=? LIMIT1;', [PTypeID]);
    db.query(strSQL, next);
};

exports.GetAllPaymentTypes = (next) => {
    var strSQL = mysql.format('SELECT p.*,MD5(p.EntryID) AS UUID FROM paymenttypes p');
    db.query(strSQL, next);
};



exports.payrollConfig = (next) => {
    async.waterfall([
        (callback) => {
            var str = mysql.format('SELECT P.*,tp.tp_name,MD5(P.PayrollID) AS UUID FROM payroll P LEFT JOIN paytypes tp ON P.CategoryID = tp.tp_id ORDER BY PayrollID DESC;');
            db.query(str, (err, payrolls) => {
                if (err) {
                    next(err, null);
                }
                callback(null, payrolls);
            });
        },
        (payrolls, callback) => {
            var strSQL = mysql.format('SELECT * FROM paymenttypes');
            db.query(strSQL, (err, paymenttypes) => {
                if (err) {
                    next(err, null);
                }
                _.each(payrolls,(row)=>{
                    row.PaymentTypes = _.find(paymenttypes,{'EntryID':row.PaymentTypes}) 
                });
                callback(null, payrolls);
            });
        }
    ], next);
};

exports.createPayrollConfig = (P, next) => {
    console.log('P: ',P);
    if(P.FirstDate){
        P.FirstDate = moment(new Date(P.FirstDate)).format('YYYY-MM-DD')
    }
    if(P.SecondDate){
        P.SecondDate = moment(new Date(P.SecondDate)).format('YYYY-MM-DD')
    }
    var strSQL = mysql.format('INSERT INTO payroll (PayrollCode,UsedYear,FirstDate,SecondDate,PaymentTypes,UsedMonth,Term,CategoryID,SetupID) VALUES (?,?,?,?,?,?,?,?,(SELECT SetupID FROM payroll_setup WHERE Active=1 LIMIT 1));', [
        P.PayrollCode, P.UsedYear, P.FirstDate, P.SecondDate, P.PaymentType, P.UsedMonth, P.Term, P.CategoryID
    ]);
    console.log('strSQL: ',strSQL);
    db.insertWithId(strSQL, next);
};

exports.getPayrollConfig = (PayrollID, next) => {
    async.waterfall([
        (callback) => {
            var strSQL = mysql.format('SELECT P.*,tp.tp_name,MD5(P.PayrollID) AS UUID FROM payroll P LEFT JOIN paytypes tp ON P.CategoryID = tp.tp_id WHERE MD5(P.PayrollID)= ? LIMIT 1;',[PayrollID]);
            console.log('strSQL: ',strSQL);
            db.query(strSQL, (err, payrolls) => {
                if (err) {
                    next(err, null);
                }
                callback(null, payrolls);
            });
        },
        (payrolls, callback) => {
            var strSQL = mysql.format('SELECT * FROM paymenttypes');
            db.query(strSQL, (err, paymenttypes) => {
                if (err) {
                    next(err, null);
                }
                _.each(payrolls,(row)=>{
                    row.PaymentTypes = _.find(paymenttypes,{'EntryID':row.PaymentTypes}) || {}
                });
                callback(null, payrolls);
            });
        }
    ], next);
};

exports.Update_PayrollConfig = (PayrollID, P, next) => {
    if(P.FirstDate){
        P.FirstDate = moment(new Date(P.FirstDate)).format('YYYY-MM-DD')
    }
    if(P.SecondDate){
        P.SecondDate = moment(new Date(P.SecondDate)).format('YYYY-MM-DD')
    }
    var strSQL = mysql.format('UPDATE payroll SET PayrollCode=?,UsedYear=?,FirstDate=?,SecondDate=?,PaymentTypes=?,UsedMonth=?,Term=?,CategoryID=? WHERE MD5(PayrollID)=?;', [
        P.PayrollCode, P.UsedYear, P.FirstDate, P.SecondDate, P.PaymentType, P.UsedMonth, P.Term, P.CategoryID, PayrollID
    ]);
    console.log('strSQL: ',strSQL);
    db.actionQuery(strSQL, next);
};

exports.Delete_Payroll = (PayrollID, next) => {
    var strSQL = mysql.format('DELETE FROM payroll WHERE MD5(PayrollID)=?;', [PayrollID]);
    db.actionQuery(strSQL, next);
};