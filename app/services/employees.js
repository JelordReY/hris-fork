'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();

var async = require('async');
var _ = require('lodash-node');
var fs = require('fs');

exports.GetNewEmployeeID = (next) => {
    var sNewID = '';
    var strSQL = mysql.format("SELECT CASE WHEN (SELECT COUNT(*) FROM employees) = 0 THEN '4LOOP.001' " +
        "ELSE CONCAT('4LOOP.',LEFT('000',(LENGTH('000') - LENGTH(CONVERT( (CONVERT( RIGHT((SELECT MAX(e_id) FROM employees),LENGTH('000')) , SIGNED) + 1) , CHAR))))," +
        "CONVERT( (CONVERT( RIGHT((SELECT MAX(e_id) FROM employees),LENGTH('000')) , SIGNED) + 1) , CHAR)) END AS EmployeeID");
    db.query(strSQL, next);
};

exports.Add_Employee = (data, next) => {
    var E = {
        e_idno: data.e_idno,
        device_no: data.device_no || '',
        e_title: data.e_title || '',
        e_fname: data.e_fname || '',
        e_lname: data.e_lname || '',
        e_mname: data.e_mname || '',
        e_suffix: data.e_suffix || '',
        e_birthdate: data.e_birthdate || '',
        e_gender: data.e_gender || '',
        e_civil_status: data.e_civil_status,
        tp_id: data.tp_id,
        e_hourlyrate: data.e_hourlyrate || '',
        e_dailyrate: data.e_dailyrate || '',
        e_monthlyrate: data.e_monthlyrate || '',
        e_salaryamount: data.e_salaryamount || '',
        s_id: data.s_id,
        branch_id: data.branch_id,
        division_id: data.division_id,
        DeptID: data.DeptID,
        jt_id: data.jt_id,
        e_dateemployed: data.e_dateemployed,
        AppointedDate: data.AppointedDate,
        ExemptionID: data.ExemptionID,
        IsSSS: data.IsSSS,
        SSS_FIXED: data.SSS_FIXED,
        e_ssno: data.e_ssno,
        SSSEC: data.SSSEC,
        SSSEE: data.SSSEE,
        SSSER: data.SSSER,
        TAX_FIXED: data.TAX_FIXED,
        TAXRATE: data.TAXRATE,
        IsTin: data.IsTin,
        e_tinno: data.e_tinno,
        IsPh: data.IsPh,
        e_philhealthno: data.e_philhealthno,
        PH_FIXED: data.PH_FIXED,
        PHEE: data.PHEE,
        PHER: data.PHER,
        IsPagIbig: data.IsPagIbig,
        e_pagibigno: data.e_pagibigno,
        PAGIBIG_FIXED: data.PAGIBIG_FIXED,
        PAGIBIGEE: data.PAGIBIGEE,
        PAGIBIGER: data.PAGIBIGER,
        AccountNo: data.AccountNo,
        InActive: data.InActive,
        e_dateadded: new Date(),
        e_addedby: data.CURRENT_USER.UserID
    };

    var sSQL = mysql.format('INSERT INTO employees SET ?', E);
    console.log('sSQL: ', sSQL);
    db.insertWithId(sSQL, (err, resp) => {
        if (err) {
            next(err, null);
        }
        next(null, resp);
    });
};

exports.Add_Employee_Family_Background = (data, next) => {
    var FB = {
        EmployeeID: data.EmployeeID,
        MotherCompany: data.MotherCompany,
        MotherCompanyAddress: data.MotherCompanyAddress,
        MotherTelNo: data.MotherTelNo,
        MotherEmail: data.MotherEmail,
        EmergencyContactPerson: data.EmergencyContactPerson,
        EmergencyAddress: data.EmergencyAddress,
        EmergencyTelNo: data.EmergencyTelNo,
        EmergencyMobileNo: data.EmergencyMobileNo,
        FatherEmail: data.FatherEmail,
        MotherLastName: data.MotherLastName,
        MotherFirstName: data.MotherFirstName,
        MotherMiddleName: data.MotherMiddleName,
        MotherOccupation: data.MotherOccupation,
        SpouseEmployer: data.SpouseEmployer,
        SpouseBusiness: data.SpouseBusiness,
        SpouseBusTelNo: data.SpouseBusTelNo,
        FatherLastName: data.FatherLastName,
        FatherFirstName: data.FatherFirstName,
        FatherMiddleName: data.FatherMiddleName,
        FatherOccupation: data.FatherOccupation,
        FatherCompany: data.FatherCompany,
        FatherCompanyAddress: data.FatherCompanyAddress,
        FatherTelNo: data.FatherTelNo,
        SpouseTelNo: data.SpouseTelNo,
        SpouseEmail: data.SpouseEmail,
        SpouseOccupation: data.SpouseOccupation,
        SpouseCompany: data.SpouseCompany,
        SpouseLastName: data.SpouseLastName,
        SpouseFirstName: data.SpouseFirstName,
        SpouseMiddleName: data.SpouseMiddleName,
        SpouseRelationship: data.SpouseRelationship,
        SpouseAddress: data.SpouseAddress
    };

    var sSQL = mysql.format('INSERT INTO employee_family_background SET ? ', FB);
    console.log('sSQL: ', sSQL);
    db.insertWithId(sSQL, (err, resp) => {
        if (err) {
            console.log('err: ', err);
            next(err, null);
        }
        next(null, resp);
    });
};

exports.Add_Employee_Education = (e_id, ED, next) => {
    var educationObj = {
        employee_id: ED.employee_id,
        grade_school: ED.grade_school,
        grade_school_address: ED.grade_school_address,
        grade_school_inclusive_dates: ED.grade_school_inclusive_dates,
        grade_school_year_graduated: ED.grade_school_year_graduated,
        grade_school_highest_grade: ED.grade_school_highest_grade,
        grade_school_honors: ED.grade_school_honors,
        secondary_school: ED.secondary_school,
        secondary_school_address: ED.secondary_school_address,
        secondary_school_inclusive_dates: ED.secondary_school_inclusive_dates,
        secondary_school_year_graduated: ED.secondary_school_year_graduated,
        secondary_highest_grade: ED.secondary_highest_grade,
        secondary_honors: ED.secondary_honors,
        college_school: ED.college_school,
        college_school_address: ED.college_school_address,
        college_school_course: ED.college_school_course,
        college_school_inclusive_dates: ED.college_school_inclusive_dates,
        college_school_year_graduated: ED.college_school_year_graduated,
        college_highest_grade: ED.college_highest_grade,
        college_honors: ED.college_honors,
        vocational_school: ED.vocational_school,
        vocational_school_address: ED.vocational_school_address,
        vocational_school_course: ED.vocational_school_course,
        vocational_school_inclusive_dates: ED.vocational_school_inclusive_dates,
        vocational_school_year_graduated: ED.vocational_school_year_graduated,
        vocational_highest_grade: ED.vocational_highest_grade,
        vocational_honors: ED.vocational_honors,
        graduated_school: ED.graduated_school,
        graduated_school_address: ED.graduated_school_address,
        graduated_school_course: ED.graduated_school_course,
        graduated_school_inclusive_dates: ED.graduated_school_inclusive_dates,
        graduated_school_year_graduated: ED.graduated_school_year_graduated,
        graduated_highest_grade: ED.graduated_highest_grade,
        graduated_honors: ED.graduated_honors,
        doctorate_school: ED.doctorate_school,
        doctorate_school_address: ED.doctorate_school_address,
        doctorate_school_course: ED.doctorate_school_course,
        doctorate_school_inclusive_dates: ED.doctorate_school_inclusive_dates,
        doctorate_school_year_graduated: ED.doctorate_school_year_graduated,
        doctorate_highest_grade: ED.doctorate_highest_grade,
        doctorate_honors: ED.doctorate_honors
    }

    var strSQL = mysql.format('INSERT INTO employees_edu SET ? ;', [educationObj]);
    console.log('strSQL: ', strSQL);
    db.insertWithId(strSQL, (err, resp) => {
        if (err) {
            console.log('err: ', err);
            next(err, null);
        }
        next(null, resp);
    });
};

exports.Add_Employee_Children = (e_id, data, next) => {
    var strSQL = mysql.format('INSERT INTO employees_children VALUES(null,?,?,?);', [
        data.ec_name, data.ec_dateofbirth, data.e_id
    ]);
    db.insertWithId(strSQL, next);
};

exports.Delete_Employee = (e_id, next) => {
    db.query(mysql.format('DELETE FROM employees WHERE e_idno=?;', [e_id]), next);
};

exports.deleteEmployeeChildren = (e_id, _id, next) => {
    var strSQL = mysql.format('DELETE FROM employees_children WHERE MD5(e_id)=? AND MD5(ec_id)=?;', [e_id, _id]);
    db.actionQuery(strSQL, next);
};

exports.Correction_Employee = (e_id, E, next) => {
    var strSQL = mysql.format('UPDATE employees SET e_title =?,e_fname=?,e_lname=?,e_mname=?,e_suffix=?,e_birthdate=?,' +
        'e_gender=?,e_civil_status=?,tp_id=?,e_hourlyrate=?,e_dailyrate=?,e_monthlyrate=?,e_salaryamount=?,' +
        's_id=?,branch_id=?,division_id=?,DeptID=?,jt_id=?,e_dateemployed=?,AppointedDate=?,' +
        'ExemptionID=?,e_ssno=?,SSS_Fixed=?,IsSSS=?,SSSEC=?,SSSEE=?,SSSER=?,TAX_FIXED=?,TAXRATE=?,IsTin=?,e_tinno=?,' +
        'IsPh=?,e_philhealthno=?,PH_FIXED=?,PHEE=?,PHER=?,' +
        'IsPagIbig=?,e_pagibigno=?,PAGIBIG_FIXED=?,PAGIBIGEE=?,PAGIBIGER=?,AccountNo=?,' +
        'InActive=?,DateResigned=?,DateRetired=?,e_datemodified=CURDATE(),e_modifiedby=? WHERE MD5(e_idno)=?;', [
            E.e_title, E.e_fname, E.e_lname, E.e_mname, E.e_suffix, E.e_birthdate,
            E.e_gender, E.e_civil_status, E.tp_id, E.e_hourlyrate, E.e_dailyrate, E.e_monthlyrate, E.e_salaryamount,
            E.s_id, E.branch_id, E.division_id, E.DeptID, E.jt_id, E.e_dateemployed, E.AppointedDate,
            E.ExemptionID, E.e_ssno, E.SSS_FIXED, E.IsSSS, E.SSSEC, E.SSSEE, E.SSSER, E.TAX_FIXED, E.TAXRATE, E.IsTin, E.e_tinno,
            E.IsPh, E.e_philhealthno, E.PH_FIXED, E.PHEE, E.PHER,
            E.IsPagIbig, E.e_pagibigno, E.PAGIBIG_FIXED, E.PAGIBIGEE, E.PAGIBIGER, E.AccountNo,
            E.InActive, E.DateResigned, E.DateRetired, E.CURRENT_USER.UserID, e_id
        ]);
    console.log('strSQL: ', strSQL);
    db.actionQuery(strSQL, next);
}

exports.Update_Employee = (e_id, E, next) => {
    var strSQL = mysql.format('UPDATE employees SET e_bloodtype =?,e_cityadd =?,e_email =?,e_fax_number=?,e_homeadd=?,e_mobile_number=?,e_pagibigno=?,e_provincialadd=?,e_zipcode=?, ' +
        'e_cityadd=?,Perhomeadd=?,PerMunicipality=?,PerProvince=?,PerZipCode=?,nationality_id=?,PlaceOfBirth=?,e_religion=?,InActive=?,Height=?,Weight=?,device_no=?,e_datemodified=CURDATE(), ' +
        'e_modifiedby=? WHERE MD5(e_idno)=?', [
            E.e_bloodtype, E.e_cityadd, E.e_email, E.e_fax_number, E.e_homeadd, E.e_mobile_number, E.e_pagibigno, E.e_provincialadd, E.e_zipcode, E.e_cityadd, E.Perhomeadd, E.PerMunicipality,
            E.PerProvince, E.PerZipcode, E.nationality_id, E.PlaceOfBirth, E.e_religion, E.InActive, E.Height, E.Weigth, E.device_no, E.CURRENT_USER.UserID,
            e_id
        ]);

    // var strSQL = mysql.format('UPDATE employees SET e_bloodtype =?,e_cityadd =?,e_email =?,e_fax_number=?,e_homeadd=?,e_mobile_number=?,e_pagibigno=?,e_provincialadd=?,e_zipcode=?, ' +
    //     'e_cityadd=?,Perhomeadd=?,PerMunicipality=?,PerProvince=?,PerZipCode=?,nationality_id=?,PlaceOfBirth=?,e_religion=?,InActive=?,Height=?,Weight=?,device_no=?,e_datemodified=CURDATE() ' +
    //     'WHERE MD5(e_id)=?', [
    //         E.e_bloodtype, E.e_cityadd, E.e_email, E.e_fax_number, E.e_homeadd, E.e_mobile_number, E.e_pagibigno, E.e_provincialadd, E.e_zipcode, E.e_cityadd, E.Perhomeadd, E.PerMunicipality,
    //         E.PerProvince, E.PerZipcode, E.nationality_id, E.PlaceOfBirth, E.e_religion, E.InActive, E.Height, E.Weight, E.device_no,
    //         e_id
    //     ]);
    console.log('Update_Employee: ', strSQL);
    db.actionQuery(strSQL, next);
};

exports.Update_Employee_Family_Background = (e_id, FB, next) => {
    var strSQL = mysql.format('UPDATE employee_family_background SET MotherCompany=?,MotherCompanyAddress=?,MotherTelNo=?,MotherEmail=?,EmergencyContactPerson=?,EmergencyAddress=?,EmergencyTelNo=?,' +
        'EmergencyMobileNo=?,FatherEmail=?,MotherLastName=?,MotherFirstName=?,MotherMiddleName=?,MotherOccupation=?,SpouseEmployer=?,SpouseBusiness=?,SpouseBusTelNo=?,FatherLastName=?,FatherFirstName=?,' +
        'FatherMiddleName=?,FatherOccupation=?,FatherCompany=?,FatherCompanyAddress=?,FatherTelNo=?,SpouseTelNo=?,SpouseEmail=?,SpouseOccupation=?,SpouseCompany=?,SpouseLastName=?,SpouseFirstName=?,' +
        'SpouseMiddleName=?,SpouseRelationship=?,SpouseAddress=? WHERE MD5(EmployeeID)=?', [
            FB.MotherCompany, FB.MotherCompanyAddress, FB.MotherTelNo, FB.MotherEmail, FB.EmergencyContactPerson, FB.EmergencyAddress, FB.EmergencyTelNo, FB.EmergencyMobileNo, FB.FatherEmail, FB.MotherLastName,
            FB.MotherFirstName, FB.MotherMiddleName, FB.MotherOccupation, FB.SpouseEmployer, FB.SpouseBusiness, FB.SpouseBusTelNo, FB.FatherLastName, FB.FatherFirstName, FB.FatherMiddleName, FB.FatherOccupation,
            FB.FatherCompany, FB.FatherCompanyAddress, FB.FatherTelNo, FB.SpouseTelNo, FB.SpouseEmail, FB.SpouseOccupation, FB.SpouseCompany, FB.SpouseLastName, FB.SpouseFirstName, FB.SpouseMiddleName, FB.SpouseMiddleName,
            FB.SpouseAddress, e_id
        ]);
    console.log('Update_Employee_Family_Background: ', strSQL);
    db.actionQuery(strSQL, next);
};

exports.Update_Employee_Education = (EmployeeID, ED, next) => {
    var strSQL = mysql.format('UPDATE employees_edu SET grade_school=?,grade_school_address=?,grade_school_inclusive_dates=?,grade_school_year_graduated=?,' +
        'secondary_school=?,secondary_school_address=?,secondary_school_inclusive_dates=?,secondary_school_year_graduated=?,' +
        'vocational_school=?,vocational_school_address=?,vocational_school_course=?,vocational_school_inclusive_dates=?,vocational_school_year_graduated=?,' +
        'graduated_school=?,graduated_school_address=?,graduated_school_course=?,graduated_school_inclusive_dates=?,graduated_school_year_graduated=?,' +
        'doctorate_school=?,doctorate_school_address=?,doctorate_school_course=?,doctorate_school_inclusive_dates=?,doctorate_school_year_graduated=?,' +
        'grade_school_highest_grade=?,grade_school_honors=?,secondary_highest_grade=?,secondary_honors=?,college_highest_grade=?,college_honors=?,' +
        'vocational_highest_grade=?,vocational_honors=?,graduated_highest_grade=?,graduated_honors=?,doctorate_highest_grade=?,doctorate_honors=?,' +
        'college_school=?,college_school_address=?,college_school_course=?,college_school_inclusive_dates=?,college_school_year_graduated=? ' +
        'WHERE MD5(employee_id)=?;', [
            ED.grade_school, ED.grade_school_address, ED.grade_school_inclusive_dates, ED.grade_school_year_graduated,
            ED.secondary_school, ED.secondary_school_address, ED.secondary_school_inclusive_dates, ED.secondary_school_year_graduated,
            ED.vocational_school, ED.vocational_school_address, ED.vocational_school_course, ED.vocational_school_inclusive_dates, ED.vocational_school_year_graduated,
            ED.graduated_school, ED.graduated_school_address, ED.graduated_school_course, ED.graduated_school_inclusive_dates, ED.graduated_school_year_graduated,
            ED.doctorate_school, ED.doctorate_school_address, ED.doctorate_school_course, ED.doctorate_school_inclusive_dates, ED.doctorate_school_year_graduated,
            ED.grade_school_highest_grade, ED.grade_school_honors, ED.secondary_highest_grade, ED.secondary_honors, ED.college_highest_grade, ED.college_honors,
            ED.vocational_highest_grade, ED.vocational_honors, ED.graduated_highest_grade, ED.graduated_honors, ED.doctorate_highest_grade, ED.doctorate_honors,
            ED.college_school, ED.college_school_address, ED.college_school_course, ED.college_school_inclusive_dates, ED.college_school_year_graduated,
            EmployeeID
        ]);
    console.log('Update_Employee_Education: ', strSQL);
    db.actionQuery(strSQL, next);
};

exports.Update_Employee_ID = (e_id, data, next) => {
    data.e_idno = data.new_e_id || data.e_idno;
    var strSQL = mysql.format('UPDATE employees SET e_idno=? WHERE MD5(e_id)=?;', [data.e_idno, e_id]);
    console.log('strSQL: ', strSQL);
    db.actionQuery(strSQL, next);
};

exports.IsEmployeeActive = (e_id, next) => {
    var strSQL = mysql.format('SELECT * FROM employees WHERE MD5(e_id)=? AND Inactive <> 0 LIMIT 1;', [e_id]);
    db.query(strSQL, next);
};

exports.IsEmployeeResigned = (e_id, next) => {
    var strSQL = mysql.format('SELECT * FROM employees WHERE MD5(e_id)=? AND (DateResigned IS NOT NULL AND DateResigned <> "0001-01-01") LIMIT 1;', [e_id]);
    db.query(strSQL, next);
};

exports.IsEmployeeRetired = (e_id, next) => {
    var strSQL = mysql.format('SELECT * FROM employees WHERE MD5(e_id)=? AND (DateRetired IS NOT NULL AND DateRetired <> "0001-01-01") LIMIT 1;', [e_id]);
    db.query(strSQL, next);
};

exports.GetEmployeeProfile = (e_id, next) => {
    var strSQL = mysql.format('SELECT E.*,fn_EmployeeName(E.e_idno) AS employee_name,' +
        'fn_EmployeePositionTitle(E.e_idno) as job_position,fn_EmployeeDepartmentName(E.e_idno) as department,fn_EmploymentStatus(E.s_id) as employment_status, ' +
        'fn_DivisionName(E.division_id) as division,fn_BranchName(E.branch_id) as branch,(SELECT tp_name FROM paytypes WHERE tp_id = E.tp_id LIMIT 1) as payroll_type, ' +
        'MD5(E.e_id) as UUID,MD5(E.e_idno) as Emp_UUID ' +
        'FROM employees E WHERE (MD5(E.e_id)=? OR MD5(E.e_idno)=?)LIMIT 1;', [e_id, e_id]);

    /*var strSQL = mysql.format('SELECT E.`e_idno`,E.`e_id`,E.`branch_id`,E.`device_no`,E.`e_ssno`,E.`e_gsisno`,E.`e_pagibigno`,E.`e_philhealthno`,E.`e_tinno`,E.`e_title`, ' + 
        'E.`e_fname`,E.`e_lname`,E.`e_mname`,E.`e_suffix`,E.`e_birthdate`,E.`e_gender`,E.`e_civil_status`,E.`e_cityadd`,E.`e_homeadd`,E.`e_provincialadd`,E.`e_zipcode`,E.`e_email`,' +
        'E.`e_phone_number`,E.`e_mobile_number`,E.`e_incentive`,E.`e_salaryamount`,E.`tp_id`,E.`e_bloodtype`,E.`e_religion`,E.`e_dateemployed`,E.`s_id`,E.`jt_id`,E.`e_addedby`,' + 
        'E.`e_dateadded`,E.`e_modifiedby`,E.`e_datemodified`,E.`DeptID`,E.`division_id`,E.`StepID`,E.`TemplateID`,E.`ExemptionID`,E.`PlaceOfBirth`,E.`Perhomeadd`,E.`PerMunicipality`,' + 
        'E.`PerProvince`,E.`PerZipcode`,E.`PerTelNo`,E.`PersonalWeb`,E.`IsSSS`,E.`IsPagIbig`,E.`IsPh`,E.`IsTin`,E.`AppointedDate`,E.`DateResigned`,E.`DateRetired`,E.`ConfidentialityID`,' +
        'E.`IsGSIS`,E.`BaccalaurateRate`,E.`MasteralRate`,E.`DoctoralRate`,E.`TAXRATE`,E.`GSISEE`,E.`GSISER`,E.`GSISEC`,E.`SSSEE`,E.`SSSER`,E.`SSSEC`,E.`PHEE`,E.`PHER`,E.`PAGIBIGEE`,' + 
        'E.`PAGIBIGER`,E.`SSS_FIXED`,E.`GSIS_FIXED`,E.`PH_FIXED`,E.`PAGIBIG_FIXED`,E.`TAX_FIXED`,E.`GSISPolicyNo`,E.`e_hourlyrate`,E.`e_dailyrate`,E.`e_monthlyrate`,E.`DateLastPromotion`,' +
        'E.`DateEnteredGovService`,E.`PRCID`,E.`InActive`,E.`e_fax_number`,E.`nationality_id`,E.`ScheduleID`,E.`Height`,E.`Weight`,E.`AccountNo`,fn_EmployeeName(E.e_idno) AS employee_name,' +
        'fn_EmployeePositionTitle(E.e_idno) as job_position,fn_EmployeeDepartmentName(E.e_idno) as department,fn_EmploymentStatus(E.s_id) as employment_status, ' +
        'fn_DivisionName(E.division_id) as division,fn_BranchName(E.branch_id) as branch,(SELECT tp_name FROM paytypes WHERE tp_id = E.tp_id LIMIT 1) as payroll_type, ' +
        'MD5(E.e_id) as UUID,MD5(E.e_idno) as Emp_UUID ' +
        'FROM employees E WHERE (MD5(E.e_id)=? OR MD5(E.e_idno)=?)LIMIT 1;', [e_id, e_id]);*/
    console.log('strSQL: ',strSQL);
    db.query(strSQL, next);
};

exports.GetEmployeeFamilyBackground = (e_id, next) => {
    var strSQL = mysql.format('SELECT * FROM employee_family_background WHERE MD5(EmployeeID)=? LIMIT 1;', [e_id]);
    db.query(strSQL, next);
};

exports.GetEmployeeEducation = (e_id, next) => {
    var strSQL = mysql.format('SELECT * FROM employees_edu WHERE md5(employee_id)=? LIMIT 1;', [e_id]);
    db.query(strSQL, next);
};

exports.GetEmployeeChildren = (e_id, next) => {
    var strSQL = mysql.format('SELECT ec_name,DATE_FORMAT(ec_dateofbirth,"%m/%d/%Y") AS ec_dateofbirth, MD5(ec_id) AS UUID FROM employees_children WHERE MD5(e_id)=?;', [e_id]);
    db.query(strSQL, next);
};

exports.employeeMasterlist = (params, next) => {
    var str = 'SELECT E.e_idno,E.device_no, E.e_lname,E.e_fname,E.e_mname,E.e_suffix,E.e_gender," " As Age,C.cs_name,B.branch_name,`Div`.div_name,D.d_name,J.jt_name, ES.Status,' +
        'fn_TaxCode(E.ExemptionID) AS TaxCode,E.e_monthlyrate, DATE_FORMAT(E.e_dateemployed,"%m-%d-%Y") AS date_employed,E.AccountNo,IF(E.InActive = 1,"YES","NO") AS InActive,' +
        'IF(E.DateResigned IS NULL || E.DateResigned = "0001-01-01","NO","YES") AS IsResigned,IF(E.DateRetired IS NULL || E.DateRetired = "0001-01-01","NO","YES") AS IsRetired,E.Photo,' +
        'MD5(E.e_id) AS UUID,MD5(E.e_idno) as Emp_UUID  ' +
        'FROM employees E LEFT JOIN job_titles J ON E.jt_id = J.jt_id LEFT JOIN departments D ON E.DeptID = D.d_id ' +
        'LEFT JOIN employment_status ES ON E.s_id = ES.StatusID LEFT JOIN civil_status C ON E.e_civil_status = C.cs_id ' +
        'LEFT JOIN branch B ON E.branch_id = B.branch_id LEFT JOIN divisions `Div` ON E.division_id = `Div`.div_id ';

    /*var str = 'SELECT E.e_idno,E.device_no, E.e_lname,E.e_fname,E.e_mname,E.e_suffix,E.e_gender," " As Age,C.cs_name,B.branch_name,`Div`.div_name,D.d_name,J.jt_name, ES.Status,' +
        'fn_TaxCode(E.ExemptionID) AS TaxCode,E.e_monthlyrate, DATE_FORMAT(E.e_dateemployed,"%m-%d-%Y") AS date_employed,E.AccountNo,IF(E.InActive = 1,"YES","NO") AS InActive,' +
        'IF(E.DateResigned IS NULL || E.DateResigned = "0001-01-01","NO","YES") AS IsResigned,IF(E.DateRetired IS NULL || E.DateRetired = "0001-01-01","NO","YES") AS IsRetired,' +
        'MD5(E.e_id) AS UUID,MD5(E.e_idno) as Emp_UUID  ' +
        'FROM employees E LEFT JOIN job_titles J ON E.jt_id = J.jt_id LEFT JOIN departments D ON E.DeptID = D.d_id ' +
        'LEFT JOIN employment_status ES ON E.s_id = ES.StatusID LEFT JOIN civil_status C ON E.e_civil_status = C.cs_id ' +
        'LEFT JOIN branch B ON E.branch_id = B.branch_id LEFT JOIN divisions `Div` ON E.division_id = `Div`.div_id ';*/

    if (!_.isEmpty(params)) {
        if (params.action == 'branch') {
            str = str + ' WHERE E.branch_id=? ORDER BY E.e_idno;';
            var strSQL = mysql.format(str, [params._id]);
            console.log('str: ',strSQL);
            db.query(strSQL, next);
        } else if (params.action == 'department') {
            str = str + ' WHERE E.DeptID=? ORDER BY E.e_idno;';
            var strSQL = mysql.format(str, [params._id]);
            console.log('str: ',strSQL);
            db.query(strSQL, next);
        } else {
            str = str + 'ORDER BY E.e_idno;'
            console.log('str: ',str);
            db.query(mysql.format(str), next);
        }
    } else {
        str = str + 'ORDER BY E.e_idno;'
        console.log('str: ',str);
        db.query(mysql.format(str), next);
    }
};

exports.employeePickerData = (limit, next) => {
    var str = 'SELECT E.e_id,E.e_idno, fn_EmployeeName(E.e_idno) AS employee_name,E.e_fname,E.e_lname,E.e_mname,E.e_suffix,E.e_gender, fn_EmployeePositionTitle(E.e_idno) as job_position,fn_EmployeeDepartmentName(E.e_idno) as department,' +
        'fn_EmploymentStatus(E.s_id) as employment_status,E.Inactive,E.DateResigned,E.DateRetired,MD5(E.e_id) AS UUID,MD5(E.e_idno) as Emp_UUID FROM employees E ORDER BY E.e_id ';
    if (limit) {
        str = str + 'LIMIT ' + limit + ';';
    }
    var strSQL = mysql.format(str);
    console.log('strSQL: ', strSQL);
    db.query(strSQL, next);
};

exports.TagEmployeeWorkSchedule = (emp_id, sched_id, next) => {
    var strSQL = mysql.format('UPDATE employees SET ScheduleID=? WHERE MD5(e_idno)=?', [sched_id, emp_id]);
    db.actionQuery(strSQL, next);
};

exports.UnTagEmployeeWorkSchedule = (emp_id, sched_id, next) => {
    var strSQL = mysql.format('UPDATE employees SET ScheduleID=null WHERE MD5(e_idno)=?;', [emp_id]);
    db.actionQuery(strSQL, next);
};

exports.getEmployeeID = (emp_id, next) => {
    var strSQL = mysql.format('SELECT e_idno FROM employees WHERE e_idno=? LIMIT 1;', [emp_id]);
    db.query(strSQL, next);
};

exports.Remove_Employee_Photo = (emp_id, next) => {
    var strSQL = mysql.format('UPDATE employees SET photo=null WHERE MD5(e_id)=?;', [emp_id]);
    db.actionQuery(strSQL, next);
};

exports.Add_Employee_Photo = (emp_id, data, next) => {
    var strSQL = mysql.format('UPDATE employees SET Photo=? WHERE MD5(e_id)=?;', [fs.readFileSync(data.path), emp_id]);
    db.actionQuery(strSQL, next);
};

exports.getEmployeeFingerPrint = (emp_id,next)=>{
    var strSQL = mysql.format('SELECT EmployeeID,RThumb_Pic1,RThumb_Pic2,RThumb_Pic3,RThumb_Pic4,LThumb_Pic1,LThumb_Pic2,LThumb_Pic3,LThumb_Pic4,FingerBck1_pic1,FingerBck1_pic2,FingerBck1_pic3,FingerBck1_pic4,FingerBck2_pic1,FingerBck2_pic2,FingerBck2_pic3,FingerBck2_pic4 FROM employee_fingerprint WHERE MD5(EmployeeID)=?;',[emp_id]);
    db.query(strSQL,next);
};