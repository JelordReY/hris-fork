'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();

var async = require('async');
var _ = require('lodash-node');

exports.getAllDays = (next) => {
    db.query(mysql.format('SELECT *, MD5(IndexID) AS UUID FROM days;'), next);
};

exports.getAllWorkSchedules = (next) => {
    db.query(mysql.format("SELECT ws.SchedID,ws.Code,ws.Description,CONCAT(Code,' - [', Template,']') As CodeTemplate,ws.Template,ws.InActive,MD5(ws.SchedID) AS UUID FROM work_schedules ws;"), next);
};

exports.Add_Work_Schedule = (WS, next) => {
    var strSQL = mysql.format('INSERT INTO work_schedules(Code,Description,Template,InActive,CreatedBy,DateCreated) VALUES (?,?,?,?,?,CURDATE())', [
        WS.Code, WS.Description, WS.Template, WS.InActive || 0, WS.CURRENT_USER.UserID
    ]);
    db.insertWithId(strSQL, next);
};

exports.Delete_Work_Schedule = (sched_id, next) => {
    var strSQL = mysql.format('DELETE FROM work_schedules WHERE MD5(SchedID)=?; ' +
        'DELETE FROM time_schedule WHERE MD5(SchedID)=?; ' +
        'DELETE FROM work_schedules_detail WHERE MD5(SchedID)=?;', [sched_id, sched_id, sched_id]);
    console.log('strSQL: ', strSQL);
    db.actionQuery(strSQL, next);
};

exports.Update_Work_Schedule = (sched_id, W, next) => {
    var strSQL = mysql.format('UPDATE work_schedules SET Code=?,Description=?,Template=?,InActive=?,DateModified=CURDATE(),ModifiedBy=? WHERE MD5(SchedID)=?;', [
        W.Code, W.Description, W.Template, W.InActive || 0, W.CURRENT_USER.UserID, sched_id
    ]);
    console.log('strSQL: ', strSQL);
    db.actionQuery(strSQL, next);
};

exports.getWorkScheduleDetails = (sched_id, next) => {
    async.waterfall([
        (callback) => {
            var strSQL = mysql.format("SELECT ws.SchedID,ws.Code,ws.Description,CONCAT(Code,' - [', Template,']') As CodeTemplate,ws.Template,ws.InActive,MD5(ws.SchedID) AS UUID " +
                "FROM work_schedules ws WHERE MD5(SchedID)=?;", [sched_id]);
            db.query(strSQL, (err, response) => {
                if (err) {
                    next(err, null);
                }
                if (response && response.length > 0) {
                    response = response[0];
                }
                callback(null, response);
            });
        },
        (sched, callback) => {
            var strSQL = mysql.format("SELECT IN01, OUT01, IN02, OUT02, IN03, OUT03, IN11, OUT11, IN12, OUT12, IN13, OUT13, IN21, OUT21, IN22, OUT22, IN23, OUT23, IN31, OUT31, IN32, OUT32, IN33, OUT33, IN41, OUT41, IN42, OUT42, IN43, OUT43, IN51, OUT51, IN52, OUT52, IN53, OUT53, IN61, OUT61, IN62, OUT62, IN63, OUT63 " +
                "FROM time_schedule WHERE MD5(SchedID)=?;", [sched_id]);
            db.query(strSQL, (err, response) => {
                if (err) {
                    next(err, null);
                }
                if (response && response.length > 0) {
                    response = response[0];
                }
                sched.workschedules = response;
                callback(null, sched);
            });
        }
    ], next);
};

exports.getWorkSchedulesTemplates = (sched_id, next) => {
    var strSQL = mysql.format("SELECT IN01, OUT01, IN02, OUT02, IN03, OUT03, IN11, OUT11, IN12, OUT12, IN13, OUT13, IN21, OUT21, IN22, OUT22, IN23, OUT23, IN31, OUT31, IN32, OUT32, IN33, OUT33, IN41, OUT41, IN42, OUT42, IN43, OUT43, IN51, OUT51, IN52, OUT52, IN53, OUT53, IN61, OUT61, IN62, OUT62, IN63, OUT63 " +
        "FROM time_schedule WHERE MD5(SchedID)=?;", [sched_id]);
    db.query(strSQL, next);
};

exports.saveWorkScheduleTemplates = (sched_id, data, next) => {
    var template = {
        SchedID: data.SchedID,
        IN01: data.IN01,
        OUT01: data.OUT01,
        IN02: data.IN02,
        OUT02: data.OUT02,
        IN03: data.IN03,
        OUT03: data.OUT03,
        IN11: data.IN11,
        OUT11: data.OUT11,
        IN12: data.IN12,
        OUT12: data.OUT12,
        IN13: data.IN13,
        OUT13: data.OUT13,
        IN21: data.IN21,
        OUT21: data.OUT21,
        IN22: data.IN22,
        OUT22: data.OUT22,
        IN23: data.IN23,
        OUT23: data.OUT23,
        IN31: data.IN31,
        OUT31: data.OUT31,
        IN32: data.IN32,
        OUT32: data.OUT32,
        IN33: data.IN33,
        OUT33: data.OUT33,
        IN41: data.IN41,
        OUT41: data.OUT41,
        IN42: data.IN42,
        OUT42: data.OUT42,
        IN43: data.IN43,
        OUT43: data.OUT43,
        IN51: data.IN51,
        OUT51: data.OUT51,
        IN52: data.IN52,
        OUT52: data.OUT52,
        IN53: data.IN53,
        OUT53: data.OUT53,
        IN61: data.IN61,
        OUT61: data.OUT61,
        IN62: data.IN62,
        OUT62: data.OUT62,
        IN63: data.IN63,
        OUT63: data.OUT63
    };

    var strSQL = mysql.format('INSERT INTO time_schedule SET ?', [template]);
    console.log('strSQL: ',strSQL);
    db.insertWithId(strSQL, next);
};

exports.updateWorkScheduleTemplates = (sched_id, data, next) => {
    var template = {
        IN01: data.IN01,
        OUT01: data.OUT01,
        IN02: data.IN02,
        OUT02: data.OUT02,
        IN03: data.IN03,
        OUT03: data.OUT03,
        IN11: data.IN11,
        OUT11: data.OUT11,
        IN12: data.IN12,
        OUT12: data.OUT12,
        IN13: data.IN13,
        OUT13: data.OUT13,
        IN21: data.IN21,
        OUT21: data.OUT21,
        IN22: data.IN22,
        OUT22: data.OUT22,
        IN23: data.IN23,
        OUT23: data.OUT23,
        IN31: data.IN31,
        OUT31: data.OUT31,
        IN32: data.IN32,
        OUT32: data.OUT32,
        IN33: data.IN33,
        OUT33: data.OUT33,
        IN41: data.IN41,
        OUT41: data.OUT41,
        IN42: data.IN42,
        OUT42: data.OUT42,
        IN43: data.IN43,
        OUT43: data.OUT43,
        IN51: data.IN51,
        OUT51: data.OUT51,
        IN52: data.IN52,
        OUT52: data.OUT52,
        IN53: data.IN53,
        OUT53: data.OUT53,
        IN61: data.IN61,
        OUT61: data.OUT61,
        IN62: data.IN62,
        OUT62: data.OUT62,
        IN63: data.IN63,
        OUT63: data.OUT63
    };

    var strSQL = mysql.format('UPDATE time_schedule SET ? WHERE MD5(SchedID)=?', [template, sched_id]);
    console.log('strSQL: ',strSQL);
    db.actionQuery(strSQL, next);
};