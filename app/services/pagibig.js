'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();

var async = require('async');
var _ = require('lodash-node');


exports.getAllPagIBIG = (next) => {
    var strSQL = mysql.format('SELECT MonthlyCompensation, BracketFrom, BracketTo, (EmployeeShare*100) AS EmployeeShare, (EmployerShare*100) AS EmployerShare, (Total*100) AS Total, IndexID,MD5(IndexID) AS UUID FROM pay_pagibig');
    db.query(strSQL, next);
};

exports.Add_HDMF = (hdmf, next) => {
    var strSQL = mysql.format('INSERT INTO pay_pagibig (MonthlyCompensation, BracketFrom, BracketTo, EmployerShare, EmployeeShare, Total) VALUES (?,?,?,?,?,?)', [
        hdmf.MonthlyCompensation, hdmf.BracketFrom, hdmf.BracketTo, hdmf.EmployerShare, hdmf.EmployeeShare, hdmf.Total
    ]);
    db.insertWithId(strSQL, next);
};

exports.Update_HDMF = (IndexID, hdmf, next) => {
    var strSQL = mysql.format("UPDATE pay_pagibig SET MonthlyCompensation = ?, BracketFrom = ?, BracketTo = ?, EmployerShare = ?, EmployeeShare = ?, Total = ? WHERE MD5(IndexID) =?;", [
        hdmf.MonthlyCompensation, hdmf.BracketFrom, hdmf.BracketTo, hdmf.EmployerShare, hdmf.EmployeeShare, hdmf.Total, IndexID
    ]);
    db.actionQuery(strSQL,next);
};

exports.GetHDMF_ById = (hdmf_id,next)=>{
    console.log('v:',hdmf_id)
    var strSQL = mysql.format('SELECT P.*,MD5(P.IndexID) AS UUID FROM pay_pagibig P WHERE MD5(IndexID) =? LIMIT 1;',[hdmf_id]);
    console.log('strSQLv:',strSQL)
    db.query(strSQL, next);
};

exports.DeleteHDMF = (hdmf_id,next)=>{
    var strSQL = mysql.format('DELETE FROM pay_pagibig WHERE MD5(IndexID)=?;',[hdmf_id]);
    db.actionQuery(strSQL,next);
}