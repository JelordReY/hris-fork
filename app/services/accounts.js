'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();

var async = require('async');
var _ = require('lodash-node');

exports.getAllAccountClass = (next) => {
    db.query(mysql.format('SELECT *, MD5(ClassID) AS UUID FROM accountsclass;'), next);
};

exports.getAccountClass = (ClassID, next) => {
    db.query(mysql.format('SELECT *, MD5(ClassID) AS UUID FROM accountsclass WHERE MD5(ClassID)=? LIMIT 1;', [ClassID]), next);
};

exports.Add_Account_Class = (AC, next) => {
    var strSQL = mysql.format('INSERT INTO accountsclass(ClassCode,ClassName,ClassShort) VALUES(?,?,?);', [
        AC.ClassCode, AC.ClassName, AC.ClassShort
    ]);
    db.insertWithId(strSQL, next);
};

exports.Update_Account_Class = (ClassID, AC, next) => {
    var strSQL = mysql.format('UPDATE accountsclass SET ClassCode=?,ClassName=?,ClassShort=? WHERE MD5(ClassID)=?;', [
        AC.ClassCode, AC.ClassName, AC.ClassShort, ClassID
    ]);
    db.actionQuery(strSQL, next);
};

exports.Delete_Account_Class = (ClassID, next) => {
    var strSQL = mysql.format('DELETE FROM accountsclass WHERE MD5(ClassID)=?;', [ClassID]);
    db.actionQuery(strSQL, next);
};


exports.getAllAccountGroups = (next) => {
    var strSQL = mysql.format('SELECT *,MD5(GroupID) AS UUID FROM accountgroups;');
    db.query(strSQL, next);
};

exports.getAccountGroup = (GroupID, next) => {
    var strSQL = mysql.format('SELECT *,MD5(GroupID) AS UUID FROM accountgroups WHERE MD5(GroupID)=? LIMIT 1;', [GroupID]);
    db.query(strSQL, next);
};

exports.Add_Account_Groups = (AG, next) => {
    var strSQL = mysql.format('INSERT INTO accountgroups(GroupCode,GroupName,GroupShort) VALUES (?,?,?);', [
        AG.GroupCode, AG.GroupName, AG.GroupShort
    ]);
    db.insertWithId(strSQL, next);
};

exports.Update_Account_Groups = (GroupID, AG, next) => {
    var strSQL = mysql.format('UPDATE accountgroups SET GroupCode=?,GroupName=?,GroupShort=? WHERE MD5(GroupID)=?;', [
        AG.GroupCode, AG.GroupName, AG.GroupShort, GroupID
    ]);
    db.actionQuery(strSQL, next);
};

exports.Delete_Account_Groups = (GroupID, next) => {
    var strSQL = mysql.format('DELETE FROM accountgroups WHERE MD5(GroupID)=?;', [GroupID]);
    db.actionQuery(strSQL, next);
};



exports.getAllAccountTypes = (next) => {
    var strSQL = mysql.format('SELECT *,MD5(TypeID) AS UUID FROM accountstype;')
    db.query(strSQL, next);
};

exports.getAccountType = (TypeID, next) => {
    var strSQL = mysql.format('SELECT *,MD5(TypeID) AS UUID FROM accountstype WHERE MD5(TypeID)=? LIMIT 1;', [TypeID])
    db.query(strSQL, next);
};


exports.Add_Account_Types = (AT, next) => {
    var strSQL = mysql.format('INSERT INTO accountstype(CategoryCode,CategoryName,CategoryShort) VALUES (?,?,?);', [
        AT.CategoryCode, AT.CategoryName, AT.CategoryShort
    ]);
    db.insertWithId(strSQL, next);
};

exports.Update_Account_Types = (TypeID, AT, next) => {
    var strSQL = mysql.format('UPDATE accountstype SET CategoryCode=?,CategoryName=?,CategoryShort=? WHERE MD5(TypeID)=?;', [
        AT.CategoryCode, AT.CategoryName, AT.CategoryShort, TypeID
    ]);
    db.actionQuery(strSQL, next);
};

    exports.Delete_Account_Types = (TypeID, next) => {
    var strSQL = mysql.format('DELETE FROM accountstype WHERE MD5(TypeID)=?;', [TypeID]);
    db.actionQuery(strSQL, next);
};



    
exports.getAllAccounts = (next) => {
    async.waterfall([
        (callback) => {
            var strSQL = mysql.format('SELECT A.*,MD5(A.AcctID) AS UUID FROM accounts A;')
            db.query(strSQL, (err, accounts) => {
                if (err) {
                    next(err, null);
                }
                callback(null, accounts);
            });
        },
        (accounts, callback) => {
            var strSQL = mysql.format('SELECT * FROM accountgroups AG;');
            db.query(strSQL, (err, accountgroups) => {
                if (err) {
                    next(err, null);
                }
                _.each(accounts, (row) => {
                    
                    row.groups = _.find(accountgroups, {
                        'GroupID': row.GroupID
                    }) || {};
                });
                callback(null, accounts);
            });
        },
        (accounts, callback) => {
            var strSQL = mysql.format('SELECT * FROM accountsclass AG;');
            db.query(strSQL, (err, accountsclass) => {
                if (err) {
                    next(err, null);
                }
                _.each(accounts, (row) => {
                    row.class = _.find(accountsclass, {
                        'ClassID': row.ClassID
                    }) || {};
                });
                callback(null, accounts);
            });
        },
        (accounts, callback) => {
            var strSQL = mysql.format('SELECT * FROM accountstype AG;');
            db.query(strSQL, (err, accountstype) => {
                if (err) {
                    next(err, null);
                }
                _.each(accounts, (row) => {
                    row.types = _.find(accountstype, {
                        'TypeID': row.TypeID
                    }) || {};
                });
                // console.log('accounts ->',accounts);
                callback(null, accounts);
            });
        }
    ], next);
};

exports.GetAccountByID = (AcctID, next) => {
    async.waterfall([
        (callback) => {
            var strSQL = mysql.format('SELECT A.*,MD5(A.AcctID) AS UUID FROM accounts A WHERE MD5(A.AcctID)=? LIMIT 1;', [AcctID])
            db.query(strSQL, (err, accounts) => {
                if (err) {
                    next(err, null);
                }
                callback(null, accounts);
            });
        },
        (accounts, callback) => {
            var strSQL = mysql.format('SELECT * FROM accountgroups AG;');
            db.query(strSQL, (err, accountgroups) => {
                if (err) {
                    next(err, null);
                }
                _.each(accounts, (row) => {
                    row.groups = _.find(accountgroups, {
                        'GroupID': row.GroupID
                    }) || {};
                });
                callback(null, accounts);
            });
        },
        (accounts, callback) => {
            var strSQL = mysql.format('SELECT * FROM accountsclass AG;');
            db.query(strSQL, (err, accountsclass) => {
                if (err) {
                    next(err, null);
                }
                _.each(accounts, (row) => {
                    row.class = _.find(accountsclass, {
                        'ClassID': row.ClassID
                    }) || {};
                });
                callback(null, accounts);
            });
        },
        (accounts, callback) => {
            var strSQL = mysql.format('SELECT * FROM accountstype AG;');
            db.query(strSQL, (err, accountstype) => {
                if (err) {
                    next(err, null);
                }
                _.each(accounts, (row) => {
                    row.types = _.find(accountstype, {
                        'TypeID': row.TypeID
                    }) || {};
                });
                callback(null, accounts);
            });
        }
    ], next);
};

exports.Add_Accounts = (Acct, next) => {
    var strSQL = mysql.format('INSERT INTO accounts (AcctCode,AcctName,ShortName,TypeID,GroupID,ClassID,Inactive,Locked,TaxInclude,DeducNotInclude,CreatedBy,DateCreated) VALUES (?,?,?,?,?,?,?,?,?,?,?,CURDATE());', [
        Acct.AcctCode, Acct.AcctName, Acct.ShortName, Acct.TypeID, Acct.GroupID, Acct.ClassID, Acct.Inactive , Acct.Locked || 0, Acct.TaxInclude, Acct.DeducNotInclude, Acct.CURRENT_USER.UserID
 ]);
    db.insertWithId(strSQL, next);
};

exports.Update_Accounts = (AcctID, Acct, next) => {
    var strSQL = mysql.format('UPDATE accounts SET AcctCode=?,AcctName=?,ShortName=?,TypeID=?,GroupID=?,ClassID=?,Inactive=?,Locked=?,TaxInclude=?,DeducNotInclude=?,ModifiedBy=?,DateModified=CURDATE() WHERE MD5(AcctID)=?;', [
        Acct.AcctCode, Acct.AcctName, Acct.ShortName, Acct.TypeID, Acct.GroupID, Acct.ClassID, Acct.Inactive || 0, Acct.Locked || 0, Acct.TaxInclude, Acct.DeducNotInclude, Acct.CURRENT_USER.UserID
 , AcctID
    ]);
    db.actionQuery(strSQL, next);
};

exports.Delete_Accounts = (AcctID, next) => {
    var strSQL = mysql.format('DELETE FROM accounts WHERE MD5(AcctID)=?;', [AcctID]);
    db.actionQuery(strSQL, next);
};