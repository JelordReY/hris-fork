'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();

var async = require('async');
var _ = require('lodash-node');

exports.Add_Employstat = (data, next) => {
     var strSQL = mysql.format('INSERT INTO employment_status(StatusCode ,Status, Description,  Remarks ,   InActive ,  DateCreated,  CreatedBy) VALUES (?,?,?,?,?, NOW(),?);', [
        data.StatusCode, data.Status, data.Description, data.Remarks || '', data.InActive,  data.CURRENT_USER.UserID
 ]);
    db.actionQuery(strSQL, next);
};

exports.getAllEmploymentStatus = (next) => {
    db.query(mysql.format('SELECT b.*,MD5(b.StatusID) as UUID FROM employment_status as b '), next);
};


exports.getEmploymentStatusById = (empstat_id, next) => {
    var strSQL = mysql.format('SELECT * FROM employment_status WHERE  MD5(StatusID)=? LIMIT 1;', [empstat_id]);
    db.query(strSQL, next);
};


exports.deleteEmploymentStatus = (empstat_id, next) => {
    var strSQL = mysql.format('DELETE FROM employment_status WHERE md5(StatusID) =?;', [empstat_id]);
    db.actionQuery(strSQL, next);
};

exports.updateEmploymentStatus = (empstat_id, data, next) => {
    var strSQL = mysql.format('UPDATE employment_status SET StatusCode=? , Status=? , Description=? ,Remarks = ? , InActive =? ,  LastDateModified = CURDATE() , LastModifiedBy =? WHERE MD5(StatusID)=?;', [
        data.StatusCode , data.Status, data.Description , data.Remarks || '' , data.InActive ,  data.CURRENT_USER.UserID,  empstat_id
    ]);
    db.actionQuery(strSQL, next);
};