'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();

var env = process.env.NODE_ENV || 'development';
var config = require('../../config/environment/' + env);

var async = require('async');
var Cryptr = require('cryptr');


exports.IsUserExisted = function IsUserExisted(sUserName, next) {
    var str = mysql.format('SELECT UserName FROM users WHERE UserName=? LIMIT 1', [sUserName]);
    db.query(str, next);
};

exports.IsUserExisted2 = function IsUserExisted(UserID, sUserName, next) {
    var str = mysql.format('SELECT UserName FROM users WHERE UserName=? AND MD5(UserID) <> ? LIMIT 1', [UserID, sUserName]);
    db.query(str, next);
};

exports.getUserByUsernameAndEmail = (username, next) => {
    var str = mysql.format('SELECT U.UserID,UG.GroupID,U.UserName,U.Password,U.FullName,U.Inactive,U.EmployeeID,U.IsEmployee,U.Email,U.MobileNo,U.AccessLimit,MD5(E.e_id) as E_UUID, MD5(E.e_idno) as Emp_UUID,MD5(U.UserID) AS UUID ' +
        'FROM users U INNER JOIN employees E ON E.e_idno = U.EmployeeID LEFT JOIN users_group UG ON U.UserGroupID = UG.GroupID WHERE U.UserName=? LIMIT 1;', [username]);
    console.log('str: ', str);
    db.query(str, next);
};

exports.Add_User = function Add_User(data, next) {
    async.waterfall([
        function(callback) {
            var cryptr = new Cryptr(config.app_secretkey);
            if (data.Password) {
                var encryptedPassword = cryptr.encrypt(data.Password);
                data.Password = encryptedPassword;
            }
            var obj = {
                UserName: data.UserName,
                Password: data.Password,
                DateCreated: new Date(),
                CreatedBy: data.UserID,
                FullName: data.FullName,
                Email: data.Email,
                MobileNo: data.MobileNo,
                IsEmployee: data.IsEmployee,
                EmployeeID: data.EmployeeID
            };
            var str = mysql.format('INSERT INTO users SET ?', obj);
            db.insertWithId(str, function(err, resp) {
                if (err) {
                    callback(err, null);
                }
                callback(null, resp);
            });
        },
        function(UserID, callback) {
            var query = mysql.format('INSERT INTO user_privileges SELECT null,?, M.ID,M.DefaultRead,M.DefaultWrite,M.DefaultDelete,M.DefaultPrint,M.DefaultExport FROM menus M WHERE M.Inactive<>1;', [
                UserID
            ]);
            db.actionQuery(query, function(err, resp) {
                if (err) {
                    callback(err, null);
                }
                callback(null, resp);
            });
        }
    ], next);
};

exports.Update_User = (UserID, U, next) => {
    var cryptr = new Cryptr(config.app_secretkey);
    if (U.Password) {
        var encryptedPassword = cryptr.encrypt(U.Password);
        U.Password = encryptedPassword;
    }

    var user = {
        UserName: U.UserName,
        Password: U.Password,
        DateModified: new Date(),
        ModifiedBy: U.UserID,
        FullName: U.FullName,
        Email: U.Email,
        MobileNo: U.MobileNo,
        IsEmployee: U.IsEmployee || 0,
        EmployeeID: U.EmployeeID
    };
    var strSQL = mysql.format('UPDATE users SET ? WHERE MD5(UserID) =?', [
        user, UserID
    ]);
    console.log('strSQL: ', strSQL);
    db.actionQuery(strSQL, next);
};

exports.GetAllUsers = (next) => {
    var strSQL = mysql.format('SELECT U.UserID,U.EmployeeID,U.FullName,U.UserName,IFNULL(UG.GroupName,"CUSTOM") AS UserGroup,U.DateCreated,MD5(E.e_id) as E_UUID, MD5(E.e_idno) as Emp_UUID,MD5(U.UserID) AS UUID ' +
        'FROM users U LEFT JOIN employees E ON E.e_idno = U.EmployeeID LEFT JOIN users_group UG ON U.UserGroupID = UG.GroupID');
    db.query(strSQL, next);
};

exports.Delete_User = (UserID, next) => {
    var query = mysql.format('DELETE FROM users WHERE MD5(UserID) =?', [UserID]);
    db.actionQuery(query, next);
};

exports.GetUserByUserID = (UserID, next) => {
    var str = mysql.format('SELECT U.UserID,UG.GroupID,U.UserName,U.FullName,U.Inactive,U.EmployeeID,U.IsEmployee,U.Email,U.MobileNo,U.AccessLimit,MD5(E.e_id) as E_UUID, MD5(E.e_idno) as Emp_UUID,MD5(U.UserID) AS UUID ' +
        'FROM users U LEFT JOIN employees E ON E.e_idno = U.EmployeeID LEFT JOIN users_group UG ON U.UserGroupID = UG.GroupID WHERE MD5(U.UserID)=? LIMIT 1;', [UserID]);
    console.log('str: ',str);
    db.query(str, next);
};

exports.Update_User_Password = (UserID, newPassword, next) => {
    var cryptr = new Cryptr(config.app_secretkey);
    var encryptedPassword = cryptr.encrypt(newPassword);
    var strSQL = mysql.format('UPDATE users SET Password=? WHERE MD5(UserID) =?', [encryptedPassword, UserID]);
    console.log('strSQL: ', strSQL);
    db.query(strSQL, next);
};

exports.Update_Users_Group = (UserID, GroupID, next) => {
    var strSQL = mysql.format('UPDATE users SET UserGroupID=? WHERE MD5(UserID) =?', [GroupID, UserID]);
    db.query(strSQL, next);
};

exports.setUserPrivilege = (UserID, data, next) => {
    var sSQL = '';
    if (data.action == 'read') {
        sSQL = 'UPDATE user_privileges SET `Read`=? WHERE ModuleID=? AND UserID=?;';
    } else if (data.action == 'write') {
        sSQL = 'UPDATE user_privileges SET `Write`=? WHERE ModuleID=? AND UserID=?;';
    } else if (data.action == 'delete') {
        sSQL = 'UPDATE user_privileges SET `Delete`=? WHERE ModuleID=? AND UserID=?;';
    } else if (data.action == 'print') {
        sSQL = 'UPDATE user_privileges SET `Print`=? WHERE ModuleID=? AND UserID=?;';
    }
    var strSQL = mysql.format(sSQL, [
        data.value, data.ModuleID, UserID
    ]);
    db.actionQuery(strSQL, next);
};

exports.setUserPrivilege2 = (UserID, data, next) => {
    var sSQL = 'UPDATE user_privileges SET `Read`=?,`Write`=?,`Delete`=?,`Print`=? WHERE ModuleID=? AND MD5(UserID)=?;';
    var strSQL = mysql.format(sSQL, [
        data.DefaultRead, data.DefaultWrite, data.DefaultDelete, data.DefaultPrint, data.ID, UserID
    ]);
    console.log('strSQL: ',strSQL);
    db.actionQuery(strSQL, next);
};


exports.getUserPrivilege = (UserID,next)=>{
    var strSQL = mysql.format('SELECT UP.UserID,MO.ModuleID,MO.ModuleName,ME.Caption,ME.Description,UP.Read AS DefaultRead,UP.Write AS DefaultWrite ,UP.Delete AS DefaultDelete,UP.Print AS DefaultPrint,UP.Export AS DefaultExport, ME.Inactive,ME.ID FROM menus ME ' +
        'LEFT JOIN modules MO ON ME.Module = MO.ModuleID LEFT JOIN user_privileges UP ON UP.ModuleID = ME.ID ' + 
        'WHERE MD5(UP.UserID)=? ORDER BY MO.ModuleID;',[UserID]);
    db.query(strSQL,next);
};

exports.setUserGroup = (UserID,data,next)=>{
    var strSQL = mysql.format('UPDATE users SET UserGroupID=? WHERE MD5(UserID)=?;',[data.UserGroupID,UserID]);
    console.log('strSQL: ',strSQL);
    db.actionQuery(strSQL,next);
};


exports.Add_User_Group = (UG, next) => {
    var strSQL = mysql.format('INSERT INTO users_group(GroupName,GroupDesc) VALUES (?,?);', [UG.GroupName, UG.GroupDesc]);
    db.insertWithId(strSQL, (err, GroupID) => {
        if (err) {
            next(err, null);
        }
        var strSQL2 = mysql.format('INSERT INTO group_privileges  SELECT null,?, M.ID,M.DefaultRead,M.DefaultWrite,M.DefaultDelete,M.DefaultPrint,M.DefaultExport FROM menus M WHERE M.Inactive<>1;', [GroupID]);
        db.insertWithId(strSQL2, next);
    });
};

exports.Update_User_Group = (GroupID, UG, next) => {
    var strSQL = mysql.format('UPDATE users_group SET GroupName=?,GroupDesc=? WHERE MD5(GroupID)=?;', [UG.GroupName, UG.GroupDesc, GroupID]);
    db.actionQuery(strSQL, next);
};

exports.Delete_User_Group = (GroupID, next) => {
    var strSQL = mysql.format('DELETE FROM users_group WHERE MD5(GroupID)=?;', [GroupID]);
    console.log('strSQL: ', strSQL);
    db.actionQuery(strSQL, next);
};

exports.GetAllUserGroups = (next) => {
    var strSQL = mysql.format('SELECT *,MD5(GroupID) AS UUID FROM users_group;');
    db.query(strSQL, next);
};

exports.GetUserGroupByID = (GroupID, next) => {
    var strSQL = mysql.format('SELECT *,MD5(GroupID) AS UUID FROM users_group WHERE MD5(GroupID)=? LIMIT 1;', [GroupID]);
    db.query(strSQL, next);
};

exports.setGroupPrivilege = (GroupID, data, next) => {
    var sSQL = '';
    if (data.action == 'read') {
        sSQL = 'UPDATE group_privileges SET `Read`=? WHERE ModuleID=? AND GroupID=?;';
    } else if (data.action == 'write') {
        sSQL = 'UPDATE group_privileges SET `Write`=? WHERE ModuleID=? AND GroupID=?;';
    } else if (data.action == 'delete') {
        sSQL = 'UPDATE group_privileges SET `Delete`=? WHERE ModuleID=? AND GroupID=?;';
    } else if (data.action == 'print') {
        sSQL = 'UPDATE group_privileges SET `Print`=? WHERE ModuleID=? AND GroupID=?;';
    }
    var strSQL = mysql.format(sSQL, [
        data.value, data.ModuleID, GroupID
    ]);
    db.actionQuery(strSQL, next);
};

exports.setGroupPrivilege2 = (GroupID, data, next) => {
    var sSQL = 'UPDATE group_privileges SET `Read`=?,Write`=?,`Delete`=?,`Print`=? WHERE MD5(ID)=? AND MD5(GroupID)=?;';
    var strSQL = mysql.format(sSQL, [
        data.DefaultRead, data.DefaultWrite, data.DefaultDelete, data.DefaultPrint, data.ID,GroupID
    ]);
    console.log('strSQL: ',strSQL);
    db.actionQuery(strSQL, next);
};

exports.getGroupPrivilege = (GroupID,next)=>{
    var strSQL = mysql.format('SELECT GP.GroupID,MO.ModuleID,MO.ModuleName,ME.Caption,ME.Description,GP.Read AS DefaultRead,GP.Write AS DefaultWrite, GP.Delete AS DefaultDelete, GP.Export AS DefaultExport,GP.Print AS DefaultPrint,ME.Inactive,ME.ID ' + 
        'FROM menus ME LEFT JOIN modules MO ON ME.Module = MO.ModuleID LEFT JOIN group_privileges GP ON GP.ModuleID = ME.ID ' + 
        'WHERE MD5(GP.GroupID) = ? ORDER BY MO.ModuleID;',[GroupID]);
    db.query(strSQL,next);
};

exports.IsUserGroupHasPermission = (GroupID, next) => {
    var strSQL = mysql.format('SELECT * FROM group_privileges WHERE MD5(GroupID)=? LIMIT 1;', [GroupID]);
    db.query(strSQL, next);
};

exports.User_Group_isExisted = (GroupName, next) => {
    var strSQL = mysql.format('SELECT GroupName FROM users_group WHERE GroupName =? LIMIT 1;', [GroupName]);
    db.query(strSQL, next);
};

exports.User_Group_isExisted2 = (GroupID, GroupName, next) => {
    var strSQL = mysql.format('SELECT GroupName FROM users_group WHERE GroupName =? AND MD5(GroupID) <> ? LIMIT 1;', [GroupName, GroupID]);
    db.query(strSQL, next);
};



exports.PopulateAccessPriviledge = (next) => {
    var strSQL = mysql.format('SELECT MO.ModuleID,MO.ModuleName,ME.Caption,ME.Description,ME.DefaultRead,ME.DefaultWrite,ME.DefaultDelete,ME.DefaultExport,ME.DefaultPrint, ME.Inactive,ME.ID,MD5(ME.ID) AS UUID FROM menus ME LEFT JOIN modules MO ON ME.Module = MO.ModuleID ORDER BY MO.ModuleID');
    db.query(strSQL, next);
};