'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();

var async = require('async');
var _ = require('lodash-node');


exports.getAllPHIC = (next) => {
    var strSQL = mysql.format('SELECT *,MD5(PHID) AS UUID FROM phil_health;');
    db.query(strSQL, next);
};

exports.getPhilHealthDetails = (phic_id , next) => {
    var strSQL = mysql.format('select * , MD5(PHID) AS UUID FROM phil_health_details where MD5(PHID) = ?' ,[phic_id]);
    db.query(strSQL, next);
};

exports.getPHTableById = (phic_id, next) => {
    var strSQL = mysql.format('SELECT *,MD5(PHID) AS UUID FROM phil_health WHERE MD5(PHID) =? LIMIT 1;', [phic_id]);
    db.query(strSQL, next);
};

exports.AddPHTable = (P, next) => {
    var strSQL = mysql.format('INSERT INTO phil_health(PH_Code, PH_Name, PH_Description, Inactive, DateCreated, CreatedBy) VALUES (?,?,?,?,CURDATE(),?);', [
        P.PH_Code, P.PH_Name, P.PH_Description, P.Inactive || 0, P.UserID
    ]);
    db.insertWithId(strSQL, function(e,r){
        
        if(e){
            next(e,null);
        }else{
            next(null,r);
        }
    });
};

exports.UpdatePHTable = (phic_id, P, next) => {
    var strSQL = mysql.format('UPDATE phil_health SET PH_Code =?,PH_Name =?,PH_Description =?,Inactive =?,DateModified = CURDATE(), ModifiedBy =? WHERE MD5(PHID) =?;', [
        P.PH_Code, P.PH_Name, P.PH_Description, P.Inactive || 0, P.UserID, phic_id
    ]);
    db.actionQuery(strSQL, next);
};

exports.DelPHTable = (phic_id,next)=>{
    var strSQL = mysql.format('DELETE FROM phil_health WHERE MD5(PHID) =?;',[phic_id]);
    db.actionQuery(strSQL,next);
};

exports.GetPHDetailsByPHICID = (phic_id, next) => {
    var strSQL = mysql.format('SELECT * FROM phil_health_details WHERE IndexID= ? LIMIT 1;', [phic_id]);
    db.query(strSQL, next);
};

exports.GetPHDetailsByID = (phic_id, indexid , next) => {
    var strSQL = mysql.format('SELECT *,MD5(IndexID) AS UUID FROM phil_health_details WHERE MD5(IndexID)= ? LIMIT 1;', [phic_id]);
    db.query(strSQL, next);
};

exports.AddPHDetails = (phic_id , P, next) => {
    var strSQL = mysql.format('INSERT INTO phil_health_details(PHID , bracketfrom, bracketto, salarybase , totalmonthlycontribution , employeeshare, employershare, over) VALUES (?,?,?,?,?,?,?,?);', [
        phic_id -(1000) , P.bracketfrom, P.bracketto, P.salarybase, P.totalmonthlycontribution , P.employeeshare, P.employershare, P.over
    ]);
    db.insertWithId(strSQL, next);
};

exports.UpdatePHDetails = (IndexID, phic_id , P, next) => {
    var strSQL = mysql.format('UPDATE phil_health_details SET bracketfrom =?,bracketto =?, salarybase = ? ,totalmonthlycontribution =? , employeeshare =?,employershare =?, over =? WHERE IndexID=?;', [
        P.bracketfrom, P.bracketto, P.salarybase,  P.totalmonthlycontribution,  P.employeeshare, P.employershare, P.over, IndexID
    ]);
    db.actionQuery(strSQL, next);
};

exports.DelPHDetails = (IndexID, phic_id , next)=>{
    console.log('strSQL:' , strSQL)
    var strSQL = mysql.format('DELETE FROM phil_health_details WHERE IndexID  =?;',[IndexID]);
    db.actionQuery(strSQL,next);
};

