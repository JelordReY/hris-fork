'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();

var async = require('async');
var _ = require('lodash-node');
var fs = require('fs');

exports.Add_Company_Info = (data, next) => {
    var strSQL = mysql.format('INSERT INTO company_info VALUES(null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);', [
        data.ci_name || '', data.ci_acroname || '', data.ci_fax || '', data.ci_telephone || '', data.ci_city, data.ci_streetadd, data.ci_address || '', data.ci_provincialadd, data.ci_zipcode, data.ci_late_deduction || '', data.tp_id || '', data.ci_restday1 || '', data.ci_restday2 || '', data.ci_restday3 || '', data.ci_reg_ot || '', data.ci_rd_ot || '', data.ci_work_sp || '', data.ci_work_sp_rd || '', data.ci_work_rh || '', data.ci_ssno, data.ci_gsisno, data.ci_pagibigno, data.ci_tinno, data.ci_philhealthno, data.ci_apply_break_logs || '', data.ci_modifiedby, data.ci_datemodified || '', data.ci_owner, data.ci_logo || '', data.ci_mission || '', data.ci_vision || '', data.ci_history || '', data.ci_website || '', data.ci_zipcode || '', data.ci_url || '', data.office_code || ''
    ]);
    db.insertWithId(strSQL, next);
};
exports.getCompanyInfo = (next) => {
    db.query(mysql.format('SELECT * FROM company_info LIMIT 1;'), next);
};


exports.DeleteCompanyProfile = (ci_id, next) => {
    var strSQL = mysql.format('UPDATE company_info SET ci_logo=null WHERE ci_id=?;', [ci_id]);
    db.actionQuery(strSQL, next);

};

exports.getCompanyById = (ci_id, next) => {
    var strSQL = mysql.format('SELECT *, md5(ci_id) AS UUID  FROM company_info WHERE md5(ci_id)=? LIMIT 1;', [ci_id]);
    db.query(strSQL, next);
};

exports.deleteCompany = (ci_id, next) => {
    db.query(mysql.format('DELETE FROM company_info WHERE ci_id=?;', [ci_id]), next);
};


exports.AddCompanyProfile = (ci_id, data, next) => {
    switch (data.arg) {
        case 'Logo':
            var strSQL = mysql.format('UPDATE company_info SET ci_logo=? WHERE ci_id=?;', [fs.readFileSync(data.path), ci_id]);
            db.actionQuery(strSQL, next);
            break;
        case 'Mission':
            var strSQL1 = mysql.format('UPDATE company_info SET ci_mission=? WHERE ci_id=?;', [fs.readFileSync(data.path), ci_id]);
            db.actionQuery(strSQL1, next);
            break;
        case 'Vision':
            var strSQL2 = mysql.format('UPDATE company_info SET ci_vision=? WHERE ci_id=?;', [fs.readFileSync(data.path), ci_id]);
            db.actionQuery(strSQL2, next);
            break;
        case 'History':
            var strSQL3 = mysql.format('UPDATE company_info SET ci_history=? WHERE ci_id=?;', [fs.readFileSync(data.path), ci_id]);
            db.actionQuery(strSQL3, next);
            break;
        default:

    }


};

exports.updateCompanyInfo = (ci_id, data, next) => {
    var strSQL = mysql.format('UPDATE company_info SET ci_name=? ,  ci_acroname = ? ,   ci_fax = ? ,    ci_telephone =? ,   ci_city=? , ci_streetadd = ? ,  ci_address = ? ,    ci_provincialadd =? , ci_zipcode = ? , ci_late_deduction=? ,    tp_id = ?  , ci_restday1 = ? ,    ci_restday2 =? , ci_restday3 = ? , ci_reg_ot=? , ci_rd_ot = ?  , ci_work_sp = ? ,   ci_work_sp_rd =? ,   ci_work_rh = ? , ci_work_rh_rd=? , ci_ssno = ?  , ci_gsisno = ? ,       ci_pagibigno =? ,   ci_tinno = ? , ci_philhealthno=? , ci_apply_break_logs = ? , ci_modifiedby=? ,     ci_datemodified = ?  , ci_owner = ? ,       ci_logo =? ,    ci_mission = ? , ci_vision=? , ci_history = ? , ci_website=? ,    ci_url = ?  , office_code = ?  WHERE ci_id =?;', [
        data.ci_name || '', data.ci_acroname || '', data.ci_fax || '', data.ci_telephone || '', data.ci_city, data.ci_streetadd, data.ci_address || '', data.ci_provincialadd, data.ci_zipcode, data.ci_late_deduction || '', data.tp_id || '', data.ci_restday1 || '', data.ci_restday2 || '', data.ci_restday3 || '', data.ci_reg_ot || '', data.ci_rd_ot || '', data.ci_work_sp || '', data.ci_work_sp_rd || '', data.ci_work_rh || '', data.ci_ssno, data.ci_gsisno, data.ci_pagibigno, data.ci_tinno, data.ci_philhealthno, data.ci_apply_break_logs || '', data.ci_modifiedby, data.ci_datemodified || '', data.ci_owner, data.ci_logo || '', data.ci_mission || '', data.ci_vision || '', data.ci_history || '', data.ci_website || '', data.ci_zipcode || '', data.ci_url || '', data.office_code || '', ci_id

    ]);
    db.actionQuery(strSQL, next);
};