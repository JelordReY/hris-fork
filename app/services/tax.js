'use strict';

var mysql = require('mysql');
var Database = require('../../app/utils/database').Database;
var db = new Database();

var async = require('async');
var _ = require('lodash-node');


exports.getAllWithholdingTax = (next) => {
    var strSQL = mysql.format('SELECT *,MD5(TAXID) as UUID FROM pay_withholdingtax ORDER BY Code ASC;');
    db.query(strSQL, next);
};

exports.getTaxTableByID = (tax_id, next) => {
    var strSQL = mysql.format('SELECT *,MD5(TAXID) as UUID FROM pay_withholdingtax WHERE MD5(TAXID)=?;', [tax_id]);
    db.query(strSQL, next);
};

exports.getAllTaxSummary = (tax_id, next) => {
    async.waterfall([
        (callback) => {
            var strSQL = mysql.format('SELECT pt.tp_name, e.ExemptionCode, wtd.IndexID , wtd.Range1, wtd.Range2, wtd.Range3, wtd.Range4, wtd.Range5, wtd.Range6, wtd.Range7, wtd.Range8 FROM pay_withholdingtaxdetails wtd INNER JOIN paytypes pt ON pt.tp_id = wtd.CategoryID INNER JOIN exemption e ON wtd.ExemptionID = e.IndexID WHERE MD5(wtd.TAXID) = ? AND wtd.CategoryID = 1 ORDER BY pt.tp_id ASC, e.IndexID ASC', [tax_id])
            db.query(strSQL, (err, monthly) => {
                if (err) {
                    next(err, null);
                }
                callback(null, monthly);
            });
        },
        (monthly, callback) => {
            var strSQL = mysql.format('SELECT pt.tp_name, e.ExemptionCode, wtd.IndexID , wtd.Range1, wtd.Range2, wtd.Range3, wtd.Range4, wtd.Range5, wtd.Range6, wtd.Range7, wtd.Range8 FROM pay_withholdingtaxdetails wtd INNER JOIN paytypes pt ON pt.tp_id = wtd.CategoryID INNER JOIN exemption e ON wtd.ExemptionID = e.IndexID WHERE MD5(wtd.TAXID) = ? AND wtd.CategoryID = 2 ORDER BY pt.tp_id ASC, e.IndexID ASC', [tax_id])
            db.query(strSQL, (err, semimonthly) => {
                if (err) {
                    next(err, null);
                }
                callback(null, monthly, semimonthly);
            });
        },
        (monthly, semimonthly, callback) => {
            var strSQL = mysql.format('SELECT pt.tp_name, e.ExemptionCode, wtd.IndexID ,  wtd.Range1, wtd.Range2, wtd.Range3, wtd.Range4, wtd.Range5, wtd.Range6, wtd.Range7, wtd.Range8 FROM pay_withholdingtaxdetails wtd INNER JOIN paytypes pt ON pt.tp_id = wtd.CategoryID INNER JOIN exemption e ON wtd.ExemptionID = e.IndexID WHERE MD5(wtd.TAXID) = ? AND wtd.CategoryID = 3 ORDER BY pt.tp_id ASC, e.IndexID ASC', [tax_id])
            db.query(strSQL, (err, weekly) => {
                if (err) {
                    next(err, null);
                }
                callback(null, monthly, semimonthly, weekly);
            });
        },

        (monthly, semimonthly, weekly, callback) => {
            var strSQL = mysql.format('SELECT p.tp_name, t.Range1, t.Range2, t.Range3, t.Range4, t.Range5, t.Range6, t.Range7, t.Range8, t.IndexID, t.CategoryID FROM pay_taxceiling t INNER JOIN paytypes p ON p.tp_id = t.CategoryID WHERE MD5(t.TaxID) = ? ORDER BY p.tp_id ASC', [tax_id])
            db.query(strSQL, (err, taxceil) => {
                if (err) {
                    next(err, null);
                }
                callback(null, monthly, semimonthly, weekly, taxceil);
            });
        },

        (monthly, semimonthly, weekly, taxceil, callback) => {
            var strSQL = mysql.format('SELECT Range1, Range2, Range3, Range4, Range5, Range6, Range7, Range8, IndexID FROM tax_percent WHERE MD5(TaxID) = ?', [tax_id])
            db.query(strSQL, (err, taxpercentage) => {
                if (err) {
                    next(err, null);
                }
                callback(null, monthly, semimonthly, weekly, taxceil, taxpercentage);
            });
        },

        (monthly, semimonthly, weekly, taxceil, taxpercentage, callback) => {
            var strSQL = mysql.format('SELECT BracketFrom, BracketTo, Plus, Less, Percent, IndexID FROM pay_taxdue WHERE MD5(TaxID) = ? ORDER BY BracketFrom ASC ', [tax_id])
            db.query(strSQL, (err, taxdue) => {
                if (err) {
                    next(err, null);
                }
                callback(null, monthly, semimonthly, weekly, taxceil, taxpercentage, taxdue);
            });
        },

        (monthly, semimonthly, weekly, taxceil, taxpercentage, taxdue, callback) => {
            var strSQL = mysql.format('SELECT pt.tp_name, e.ExemptionCode, wtd.IndexID ,  wtd.Range1, wtd.Range2, wtd.Range3, wtd.Range4, wtd.Range5, wtd.Range6, wtd.Range7, wtd.Range8 FROM pay_withholdingtaxdetails wtd INNER JOIN paytypes pt ON pt.tp_id = wtd.CategoryID INNER JOIN exemption e ON wtd.ExemptionID = e.IndexID WHERE MD5(wtd.TAXID) = ? AND wtd.CategoryID = 4 ORDER BY pt.tp_id ASC, e.IndexID ASC', [tax_id])
            db.query(strSQL, (err, daily) => {
                if (err) {
                    next(err, null);
                }   

                callback(null, {
                    monthly: monthly,
                    semimonthly: semimonthly,
                    weekly: weekly,
                    daily: daily,
                    taxceil: taxceil,
                    taxpercentage: taxpercentage,
                    taxdue: taxdue
                });
            });
        }

    ], next);
};



exports.newWithholdingTax = (W, next) => {
    var strSQL = mysql.format('INSERT INTO pay_withholdingtax (Code, Name, Description, InActive, DateCreated, CreatedBy) VALUES(?,?,?,?,CURDATE(),?);', [
        W.Code, W.Name, W.Description, W.InActive || 0, W.UserID
    ]);
    db.insertWithId(strSQL, next);
};

exports.updateWithholdingTax = (tax_id, W, next) => {
    var strSQL = mysql.format('UPDATE pay_withholdingtax SET Code =?,Name =?, Description =?,InActive =?,DateModified = CURDATE(), ModifiedBy =? WHERE MD5(TAXID)=?;', [
        W.Code, W.Name, W.Description, W.InActive || 0, W.UserID, tax_id
    ]);
    db.actionQuery(strSQL, next);
};

exports.removeWithholdingTax = (tax_id, next) => {
    var strSQL = mysql.format('DELETE FROM pay_withholdingtax WHERE MD5(TAXID)=?;', [tax_id]);
    db.actionQuery(strSQL, next);
};

exports.newTaxDetails = (tax_id, wtd, next) => {
    var strSQL = mysql.format('INSERT INTO pay_withholdingtaxdetails (TaxID, CategoryID, ExemptionID, Range1, Range2, Range3, Range4, Range5, Range6, Range7, Range8) VALUES(?,?,?,?,?,?,?,?,?,?,?);', [
        tax_id, wtd.CategoryID, wtd.ExemptionID, wtd.Range1, wtd.Range2, wtd.Range3, wtd.Range4, wtd.Range5, wtd.Range6, wtd.Range7, wtd.Range8
    ]);
    db.insertWithId(strSQL, next);
};

exports.updateTaxDetails = (IndexID, tax_id, wtd, next) => {
    var strSQL = mysql.format('UPDATE pay_withholdingtaxdetails SET  ExemptionID = ? , Range1=?, Range2=?, Range3=?, Range4=?, Range5=?, Range6=?, Range7=?, Range8=? WHERE IndexID=?;', [
         wtd.ExemptionID , wtd.Range1, wtd.Range2, wtd.Range3, wtd.Range4, wtd.Range5, wtd.Range6, wtd.Range7, wtd.Range8, IndexID
    ]);
    db.insertWithId(strSQL, next);
};

exports.removeTaxDetailsByTAXID = (tax_id, next) => {
    var strSQL = mysql.format('DELETE FROM pay_withholdingtaxdetails WHERE IndexID =?;', [tax_id]);

    db.actionQuery(strSQL, next);
};

exports.getTaxDetails = (tax_id, next) => {
    var strSQL = mysql.format('SELECT *  FROM pay_withholdingtaxdetails  WHERE IndexID =? LIMIT 1;', [tax_id]);
    db.query(strSQL, next);
};

exports.getTaxDueByID = (tax_id, next) => {
    var strSQL = mysql.format('SELECT * FROM pay_taxdue  WHERE IndexID =? LIMIT 1;', [tax_id]);
    db.query(strSQL, next);
};


exports.getTaxCeilingByID = (tax_id, next) => {
    var strSQL = mysql.format('SELECT * FROM pay_taxceiling  WHERE IndexID =? LIMIT 1;', [tax_id]);
    db.query(strSQL, next);
};

exports.getTaxPercentByID = (tax_id, next) => {
    var strSQL = mysql.format('SELECT * FROM tax_percent  WHERE IndexID =? LIMIT 1;', [tax_id]);
    db.query(strSQL, next);
};

exports.newTaxCeil = (tax_id, tc, next) => {
    var strSQL = mysql.format('INSERT INTO pay_taxceiling (TaxID, CategoryID, Range1, Range2, Range3, Range4, Range5, Range6, Range7, Range8) VALUES(?,?,?,?,?,?,?,?,?,?);', [
        tax_id, tc.CategoryID, tc.Range1, tc.Range2, tc.Range3, tc.Range4, tc.Range5, tc.Range6, tc.Range7, tc.Range8
    ]);
    db.insertWithId(strSQL, next);
};

exports.updateTaxCeil = (IndexID, tax_id, tc, next) => {
    var strSQL = mysql.format('UPDATE pay_taxceiling SET CategoryID = ? , Range1=?, Range2=?, Range3=?, Range4=?, Range5=?, Range6=?, Range7=?, Range8=? WHERE IndexID=?;', [
        tc.CategoryID, tc.Range1, tc.Range2, tc.Range3, tc.Range4, tc.Range5, tc.Range6, tc.Range7, tc.Range8, IndexID
    ]);
    db.actionQuery(strSQL, next);
};

exports.removeTaxCeilByTAXID = (tax_id, next) => {
    var strSQL = mysql.format('DELETE FROM pay_taxceiling WHERE IndexID =?;', [tax_id]);

    db.actionQuery(strSQL, next);
};


exports.newTaxPercent = (tax_id, tp, next) => {
    var strSQL = mysql.format('INSERT INTO tax_percent (TaxID, Range1, Range2, Range3, Range4, Range5, Range6, Range7, Range8) VALUES(?,?,?,?,?,?,?,?,?);', [
        tax_id, tp.Range1, tp.Range2, tp.Range3, tp.Range4, tp.Range5, tp.Range6, tp.Range7, tp.Range8
    ]);
    db.insertWithId(strSQL, next);
};

exports.updateTaxPercent = (IndexID, tax_id, tp, next) => {
    var strSQL = mysql.format('UPDATE tax_percent SET Range1=?, Range2=?, Range3=?, Range4=?, Range5=?, Range6=?, Range7=?, Range8=? WHERE IndexID=?;', [
        tp.Range1, tp.Range2, tp.Range3, tp.Range4, tp.Range5, tp.Range6, tp.Range7, tp.Range8, IndexID
    ]);
    db.actionQuery(strSQL, next);
};

exports.updateTaxDue = (IndexID, tax_id, td, next) => {
    var strSQL = mysql.format('UPDATE pay_taxdue SET BracketFrom=?, BracketTo=?, Less=?, Plus=?, Percent=? WHERE IndexID=?;', [
        td.BracketFrom, td.BracketTo, td.Less, td.Plus, td.Percent, IndexID
    ]);
    db.actionQuery(strSQL, next);
};

exports.removeTaxPercentByTAXID = (tax_id, next) => {
    var strSQL = mysql.format('DELETE FROM tax_percent WHERE IndexID  =?;', [tax_id]);
    db.actionQuery(strSQL, next);
};


exports.newTaxDue = (tax_id, td, next) => {
    console.log('td:',td)
    var strSQL = mysql.format('INSERT INTO pay_taxdue (TaxID, BracketFrom, BracketTo, Less, Plus, Percent) VALUES(?,?,?,?,?,?);', [
        tax_id, td.BracketFrom, td.BracketTo, td.Less, td.Plus, td.Percent
    ]);
    db.insertWithId(strSQL, next);
};


exports.removeTaxdueByTAXID = (tax_id, next) => {
    var strSQL = mysql.format('DELETE FROM pay_taxdue WHERE IndexID = ?;', [tax_id]);
    db.actionQuery(strSQL, next);
};

exports.removeTaxdue = (IndexID, next) => {
    var strSQL = mysql.format('DELETE FROM pay_taxdue WHERE MD5(IndexID) = ?;', [IndexID]);
    db.actionQuery(strSQL, next);
};