'use strict';

exports.validateWithHoldingTax = function(req, res, next) {
    req.checkBody('Code', 'Please Provide A Withholding Tax Code.').notEmpty();
    req.checkBody('Name', 'Please Provide A Withholding Tax Name.').notEmpty();
    req.checkBody('Description', 'Please Provide A Description').notEmpty();
    req.checkBody('InActive', 'Please Confirm If Active Or Not').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};

exports.validateTaxDetails = function(req, res, next) {
    req.checkBody('ExemptionID', 'Please Provide An  Exemption').notEmpty();
    req.checkBody('Range1', 'Please Provide A Range1.').notEmpty();
    req.checkBody('Range2', 'Please Provide A Range2.').notEmpty();
    req.checkBody('Range3', 'Please Provide A Range3.').notEmpty();
    req.checkBody('Range4', 'Please Provide A Range4.').notEmpty();
    req.checkBody('Range5', 'Please Provide A Range5.').notEmpty();
    req.checkBody('Range6', 'Please Provide A Range6.').notEmpty();
    req.checkBody('Range7', 'Please Provide A Range7.').notEmpty();
    req.checkBody('Range8', 'Please Provide A Range8.').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};


exports.validateTaxCeils = function(req, res, next) {
    req.checkBody('CategoryID', 'Please Provide A Category').notEmpty();
    req.checkBody('Range1', 'Please Provide A Range1.').notEmpty();
    req.checkBody('Range2', 'Please Provide A Range2.').notEmpty();
    req.checkBody('Range3', 'Please Provide A Range3.').notEmpty();
    req.checkBody('Range4', 'Please Provide A Range4.').notEmpty();
    req.checkBody('Range5', 'Please Provide A Range5.').notEmpty();
    req.checkBody('Range6', 'Please Provide A Range6.').notEmpty();
    req.checkBody('Range7', 'Please Provide A Range7.').notEmpty();
    req.checkBody('Range8', 'Please Provide A Range8.').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};


exports.validateTaxPercent = function(req, res, next) {
    
    req.checkBody('Range1', 'Please Provide A Range1.').notEmpty();
    req.checkBody('Range2', 'Please Provide A Range2.').notEmpty();
    req.checkBody('Range3', 'Please Provide A Range3.').notEmpty();
    req.checkBody('Range4', 'Please Provide A Range4.').notEmpty();
    req.checkBody('Range5', 'Please Provide A Range5.').notEmpty();
    req.checkBody('Range6', 'Please Provide A Range6.').notEmpty();
    req.checkBody('Range7', 'Please Provide A Range7.').notEmpty();
    req.checkBody('Range8', 'Please Provide A Range8.').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};


exports.validateTaxDue = function(req, res, next) {
    req.checkBody('BracketFrom', 'Please Provide A Bracket ( From : )').notEmpty();
    req.checkBody('BracketTo', 'Please Provide A Bracket ( To : ).').notEmpty();
    req.checkBody('Less', 'Please Provide A Tax Difference.').notEmpty();
    req.checkBody('Plus', 'Please Provide A Tax Ceiling.').notEmpty();
    req.checkBody('Percent', 'Please Provide A Tax Percentage.').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};