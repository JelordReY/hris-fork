'use strict';

exports.validateTaxExemption = function(req, res, next) {
    req.checkBody('ExemptionCode', 'Please Provide A Tax Exemption Code.').notEmpty();
    req.checkBody('Exemption', 'Please Provide An Exemption Description.').notEmpty();
    req.checkBody('TaxExemption', 'Please Provide A Tax Exemption.').notEmpty();
    req.checkBody('Inactive', 'Please Confirm Tax Inacivity. ').notEmpty();
 

    var errors = req.validationErrors();

    if (errors) {
        res.status(400).send({
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};

