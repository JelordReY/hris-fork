'use strict';

exports.validateHDMF = function(req, res, next) {
    req.checkBody('MonthlyCompensation', 'Please Provide Monthly Compensation.').notEmpty();
    req.checkBody('BracketFrom', 'Please Provide A Bracket (From).').notEmpty();
    req.checkBody('BracketTo', 'Please Provide A Bracket (To).').notEmpty();
    req.checkBody('EmployeeShare', 'Please Procide A Share (Employee). ').notEmpty();
    req.checkBody('Total', 'Please Provide A Total . ').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        res.status(400).send({
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};

