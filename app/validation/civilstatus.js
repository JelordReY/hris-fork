'use strict';

exports.validateCivilStatus = function(req, res, next) {
    req.checkBody('cs_name', 'Please Provide A Civil Status').notEmpty();
   
    var errors = req.validationErrors();

    if (errors) {
        res.status(400).send({
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};