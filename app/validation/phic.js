'use strict';

exports.validatePHIC = function(req, res, next) {
    req.checkBody('PH_Name', 'Please Provide A Phil-Health Name.').notEmpty();
    req.checkBody('PH_Code', 'Please Provide A Phil-health Code.').notEmpty();
    req.checkBody('PH_Description', 'Please Provide A Phil-Health Description.').notEmpty();
    req.checkBody('Inactive', 'Please Confirm PH- Inactivity. ').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        res.status(400).send({
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};

exports.validatePhicBrackets = function(req, res, next) {
    req.checkBody('bracketfrom', 'Please Provide A Bracket (From : ).').notEmpty();
    req.checkBody('bracketto', 'Please Provide A Bracket (To) .').notEmpty();
    req.checkBody('salarybase', 'Please Provide Salarybase.').notEmpty();
    req.checkBody('totalmonthlycontribution', 'Please Provide Monthly Total Contribution. ').notEmpty();
    req.checkBody('employeeshare', 'Please Provide A Share Of Employee.').notEmpty();
    req.checkBody('employershare', 'Please Provide A Share Of Employer. ').notEmpty();
    req.checkBody('over', 'Please Provide An " Over " . ').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        res.status(400).send({
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};

