'use strict';

exports.validateDepartment = function(req, res, next) {
    req.checkBody('d_name', 'Please provide Department Name').notEmpty();
    req.checkBody('d_code', 'Please provide Department Code').notEmpty();
    req.checkBody('short_name', 'Please provide Department Shortname.').notEmpty();
    req.checkBody('branch_id ', 'Please provide Department Branch')
   
   
    var errors = req.validationErrors();

    if (errors) {
        res.status(400).send({
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};