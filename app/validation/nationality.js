'use strict';

exports.validateNationality = function(req, res, next) {
    req.checkBody('Nationality', 'Please Provide A Nationality.').notEmpty();
    req.checkBody('ShortName', 'Please Provide A Nationality Shortname').notEmpty();
    req.checkBody('Description', 'Please Provide A Nationality Short Description.').notEmpty();

  
   
    var errors = req.validationErrors();

    if (errors) {
        res.status(400).send({
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};