'use strict';

exports.validateBranch = function(req, res, next) {
    req.checkBody('branch_code', 'Please Provide Branch Code').notEmpty();
    req.checkBody('branch_name', 'Please Provide Branch Name').notEmpty();
    req.checkBody('short_name', 'Please Provide Branch Short Name ').notEmpty();
    req.checkBody('address', 'Please Provide Branch Address').notEmpty();
    req.checkBody('inactive', 'Please Provide Branch In-Activity').notEmpty();

   var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({  
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};