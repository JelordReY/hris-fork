'use strict';

exports.validateEmploymentStatus = function(req, res, next) {
    req.checkBody('StatusCode', 'Please Provide Status Code.').notEmpty();
    req.checkBody('Status', 'Please Provide Employment Status Name.').notEmpty();
    req.checkBody('Description', 'Please Provide Description').notEmpty();
    req.checkBody('InActive', 'Please Confirm Employment Status Inactivity. ').notEmpty();

   
    var errors = req.validationErrors();

    if (errors) {
        res.status(400).send({
            response: {
                result: errors,
                msg: '',
                success: false
            },
            statusCode: 400
        });
    } else {
        next();
    }
};